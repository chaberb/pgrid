#ifndef GEOMETRY_READER_H
#define GEOMETRY_READER_H

#include "Reader.h"
//#include "GridFE.h"

namespace memorph {

	struct Point {
		real x, y, z;
	};

	struct Line {
		Point beg, end;
	};

	struct Triangle {
		Point a, b, c;
	};

	struct Geometry {
		std::vector<Point> points;
		std::vector<Primitive> primitives;
	};

	class GeometryReader : public Reader {
	private:
#if 0
		bool _readInfo(GridFE *);
		bool _readNodes(GridFE *);
		bool _readElems(GridFE *);
		bool _readBndInds(GridFE *);
		bool _findHash();
#endif

	public:
		GeometryReader();
		GeometryReader(const std::string &);
		virtual ~GeometryReader();

		bool read(GridFE *);

		static const unsigned maxElemTypeLen = 10;
	};

} // namespace memorph

#endif // GEOMETRY_READER_H
