#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <GL/glut.h>


void display();
void reshape(int, int);
void pressSpecialKey(int key, int x, int y);
void releaseSpecialKey(int key, int x, int y);
void readFile(const char *path);

int main(int argc, char **argv)
{
	readFile(argv[1]);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Viewer");

    glutDisplayFunc(display);
	glutIdleFunc(display);
	glutReshapeFunc(reshape);

	glutIgnoreKeyRepeat(GLUT_KEY_REPEAT_OFF);
	glutSpecialFunc(pressSpecialKey);	
	glutSpecialUpFunc(releaseSpecialKey);

	glEnable(GL_DEPTH_TEST);
	glEnable (GL_LINE_SMOOTH);     /* Enable Antialiased lines */
 	glEnable (GL_LINE_STIPPLE);

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glutMainLoop();

	return 0;
}

void reshape(int width, int height) 
{
	// zabezpiecza przed dzieleniem przez zero
    height = (height == 0) ? 1 : height;

	// stosunek szerokości do wysokości wykorzystany później, aby renederowany obraz nie był zniekształcony - nawiasy "dla bezpieczeństwa"
//    float ratio = ( (float)width ) / ( (float)height );

    // wyzerowanie układu współrzędnych  
	glMatrixMode(GL_PROJECTION | GL_MODELVIEW);
//	glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

	// dopasowuje rzutnię do nowych rozmiarów okna
    glViewport(0, 0, width, height);

	// ustawia bryłę obcinania
    gluPerspective(90.f, (float) width / (float) height, 0.1f, 500.f);
	glFrustum(-10, 10, -10, 10, 100, -100);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	// ciekawe co to...
    //gluLookAt(x, y, z, x + lx, y + ly, z + lz, 0.0f, 1.0f, 0.0f);
}

static float xangle = 0.0f;
static float yangle = 0.0f;//45.0f;
static float distance = -3.5f;

float deltaXangle = 0.0f;
float deltaYangle = 0.0f;
float deltaDistance = 0.0f;

float xdistance = 0.0f;

//struct Color { float red, green, blue; };

#if 0
//const Color colors[] = { RED, GREEN, BLUE, BLACK, WHITE, YELLOW };
const Color colors[] = { 
	{ 0.8f, 0.8f, 0.1f }, 
	{ 0.9f, 0.1f, 0.0f }, 
	{ 0.9f, 0.5f, 0.0f }
};
int colorNum = 3;
#endif

struct Vertex {
	float x, y, z;
	Vertex();
	Vertex(float, float, float);
	virtual ~Vertex();
};

Vertex::Vertex(): x(0), y(0), z(0) { }
Vertex::Vertex(float x, float y, float z): x(x), y(y), z(z) { }
Vertex::~Vertex() { }

const float colors[][3] = {
		{ 0.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, 1.0f }
	};

struct Primitive {	
	enum Color {
		Black = 1,
		Red = 1,
		Green = 2,
		Blue = 3
	};
	enum Type {
		Point = 1,
		Line = 2,
		Triangle = 3,
		Tetrahedron = 4
	};
	int type;
	int color;
	std::vector<Vertex> vertices;

	Primitive();
	Primitive(int type, int color = 0);
	Primitive(const Primitive &);
	Primitive &operator=(const Primitive &);
	virtual ~Primitive();

	void createPoint();
	void createLine();
	void createTriangle();

	void create(int type);
};

Primitive::Primitive():
	type(0),
	color(0),
	vertices(std::vector<Vertex>(0)) { }

Primitive::Primitive(int type, int color):
	type(type),
	color(color),
	vertices(std::vector<Vertex>(0))
{
	vertices.reserve(type);
}

Primitive::Primitive(const Primitive &p):
	type(p.type), 
	color(p.color),
	vertices(std::vector<Vertex>(0))
{
	vertices.reserve(type);
	for (unsigned i = 0; i < type; i++) {
		vertices[i] = p.vertices[i];
	}
}

Primitive &Primitive::operator=(const Primitive &p)
{
	type = p.type;
	color = p.color;
	vertices.reserve(type);
	for (unsigned i = 0; i < type; i++) {
		vertices[i] = p.vertices[i];
	}
	return *this;
}

Primitive::~Primitive() { }

void Primitive::create(int type)
{
	this->type = type;
	vertices.reserve(type);
}

void Primitive::createPoint()
{
	create(Point);
}

void Primitive::createLine()
{
	create(Line);
}

void Primitive::createTriangle()
{
	create(Triangle);
}

#if 0
std::vector< Point > points;
std::vector< std::vector< Point > > lines;
#else
std::vector<Primitive> primitives;
#endif
void display() 
{   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
	
	glTranslatef(0, 0, distance);
    
	glRotatef(xangle, 1.f, 0.f, 0.f);
	glRotatef(yangle, 0.f, 1.f, 0.f);

	glTranslatef(xdistance, 0, 0);

	glPushMatrix();

	for (unsigned i = 0; i < primitives.size(); i++) {
		Primitive p = primitives[i];
		glColor3f(colors[p.color][0], colors[p.color][1], colors[p.color][2]);
		switch (p.type) {
			case Primitive::Point:
			{
				glBegin(GL_POINT);
				glVertex3f(
					primitives[i].vertices[0].x, 
					primitives[i].vertices[0].y,
					primitives[i].vertices[0].z
				);
				glEnd();	
				break;
			}
			case Primitive::Line:
			{
				glBegin(GL_LINES);
				glVertex3f(
					primitives[i].vertices[0].x, 
					primitives[i].vertices[0].y,
					primitives[i].vertices[0].z
				);
				glVertex3f(
					primitives[i].vertices[1].x, 
					primitives[i].vertices[1].y,
					primitives[i].vertices[1].z
				);
				glEnd();
				break;
			}
			case Primitive::Triangle:
			{
				glBegin(GL_LINES);
				glVertex3f(
					primitives[i].vertices[0].x, 
					primitives[i].vertices[0].y,
					primitives[i].vertices[0].z
				);
				glVertex3f(
					primitives[i].vertices[1].x, 
					primitives[i].vertices[1].y,
					primitives[i].vertices[1].z
				);
				glVertex3f(
					primitives[i].vertices[1].x, 
					primitives[i].vertices[1].y,
					primitives[i].vertices[1].z
				);
				glVertex3f(
					primitives[i].vertices[2].x, 
					primitives[i].vertices[2].y,
					primitives[i].vertices[2].z
				);
				glVertex3f(
					primitives[i].vertices[2].x, 
					primitives[i].vertices[2].y,
					primitives[i].vertices[2].z
				);
				glVertex3f(
					primitives[i].vertices[0].x, 
					primitives[i].vertices[0].y,
					primitives[i].vertices[0].z
				);
				glEnd();
				break;
			}
			default:
				{ continue; }
				break;
		}
#if 0
		for (unsigned j = 0; j < primitives[i].type; j++) {
			std::cout << "Typ: " << primitives[i].type << " "
				<< primitives[i].vertices[j].x << " "
				<< primitives[i].vertices[j].y << " "
				<< primitives[i].vertices[j].z << std::endl;
#endif
	}
#if 0
    for (unsigned i = 0; i < grid.elements.size(); i ++) {

		glColor3f(0.7f, 0.7f, 0.9f);
		glLineStipple (0, 0xFFFF); 

		for (unsigned j = 0; j < grid.elements[i].nodes.size(); j++) {
			for (unsigned k = j+1; k < grid.elements[i].nodes.size(); k++) {
				if ((grid.nodes[grid.elements[i].nodes[j]-1].numOfBoundaries != 0 &&
						grid.nodes[grid.elements[i].nodes[k]-1].numOfBoundaries != 0) || (grid.elements[i].number == elem_num)) { 
					if (grid.elements[i].number == elem_num) {
						glColor3f(1.0f, 0.0f, 0.0f);
					} else {
						glColor3f(0.7f, 0.7f, 0.9f);
					}
					glBegin(GL_LINES);
					glVertex3f(
							grid.nodes[grid.elements[i].nodes[j]-1].x(), 
							grid.nodes[grid.elements[i].nodes[j]-1].y(), 
							grid.nodes[grid.elements[i].nodes[j]-1].z());
					glVertex3f(
							grid.nodes[grid.elements[i].nodes[k]-1].x(), 
							grid.nodes[grid.elements[i].nodes[k]-1].y(), 
							grid.nodes[grid.elements[i].nodes[k]-1].z());
					glEnd();
				}
			}
		}
	}
// oś
	glColor3f(0.0f, 0.0f, 0.0f);
//	glLineStipple (1, 0x0101); 
	glBegin(GL_LINES);
	glVertex3f(
			axisBegin.x(),
			axisBegin.y(),
			axisBegin.z());
	glVertex3f(
			axisEnd.x(),
			axisEnd.y(),
			axisEnd.z());
	glEnd();
#endif
	// <- przykładowy wierzchołek

	glPopMatrix();
	glutSwapBuffers();

	xangle += deltaXangle;
	yangle += deltaYangle;
}

char *getSubStr(char *str, const char *begDelim, const char *endDelim, char *out)
{
	if (str == 0 || begDelim == 0 || endDelim == 0) return 0;
	char *subBeg = strstr(str, begDelim);   if (subBeg == 0) return 0; 
	char *subEnd = strstr(++subBeg, endDelim); if (subEnd == 0) return 0;
	size_t subLen = subEnd - subBeg;
	strncpy(out, subBeg, subLen);
	*(out + subLen) = '\0';
	return out;
}

#include "GridFE.h"
#include "GridFEReader.h"

using namespace memorph;

void readFile(const char *path)
{
	std::ifstream input(path);
	const size_t maxLineLen = 256;
	
	char line[maxLineLen];
	char subl[maxLineLen];
	char *sublBeg = 0;
	char *sublEnd = 0;
	char *color = 0;

	bool good = true;

	while (good) {
		input.getline(line, maxLineLen);
		if (input.eof()) {
			break;
		} else if (!input.good()) {
			good = false;
			break;
		} else if (strstr(line, "#Point") != NULL) {
			Primitive primitive;
			float x, y, z;
			getSubStr(line, "(", ")", subl);
			if (sscanf(subl, "%g %g %g", &x, &y, &z) == 3) {
				primitive.createPoint();
				primitive.vertices[0] = Vertex(x, y, z);
			} else {
				good = false;
				break;
			}
			color = strrchr(line, ')');
			primitive.color = atoi(++color);
			primitives.push_back(primitive);
		} else if (strstr(line, "# Line") != NULL) {
			Primitive primitive;
			float x1, y1, z1, x2, y2, z2;
			getSubStr(line, "(", ")", subl);
			if (sscanf(subl, "%g %g %g %g %g %g", &x1, &y1, &z1, &x2, &y2, &z2) == 6) {
				primitive.createLine();
				primitive.vertices[0] = Vertex(x1, y1, z1);
				primitive.vertices[1] = Vertex(x2, y2, z2);
			} else {
				good = false;
				break;
			}
			color = strrchr(line, ')');
			primitive.color = atoi(++color);
			primitives.push_back(primitive);
		} else if (strstr(line, "# Triangle") != NULL) {
			Primitive primitive;
			float x1, y1, z1, x2, y2, z2, x3, y3, z3;
			getSubStr(line, "(", ")", subl);
			if (sscanf(subl, "%g %g %g %g %g %g %g %g %g", &x1, &y1, &z1, &x2, &y2, &z2, &x3, &y3, &z3) == 9) {
				primitive.createTriangle();
				primitive.vertices[0] = Vertex(x1, y1, z1);
				primitive.vertices[1] = Vertex(x2, y2, z2);
				primitive.vertices[2] = Vertex(x3, y3, z3);
			} else {
				good = false;
				break;
			}
			color = strrchr(line, ')');
			primitive.color = atoi(++color);
			primitives.push_back(primitive);
		} else if (strstr(line, "# GridFE") != NULL) {
			getSubStr(line, "\"", "\"", subl);
			color = strrchr(line, '\"');
			int col = atoi(++color);
			GridFE grid;
			GridFEReader gridReader(subl);
			gridReader.read(&grid);
			for (int i = 0; i < grid.noElems(); i++) {
				Primitive p(3);
				for (int j = 0; j < 3; j++) {
					p.vertices[j].x = grid.coords(grid.elemNodeNr(i, j) - 1, 0);
					p.vertices[j].y = grid.coords(grid.elemNodeNr(i, j) - 1, 1);
					p.vertices[j].z = grid.coords(grid.elemNodeNr(i, j) - 1, 2);
				}
				p.color = col;
				primitives.push_back(p);
			}
		}
	}

	std::cout << "Udało się czy nie udało wczytać plik: " << good << std::endl;
	std::cout << "Wczytano " << primitives.size() << " wierzchołków." << std::endl;
}

const float angleStep = 0.5f;
const float distanceStep = 0.05f;

void pressSpecialKey(int  key, int x, int y) 
{
	switch (key) {
		case GLUT_KEY_LEFT:
			deltaYangle = -angleStep;
			break;
		case GLUT_KEY_RIGHT:
			deltaYangle = +angleStep;
			break;
		case GLUT_KEY_UP:
			deltaXangle = -angleStep;
			break;
		case GLUT_KEY_DOWN:
			deltaXangle = +angleStep;
			break;
		case GLUT_KEY_HOME:
			distance += distanceStep;
			break;
		case GLUT_KEY_END:
			distance -= distanceStep;
			break;
		case GLUT_KEY_PAGE_UP:
			xdistance += distanceStep;
			break;
		case GLUT_KEY_PAGE_DOWN:
			xdistance -= distanceStep;
			break;
		default:
			break;
	}
}

void releaseSpecialKey(int  key, int x, int y) 
{
	switch (key) {
		case GLUT_KEY_LEFT:
		case GLUT_KEY_RIGHT:
			deltaYangle = 0.0f;
			break;
		case GLUT_KEY_UP:
		case GLUT_KEY_DOWN:
			deltaXangle = 0.0f;
			break;
		case GLUT_KEY_HOME:
		case GLUT_KEY_END:
			deltaDistance = 0.0f;
	}
}
