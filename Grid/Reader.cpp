#include "Reader.h"
#include <iostream>
#include <cstdlib>

namespace memorph {

	Reader::Reader(): File() { }

	Reader::Reader(const std::string &path): File(path)
	{
		_input.open(path.c_str());
		assure();
	}

	Reader::~Reader()
	{
		if (_input.is_open()) {
			_input.close();
			std::cout
				<< "Odczyt z pliku " 
				<< "\"" << _path << "\""
				<< " zakończony powodzeniem"
				<< std::endl;
		}
	}

	void Reader::assure() const
	{
		if (!_input.is_open()) {
			std::cerr 
				<< "Nie powiodło się otwarcie pliku "
				<< "\"" << _path << "\""
				<< " do odczytu"
				<< std::endl;
			// TODO - może by posprzątac wcześniej...
			exit(0);
		}
		std::cout
			<< "Plik " << "\"" << _path << "\""
			<< " został otwarty do odczytu"
			<< std::endl;
	}

}

#if 0
#include <iostream>

using namespace std;
using namespace memorph;

int main(int, char **)
{
	Reader reader("/home/borysiam/grid/ecyl.grid");
	return 0;
}

#endif
