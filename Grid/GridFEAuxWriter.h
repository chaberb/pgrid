#ifndef GRIDFE_AUX_WRITER_H
#define GRIDFE_AUX_WRITER_H

#include <string>

namespace memorph {

	class GridFEAux;

	class GridFEAuxWriter {
	private:
		std::string _nodePath;
		std::string _elemPath;
		std::string _vectPath;
		std::string _axisPath;

		void _open(const std::string &path, std::ofstream &output) const;
		void _close(std::ofstream &output) const;
		void _good(std::ofstream &output) const;

	public:
		GridFEAuxWriter();
		GridFEAuxWriter(const std::string &pathPref);
		virtual ~GridFEAuxWriter();

		void setNodePath(const std::string &nodePath);
		void setElemPath(const std::string &elemPath);
		void setVectPath(const std::string &vectPath);
		void setAxisPath(const std::string &axisPath);
		void setPath(const std::string &pathPref);

		bool write(const std::string &pathPref, const GridFEAux *grid);
		bool write(const GridFEAux *grid) const;

		bool writeNodes(const std::string &path, const GridFEAux *grid) const;
		bool writeElems(const std::string &path, const GridFEAux *grid) const;
		bool writeVects(const std::string &path, const GridFEAux *grid) const;
		bool writeAxis(const std::string &path, const GridFEAux *grid) const;

	}; // class GridFEAuxWriter

} // namespace memorph

#endif // GRIDFE_AUX_WRITER_H
