#include "Writer.h"
#include <iostream>
#include <cstdlib>

namespace memorph {

	Writer::Writer(): File() { }

	Writer::Writer(const std::string &path): File(path)
	{
		_output.open(path.c_str());
		assure();
	}

	Writer::~Writer()
	{
		if (_output.is_open()) {
			_output.close();
			std::cout << "Zapis do pliku "
					<< '\"'
					<< _path
					<< '\"'
					<< " zakończony powodzeniem"
					<< std::endl;
		}
	}

	void Writer::assure() const
	{
		if (!_output.is_open()) {
			std::cerr << "Nie powiodło się otwarcie pliku "
					<< '\"'
					<< _path
					<< '\"'
					<< " do zapisu"
					<< std::endl;
			// TODO - może by posprzątac wcześniej...
			exit(0);
		}
		std::cout << "Plik "
				<< '\"'
				<< _path
				<< '\"'
				<< " został otwarty do zapisu"
				<< std::endl;
	}

} // namespace memorph

#if 0
#include <iostream>

using namespace std;
using namespace memorph;

int main(int, char **)
{
	Writer writer("/home/borysiam/grid/out.grid");
	return 0;
}

#endif
