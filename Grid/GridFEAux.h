#ifndef GRIDFE_AUX_H
#define GRIDFE_AUX_H

#include "GridFE.h"

namespace memorph {

	class GridFEAux: public GridFE {
	private:
		real **_axisPts;
		real **_vects;

		int _noSlices;
		int _noPtsInSlice;

	public:
		GridFEAux();
		virtual ~GridFEAux();

		int noSlices() const;
		int noPtsInSlice() const;

		void setAxisPt(int sliceIdx, const real pt[]);
		void setAxisPt(int sliceIdx, real x, real y, real z);
		void getAxisPt(int sliceIdx, real *out) const;

		const real &axisPt(int ptIdx, int dimIdx) const;
		real &axisPt(int ptIdx, int dimIdx);

		void setVect(int nodeIdx, real x, real y, real z);
		void setVect(int nodeIdx, const real vect[]);
		void getVect(int nodeIdx, real *out) const;

		void getElemVect(int elemIdx, int nodeIdx, real *out) const;
		void getElemCoords(int elemIdx, int nodeIdx, real *out) const;

		const real &vect(int Idx, int dimIdx) const { return _vects[Idx][dimIdx]; }
		real &vect(int Idx, int dimIdx) { return _vects[Idx][dimIdx]; }


	public:
		void init(int noSlices, int noPtsInSlice);
		void initNodes(int noSlices, int noPtsInSlice);
		void initElems(int noElems);

		void free();
		void freeNodes();
		void freeElems();

		void computeAxisPts();

		void getSrcGrid(GridFE *out);
		void getDstGrid(GridFE *out);

	};

}

#include <cstdlib>
#include <cstring>

namespace memorph {

	inline int GridFEAux::noSlices() const { return _noSlices; }
	inline int GridFEAux::noPtsInSlice() const { return _noPtsInSlice; }

	inline void GridFEAux::setAxisPt(int sliceIdx, const real pt[])
	{
		assertArrIndex(sliceIdx, _noSlices);
		memcpy(_axisPts[sliceIdx], pt, sizeof(real) * _noSpaceDim);
	}

	inline void GridFEAux::setAxisPt(int sliceIdx, real x, real y, real z)
	{
		assertArrIndex(sliceIdx, _noSlices);	
		assertEqual(_noSpaceDim, 3);
		_axisPts[sliceIdx][0] = x;
		_axisPts[sliceIdx][1] = y;
		_axisPts[sliceIdx][2] = z;
	}

	inline void GridFEAux::getAxisPt(int sliceIdx, real *out) const
	{
		assertArrIndex(sliceIdx, _noSlices);
		assertNotNull(out);
		memcpy(out, _axisPts[sliceIdx], sizeof(real) * _noSpaceDim);
	}

	inline void GridFEAux::setVect(int nodeIdx, real x, real y, real z)
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertEqual(_noSpaceDim, 3);
		_vects[nodeIdx][0] = x;
		_vects[nodeIdx][1] = y;
		_vects[nodeIdx][2] = z;
	}

	inline void GridFEAux::setVect(int nodeIdx, const real vect[])
	{
		assertArrIndex(nodeIdx, _noNodes);
		memcpy(_vects[nodeIdx], vect, sizeof(real) * _noSpaceDim);
	}

	inline void GridFEAux::getVect(int nodeIdx, real *out) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertNotNull(out);
		memcpy(out, _vects[nodeIdx], sizeof(real) * _noSpaceDim);
	}


	inline void GridFEAux::getElemVect(int elemIdx, int nodeIdx, real *out) const
	{
		assertArrIndex(elemIdx, _noElems);
		assertArrIndex(nodeIdx, _maxNoNodesInElem);
		assertNotNull(out);
		memcpy(out, _vects[_elemsNodes[elemIdx][nodeIdx]-1], sizeof(real) * _noSpaceDim);
	}

	inline void GridFEAux::getElemCoords(int elemIdx, int nodeIdx, real *out) const
	{
		return getElemNodeCoords(elemIdx, nodeIdx, out);
	}

	inline const real &GridFEAux::axisPt(int sliceIdx, int dimIdx) const
	{
		assertArrIndex(sliceIdx, _noSlices);
		assertArrIndex(dimIdx, _noSpaceDim);
		return _axisPts[sliceIdx][dimIdx];
	}

	inline real &GridFEAux::axisPt(int sliceIdx, int dimIdx)
	{
		assertArrIndex(sliceIdx, _noSlices);
		assertArrIndex(dimIdx, _noSpaceDim);
		return _axisPts[sliceIdx][dimIdx];
	}

}

#endif // GRIDFE_AUX_H
