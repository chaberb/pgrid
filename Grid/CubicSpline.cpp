#include "CubicSpline.h"

namespace memorph {

	CubicSpline::CubicSpline(real x[], real y[], int len) 
	{	
		this->length = len-1;
		N = len-1;
		this->x = new real[N+1];
		this->y = new real[N+1];
		q = new real[N+1];
		u = new real[N+1];
		M = new real[N+1];
		
		for (int i = 0; i <= length; i++) {
			this->x[i] = x[i];
			this->y[i] = y[i];
		}
		
		_compute();
	}
	
	CubicSpline::~CubicSpline() 
	{	
		delete[] x;
		delete[] y;
		delete[] q;
		delete[] u;
		delete[] M;
		x = y = 0;
		q = u = M = 0;
	}
	
    real CubicSpline::interpolate(real t) const 
	{			
		int i = N;	
		real dt;
		
		do {
			i--;
			dt = t-x[i];
		} while ((i > 0) && (dt < 0));
		
		real dx = x[i+1]-x[i];
		real w = (((M[i+1]-M[i])/(6*dx)*dt+M[i]/2)*dt+((y[i+1]-y[i])/dx-(M[i+1]+2*M[i])/6*dx))*dt+y[i];
		
		return w;
	}

	void CubicSpline::_compute() 
	{			
		for (int i = 1; i < N; i++) {
			real lambda = (x[i+1] - x[i]) / (x[i+1] - x[i-1]);
			real mi = 1 - lambda;
			real d = 6 * ((y[i+1] - y[i]) / (x[i+1] - x[i]) - (y[i] - y[i-1]) / (x[i] - x[i-1])) / (x[i+1]-x[i-1]);
			q[i] = -lambda / (mi * q[i-1] + 2);
			u[i] = (d - mi * u[i-1]) / (mi * q[i-1] + 2);
		}
		
		M[N] = 0;

		for (int i = N-1; i >= 0; i--) {
			M[i] = q[i]*M[i+1]+u[i];
		}
	}

} // namespace memorph

# if 0

#include <iostream>
#include <cstdlib>

using namespace std;
using namespace memorph;

int main(int argc, char **argv)
{
	if ((argc - 1) % 2 != 0) {
		std::cerr << "Podaj parzystą liczbę pomiarów x -> y" << std::endl;
		return 1;
	}

	int noMeasures = (argc - 1) / 2;

	real *x = (real *) malloc(sizeof *x * noMeasures);
	real *y = (real *) malloc(sizeof *y * noMeasures);

	int i = 1;
	for ( ; i <= argc / 2; i++) {
		x[i-1] = atof(argv[i]);
		y[i-1] = atof(argv[i + argc / 2]);
	}

	CubicSpline *spline = new CubicSpline(x, y, noMeasures);

	real begX = -4;
	real endX = +4;

	int noSlices = 48;

	real dSlice = (endX - begX) / noSlices;

	for (int i = 0; i < noSlices; i++) 
	{
		real x = begX + dSlice * i;
		real y = spline->interpolate(x);

		std::cout << "# Line ("
			<< x << " "
			<< 0 << " "
			<< 0 << " "
			<< x << " "
			<< y << " "
			<< 0 << ")\n";
	}

	return 0;
}

#endif
