#ifndef GRIDFE_AUX_READER_H
#define GRIDFE_AUX_READER_H

#include <string>

namespace memorph {

	class GridFEAux;

	class GridFEAuxReader {
	private:
		std::string _nodePath;
		std::string _elemPath;
		std::string _vectPath;
		std::string _axisPath;

		bool _readLine(std::ifstream &input, std::string &line) const;
		void _open(const std::string &path, std::ifstream &input) const;
		void _close(std::ifstream &input) const;
		void _good(std::ifstream &input) const;

	public:
		GridFEAuxReader();
		GridFEAuxReader(const std::string &pathPref);
		virtual ~GridFEAuxReader();

		void setNodePath(const std::string &nodePath);
		void setElemPath(const std::string &elemPath);
		void setVectPath(const std::string &vectPath);
		void setAxisPath(const std::string &axisPath);
		void setPath(const std::string &pathPref);

		bool read(const std::string &pathPref, GridFEAux *grid);
		bool read(GridFEAux *grid) const;

		bool readNodes(const std::string &path, GridFEAux *grid) const;
		bool readElems(const std::string &path, GridFEAux *grid) const;
		bool readVects(const std::string &path, GridFEAux *grid) const;
		bool readAxis(const std::string &path, GridFEAux *grid) const;

	}; // class GridFEAuxReader

} // namespace memorph

#endif // GRIDFE_AUX_READER_H
