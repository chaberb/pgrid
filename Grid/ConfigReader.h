#ifndef CONFIG_READER_H
#define	CONFIG_READER_H

#include <string>
#include "Base.h"

namespace memorph {

	class Config;

	class ConfigReader {
	private:
		std::string _path;

		// TODO - wyciągnąć do nadklasy
		bool _readLine(std::ifstream &input, std::string &line) const;
		void _open(const std::string &path, std::ifstream &input) const;
		void _close(std::ifstream &input) const;
		void _good(std::ifstream &input) const;

	public:
		ConfigReader();
		ConfigReader(const std::string &path);
		virtual ~ConfigReader();

		void setPath(const std::string &path);

		bool read(const std::string &path, Config *out);
		bool read(Config *out) const;
	
	}; // ConfigReader

} // namespace memorph

#endif // CONFIG_READER_H
