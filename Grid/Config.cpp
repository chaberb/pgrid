#include "Config.h"
#include <cstdlib>

namespace memorph {

	Config::Config():
		_noMsrmnts(0), 
		_msrmnts(0) 
	{ }

	Config::Config(int noMsrmnts):
		_noMsrmnts(0),
		_msrmnts(0)
	{
		init(noMsrmnts);
	}

	Config::~Config()
	{
		free();
	}

	void Config::init(int noMsrmnts)
	{
		_msrmnts = new real *[noMsrmnts];

		for (int i = 0; i < noMsrmnts; ++i) {
			_msrmnts[i] = new real[noParams];
			memset(_msrmnts[i], 0, sizeof(real) * noParams);
		}
		_noMsrmnts = noMsrmnts;
	}

	void Config::free()
	{
		for (int i = 0; i < _noMsrmnts; ++i) {
			delete[] _msrmnts[i];
		}
		delete[] _msrmnts;
		_msrmnts = 0;
		_noMsrmnts = 0;
	}

	int Config::noMsrmnts() const 
	{ 
		return _noMsrmnts; 
	}

	const real &Config::msrmnts(int i, int j) const
	{
		return _msrmnts[i][j];
	}

	real &Config::msrmnts(int i, int j)
	{
		return _msrmnts[i][j];
	}

} // namespace memorph
