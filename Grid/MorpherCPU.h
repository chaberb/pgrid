#ifndef MORPHER_CPU_H
#define MORPHER_CPU_H

#include "Base.h"

namespace memorph {

	class GridFE;
	class GridFEAux;
	class Config;

	class Morpher {
	private:
		GridFEAux *_auxGrid;
		Config *_config;

	public:
		Morpher();
		virtual ~Morpher();

	public:
		void setAuxGrid(GridFEAux *auxGrid);
		void setConfig(Config *config);

		void compute();
		void morph(GridFE *grid);

	}; // class Morpher

} // namespace memorph

#endif // MORPHER_CPU_H
