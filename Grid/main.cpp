#include <GridFEBounder.h>
#include <GridFEWriter.h>
#include <GridFEReader.h>
#include <GridFE.h>

using namespace memorph;
int main(int argc, char** argv) {
	if (argc == 1)
		return 1;
	
	GridFEBounder grid;
	GridFE boundary;

	GridFEReader reader(argv[1]);
	GridFEWriter writer(argv[2]);
	GridFEBounder bounder;

	reader.read(&grid);
	
	grid.createFacets();
	grid.sortFacets();
	grid.markExternals();
	grid.getBndGrid(&boundary);

	writer.write(boundary);
	return 0;
}
