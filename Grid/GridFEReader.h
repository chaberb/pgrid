#ifndef GRID_READER_H
#define GRID_READER_H

#include "Reader.h"

namespace memorph {

	class GridFE;

        class GridFEReader : public Reader {
        protected:
		bool _readInfo(GridFE *);
		bool _readNodes(GridFE *);
		bool _readElems(GridFE *);
		bool _readBndInds(GridFE *);
		bool _findHash();

	public:
		GridFEReader();
		GridFEReader(const std::string &);
		virtual ~GridFEReader();

		bool read(GridFE *);

		static const unsigned maxElemTypeLen = 10;
	};

} // namespace memorph

#endif // GRID_READER_H
