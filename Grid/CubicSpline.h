#ifndef CUBIC_SPLINE_H
#define CUBIC_SPLINE_H

#include "Base.h"

namespace memorph {
	
	class CubicSpline {
		
		private:
			int length;
			int N;
			
			real *x, *y;
			real *q, *u;
			real *M;
			
			void _compute();
			
		public:
			CubicSpline(real *, real *, int);
			virtual ~CubicSpline();
			
			real interpolate(real x) const;
    };

} // namespace memorph

#endif // CUBIC_SPLINE_H 
