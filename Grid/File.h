#ifndef FILE_H
#define FILE_H

#include <string>

namespace memorph {

	class File {
	protected:
		std::string _path;

	public:
		File();
		File(const std::string &);
		virtual ~File();

		virtual void assure() const = 0;
	};

}
#endif // FILE_H
