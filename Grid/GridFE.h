#ifndef GRIDFE_H
#define GRIDFE_H

#include "Base.h"

#define USE_DEPRECATED

namespace memorph {

	class GridFEJoiner;
        class GridFESpliter;

	class GridFE {
	public:
		/* liczba elementów tablicy opisującej węzeł */
		static const size_t noNodeParams = 2; //4 XXX;
		/* liczba elementów tablicy opisującej element */
		static const size_t noElemParams = 4;
		/* liczba elementów tablicy opisującej powierzchnię */
		static const size_t noBndParams = 1;

		/* typ elementu (trójkąt, czworościan) */
		enum ElementType {
			T3n2D = 3,
			T4n3D = 4
		};

        protected:
		real **_coords;

		int **_nodes;
		int **_nodesBndInds;

		int **_elems;  
		int **_elemsNodes; 

		int  **_bnds;
		char **_bndsNames;

		int  _noNodes;
		int  _noElems;
		int  _noBnds;
		int  _noSubds;
		int  _noSpaceDim;
		int  _maxNoNodesInElem;
		bool _sameElems;
		bool _onlyOneSubd;

	public:
		GridFE();
		~GridFE();


	public:
		// numerki
		int noNodes() const;
		int noElems() const;
		int noBnds() const;
		int noSubds() const;
		int noSpaceDim() const;
		bool sameElems() const;
		bool onlyOneSubd() const;

		// wpółrzędne
#ifdef USE_DEPRECATED
		void coords(int nodeIdx, real x, real y, real z);	// XXX DEPRACATED !!! - nie da się uwzględnić wymiaru siatki
		void coords(int nodeIdx, real *out) const;	      	// XXX DEPRACATED !!! - nazwa jest dwuznaczna
		const real &coords(int nodeIdx, int dimIdx) const;	// XXX DEPRACATED !!! - brzydko się nazywa :P
		real &coords(int nodeIdx, int dimIdx);				// XXX DEPRACATED !!! - brzydko się nazywa :P
#endif
		const real &coord(int nodeIdx, int dimIdx) const;
		real &coord(int nodeIdx, int dimIdx);

		void setCoords(int nodeIdx, const real coords[]);
		void setCoords(int nodeIdx, real x, real y, real z);
		void getCoords(int nodeIdx, real *out) const;
		void getCoords(real **out) const;


		// węzły
		const int &nodeNr(int nodeIdx) const;
		int &nodeNr(int nodeIdx);
		const int &nodeBndInd(int nodeIdx, int bndIndIdx) const;
		int &nodeBndInd(int nodeIdx, int bndIndIdx);
		int noNodeBndInds(int nodeIdx) const;
		void initNodeBndInds(int nodeIdx, int noBndInds);
		void freeNodeBndInds(int nodeIdx);
		void addNodeToBnd(int nodeIdx, int bnd);
		void createNode(int nodeIdx, int noBnds, int *bndInds);


		// elementy
		const int &elemNr(int elemIdx) const;
		int &elemNr(int elemIdx);
		const int &elemSubdNr(int elemIdx) const;	// XXX - nie pozwala zliczać domen
		int &elemSubdNr(int elemIdx);				// XXX - nie pozwala zliczać domen
#ifdef USE_DELETED
		void setElemSubdNr(int elemIdx, int subdNr);
		int  getElemSubdNr(int elemIdx) const;
#endif // USE_DELETED
		const int &elemNodeNr(int elemIdx, int nodeIdx) const;
		int &elemNodeNr(int elemIdx, int nodeIdx);
		int elemType(int elemIdx) const;
		int noElemNodes(int elemIdx) const;
		void initElemNodes(int elemIdx, int type);
		void freeElemNodes(int elemIdx);
		void createT3n2D(int elemIdx);
		void createT4n3D(int elemIdx);
		void setElemNodeCoords(int elemIdx, int nodeIdx, const real coords[]);
		void getElemNodeCoords(int elemIdx, int nodeIdx, real *out) const;

		// powierzchnie
		const int &bndNr(int bndIdx) const;
		const char *bndName(int bndIdx) const;
		int &bndNr(int bndIdx);
		void bndName(int bndIdx, const char *name);
		int noBndNodes(int bndIdx) const;

	public:
		void init(int noNodes, int noElems, int noBnds);
		void free();

		void initNodes(int noNodes);
		void initElems(int noElems);
		void initBnds(int noBnds);
		void initSubds(int noSubds);

		void freeNodes();
		void freeElems();
		void freeBnds();
		void freeSubds();

		void copy(GridFE *out) const;
		void copyCoords(int limit, real **out) const;

		void assign(const GridFE *grid, const real **coords = 0);
		void renumber(const int *nodesNumb, GridFE *grid) const;

	public:
		friend class GridFEJoiner;
                friend class GridFESpliter;
		//friend class GridFEWrapper;

	};

}

#include <cstdlib>
#include <cstring>

// coords - inline
namespace memorph {

#undef USE_DEPRECATED
#ifdef USE_DEPRECATED
	inline void GridFE::coords(int nodeIdx, real x, real y, real z)
	{
		assertArrIndex(nodeIdx, _noNodes);
		_coords[nodeIdx][0] = x;
		_coords[nodeIdx][1] = y;
		_coords[nodeIdx][2] = z;
	}

	inline void GridFE::coords(int nodeIdx, real *out) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertNotNull(out);
		out[0] = _coords[nodeIdx][0];
		out[1] = _coords[nodeIdx][1];
		out[2] = _coords[nodeIdx][2];
	}
	
	inline const real &GridFE::coords(int nodeIdx, int dimIdx) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertArrIndex(dimIdx, _noSpaceDim);
		return _coords[nodeIdx][dimIdx];
	}

	inline real &GridFE::coords(int nodeIdx, int dimIdx)
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertArrIndex(dimIdx, _noSpaceDim);
		return _coords[nodeIdx][dimIdx];
	}
#endif // USE_DEPRECATED

	inline const real &GridFE::coord(int nodeIdx, int dimIdx) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertArrIndex(dimIdx, _noSpaceDim);
		return _coords[nodeIdx][dimIdx];
	}

	inline real &GridFE::coord(int nodeIdx, int dimIdx)
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertArrIndex(dimIdx, _noSpaceDim);
		return _coords[nodeIdx][dimIdx];
	}

	inline void GridFE::setCoords(int nodeIdx, const real coords[])
	{
		assertArrIndex(nodeIdx, _noNodes);
		memcpy(_coords[nodeIdx], coords, sizeof(real) * _noSpaceDim);
	}

	inline void GridFE::setCoords(int nodeIdx, real x, real y, real z)
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertEqual(_noSpaceDim, 3);
		_coords[nodeIdx][0] = x;
		_coords[nodeIdx][1] = y;
		_coords[nodeIdx][2] = z;
	}

	inline void GridFE::getCoords(int nodeIdx, real *out) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertNotNull(out);
		memcpy(out, _coords[nodeIdx], sizeof(real) * _noSpaceDim);
	}

	inline void GridFE::getCoords(real **out) const
	{
		assertNotNull(out);
		for (int i = 0; i < _noNodes; ++i) {
			memcpy(out[i], _coords[i], sizeof(real) * _noSpaceDim);
		}
	}
	
} // namespace memorph - coords inline

// nodes inline
namespace memorph {

	/* _nodes[_noNodes][2]
	 * 	0 - numer węzła
	 * 	1 - liczba wskaźników powierzchni
	 */

	inline const int &GridFE::nodeNr(int nodeIdx) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		return _nodes[nodeIdx][0];
	}

	inline int &GridFE::nodeNr(int nodeIdx)
	{
		assertArrIndex(nodeIdx, _noNodes);
		return _nodes[nodeIdx][0];
	}

	inline int GridFE::noNodeBndInds(int nodeIdx) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		return _nodes[nodeIdx][1];
	}

	/* _nodesBndInds[_noNodes][0..*]
	 *  0, ... wskaźniki powierzchni
	 */

	inline const int &GridFE::nodeBndInd(int nodeIdx, int bndIndIdx) const
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertArrIndex(bndIndIdx, noNodeBndInds(nodeIdx));
		return _nodesBndInds[nodeIdx][bndIndIdx];
	}

	inline int &GridFE::nodeBndInd(int nodeIdx, int bndIndIdx)
	{
		assertArrIndex(nodeIdx, _noNodes);
		assertArrIndex(bndIndIdx, noNodeBndInds(nodeIdx));
		return _nodesBndInds[nodeIdx][bndIndIdx];
	}

	inline void GridFE::initNodeBndInds(int nodeIdx, int noBndInds)
	{
		assertArrIndex(nodeIdx, _noNodes);
		/* jeżeli wskażniki powierzchni są już zaalokowane, to zwalnia je */
		if (_nodesBndInds[nodeIdx] != 0) {
			freeNodeBndInds(nodeIdx);
		}
		_nodesBndInds[nodeIdx] = new int[noBndInds];
                _nodes[nodeIdx][1] = noBndInds;
	}

	inline void GridFE::freeNodeBndInds(int nodeIdx)
	{
		assertArrIndex(nodeIdx, _noNodes);
		delete[] _nodesBndInds[nodeIdx];
		_nodesBndInds[nodeIdx] = 0;
		_nodes[nodeIdx][1] = 0;
	}

} // namespace memorph - nodes inline

// numerki inline
namespace memorph {

	inline  int GridFE::noNodes() const { return _noNodes; }
	inline  int GridFE::noElems() const { return _noElems; }
	inline  int GridFE::noBnds()  const { return _noBnds;  }
	inline  int GridFE::noSubds() const { return _noSubds; }
	inline  int GridFE::noSpaceDim() const { return _noSpaceDim; }
	inline bool GridFE::sameElems() const { return _sameElems; }
	inline bool GridFE::onlyOneSubd() const { return _onlyOneSubd; }

} 

// elementy inline
namespace memorph {

	/* _elems[_noElems][]
	 * 0 - numer
	 * 1 - typ
	 * 2 - numer materiału
	 */

	inline const int &GridFE::elemNr(int elemIdx) const
	{
		assertArrIndex(elemIdx, _noElems);
		return _elems[elemIdx][0];
	}

	inline int &GridFE::elemNr(int elemIdx)
	{
		assertArrIndex(elemIdx, _noElems);
		return _elems[elemIdx][0];
	}

	inline int GridFE::elemType(int elemIdx) const
	{
		assertArrIndex(elemIdx, _noElems);
		return _elems[elemIdx][1];
	}

	inline int GridFE::noElemNodes(int elemIdx) const 
	{
		return elemType(elemIdx);
	}

	inline const int &GridFE::elemSubdNr(int elemIdx) const
	{
		assertArrIndex(elemIdx, _noElems);
		return _elems[elemIdx][2];
	}

	inline int &GridFE::elemSubdNr(int elemIdx)
	{
		assertArrIndex(elemIdx, _noElems);
		return _elems[elemIdx][2];
	}

#ifdef USE_DELETED 
	inline void GridFE::setElemSubdNr(int elemIdx, int subdNr)
	{
		assertArrIndex(elemIdx, _noElems);
		_elems[elemIdx][2] = subdNr;
		if (subdNr > _noSubds) {
			_noSubds = subdNr;
		}
	}

	inline int GridFE::getElemSubdNr(int elemIdx) const
	{
		assertArrIndex(elemIdx, _noElems);
		return _elems[elemIdx][2];
	}
#endif // USE_DELETED

	/* _elemsNodes[_noElems][3|4]
	 * 0, ... - numery węzłów
	 */

	inline void GridFE::initElemNodes(int elemIdx, int type)
	{
		assertArrIndex(elemIdx, _noElems);
		if (_elemsNodes[elemIdx] != 0) {
			freeElemNodes(elemIdx);	
		}
		_elemsNodes[elemIdx] = new int[type];
		memset(_elemsNodes[elemIdx], 0, sizeof(int) * type);
		_elems[elemIdx][1] = type;
	}

	inline void GridFE::freeElemNodes(int elemIdx)
	{
		assertArrIndex(elemIdx, _noElems);
		delete[] _elemsNodes[elemIdx];
		_elemsNodes[elemIdx] = 0;
		_elems[elemIdx][1] = 0;
	}

	inline void GridFE::createT3n2D(int elemIdx)
	{
		assertArrIndex(elemIdx, _noElems);
		initElemNodes(elemIdx, T3n2D);
	}

	inline void GridFE::createT4n3D(int elemIdx)
	{
		assertArrIndex(elemIdx, _noElems);
		initElemNodes(elemIdx, T4n3D);
	}

	inline const int &GridFE::elemNodeNr(int elemIdx, int nodeIdx) const
	{
		assertArrIndex(elemIdx, _noElems);
		assertArrIndex(nodeIdx, elemType(elemIdx));
		return _elemsNodes[elemIdx][nodeIdx];
	}
	
	inline int &GridFE::elemNodeNr(int elemIdx, int nodeIdx)
	{
		assertArrIndex(elemIdx, _noElems);
		assertArrIndex(nodeIdx, elemType(elemIdx));
		return _elemsNodes[elemIdx][nodeIdx];
	}

	inline void GridFE::setElemNodeCoords(int elemIdx, int nodeIdx, const real coords[])
	{
		assertArrIndex(elemIdx, _noElems);
		assertArrIndex(nodeIdx, elemType(elemIdx));
		memcpy(_coords[_elemsNodes[elemIdx][nodeIdx]-1], coords, sizeof(real) * _noSpaceDim);
	}

	inline void GridFE::getElemNodeCoords(int elemIdx, int nodeIdx, real *out) const
	{
		assertArrIndex(elemIdx, _noElems);
		assertArrIndex(nodeIdx, elemType(elemIdx));
		memcpy(out, _coords[_elemsNodes[elemIdx][nodeIdx]-1], sizeof(real) * _noSpaceDim);
	}

} // namespace memorph - elementy inline

// ograniczenia inline
namespace memorph {

	/* _bnds[_noBnds][???]
	 * 0 - numer powierzchni
	 * ....
	 */

	inline const int &GridFE::bndNr(int bndIdx) const
	{
		assertArrIndex(bndIdx, _noBnds);
		return _bnds[bndIdx][0];
	}

	inline int &GridFE::bndNr(int bndIdx)
	{
		assertArrIndex(bndIdx, _noBnds);
		return _bnds[bndIdx][0];
	}

	/* _bndsNames[_noBnds][*]
	 * nazwy zapisane jako char *
	 */
	inline const char *GridFE::bndName(int bndIdx) const
	{
		assertArrIndex(bndIdx, _noBnds);
		return _bndsNames[bndIdx];
	}

	inline void GridFE::bndName(int bndIdx, const char *name)
	{
		assertArrIndex(bndIdx, _noBnds);
		size_t len = strlen(name);
		_bndsNames[bndIdx] = new char[len + 1];
		strncpy(_bndsNames[bndIdx], name, sizeof(char) * len);
	}

	inline void GridFE::addNodeToBnd(int, int)
	{
		// TODO...
	}

} // namespace memorph - boundaries inline

#endif // GRIDFE_H
