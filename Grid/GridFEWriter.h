#ifndef GRIDWRITER_H
#define GRIDWRITER_H

#include "Writer.h"
#include "GridFE.h"


namespace memorph {

	class GridFEWriter : public Writer {
	private:
                std::ofstream _ouput;

        protected:
		bool _writeInfo(const GridFE &);
		bool _writeNodes(const GridFE &);
		bool _writeElems(const GridFE &);
		bool _writeBndInds(const GridFE &);

	public:
		GridFEWriter();
		GridFEWriter(const std::string &path);
		virtual ~GridFEWriter();

		bool write(const GridFE &);
	};

}

#endif // GRIDWRITER_H
