#include "GridFEReader.h"
#include <cstdlib>
#include <cstring>
#include <cassert>
#include "GridFE.h"

namespace memorph {

	GridFEReader::GridFEReader():
			Reader() { }

	GridFEReader::GridFEReader(const std::string &path):
			Reader(path) { }

	GridFEReader::~GridFEReader() { }

	bool GridFEReader::_readInfo(GridFE *grid)
	{
		char line[maxLineLen];
		char *p;
		unsigned char flags = 0x00;

		while (true) {
			_input.getline(line, maxLineLen);
			if (!_input.good()) {
				std::cerr << "Nie udało się odczytać nagłówka";
				return false;
			} else if ((p = strrchr(line, '=')) != NULL && strstr(line, "Number of nodes") != NULL) {
				grid->initNodes(atoi(++p));
				flags |= 0x01;
			} else if ((p = strrchr(line, '=')) != NULL && strstr(line, "Number of elements") != NULL) {
				grid->initElems(atoi(++p));
				flags |= 0x02;
			} else if ((p = strrchr(line, '=')) != NULL && strstr(line, "Number of space dim.") != NULL) {
				//grid->_noSpaceDim = atoi(++p);
				flags |= 0x04;
			}
			if (flags == 0x07) {
				break;
			}
		}
		
		std::cout << "Informacje o siatce:";
//		std::cout << '\t' << "Liczba węzłów = " << grid->noNodes() << std::endl;
//		std::cout << '\t' << "Liczba elementów = " << grid->noElems() << std::endl;
//		std::cout << '\t' << "Liczba wymiarów przestrzeni = " << grid->noSpaceDim() << std::endl;
		
		return true;
	}

	bool GridFEReader::_readBndInds(GridFE *grid)
	{
		char line[maxLineLen];
		while (true) {
			_input.getline(line, maxLineLen);
			if (!_input.good()) {
				std::cerr
					<< "Nie udało się odczytać listy węzłów - nieoczekiwany koniec pliku lub inny błąd" 
					<< std::endl;
				return false;
			}
			if (strstr(line, "Boundary indicators") != NULL) {
				break;
			}
		}

		int noBndInds;
		char bndInd[10];
		sscanf(line, " %d Boundary indicators: %[ 0123456789]", &noBndInds, line);
		grid->initBnds(noBndInds);

		for (int i = 0; i < noBndInds; ++i) {
			sscanf(line, " %s %[ 0123456789]", bndInd, line);
			grid->bndNr(i) = i+1;
			grid->bndName(i, bndInd);
#if 0
			grid->_bnds[i].nr = i+1;
			strcpy(grid->_bnds[i].name, bndInd);
#endif
		}

		std::cout << "Odczytano " << grid->noBnds() << " znaczników powierzchni\n";
#if 0
		for (int i = 0; i < grid->noBnds(); ++i) {
			std::cout << "\t" << grid->bndName(i) << std::endl;		
		}
#endif

		return true;
	}

	bool GridFEReader::_readNodes(GridFE *grid)
	{
		char line[maxLineLen];
		float x, y, z;
		int nodeNr, bndInd;
		int noInds;
		char inds[maxLineLen];
		for (int i = 0; i < grid->noNodes(); ++i) {
			_input.getline(line, maxLineLen);
			if (!_input.good()) {
				std::cerr << "Nie udało się odczytać listy węzłów - nieoczekiwany koniec pliku lub inny błąd" << std::endl;
				return false;
			}
			/* pobiera to co widać (5 sztuk) + wskaźników powierzchni,
			 * jeżeli są - są one ładowane w całości z powrotem do tablicy
			 * bndInds i odczytywane oddzielnie */

                        /* sscanf wykorzystuje locale do określenia znaku oddzielającego
                         * część dziesiętną liczby zmiennoprzecinkowej. Ustawimy, aby
                         * wykorzystywał '.'. */
                        setlocale(LC_NUMERIC, "C");
			if (sscanf(line, "%d ( %g , %g , %g ) [%d] %[ 0123456789]", &nodeNr, &x, &y, &z, &noInds, inds) >= 5) {
				grid->setCoords(i, x, y, z);
				grid->nodeNr(i) = nodeNr;
				grid->initNodeBndInds(i, noInds);
				for (int j = 0; j < noInds; ++j) {
					sscanf(inds, " %d %[ 0123456789]", &bndInd, inds);
					grid->nodeBndInd(i, j) = bndInd;
					grid->addNodeToBnd(i, bndInd);
					/* TODO - dla przyspieszenia operacji na siatce, dobrze jest
					 * dodawać węzły do powierzchni (jakaś lista, bo początkowo
					 * liczba węzłów należących do danej powierzchni jest
					 * nieznana */
				}
			} else {
				std::cerr 
					<< "Nie udało się odczytać listy węzłów :-(\n" 
					<< "Oto feralna linijka:\n" << line << std::endl;
				return false;
			}
		}
		
		std::cout << "Odczytano " << grid->noNodes() << " węzłów" << std::endl;
		return true;
	}

	bool GridFEReader::_readElems(GridFE *grid)
	{
		int nr, subdNr, nodeNr;
		int type;
		char typeStr[10];

		char line[maxLineLen];
		for (int i = 0; i < grid->noElems(); ++i) {
			_input.getline(line, maxLineLen);
			if (!_input.good()) {
				std::cerr << "Nie udało się odczytać listy elementów - nieoczekiwany koniec pliku lub inny błąd" << std::endl;
				return false;
			}
			if (sscanf(line, "%d %s %d %[ 0123456789]", &nr, typeStr, &subdNr, line) == 4) {
				if (strcmp(typeStr, "ElmT3n2D") == 0) {
					grid->createT3n2D(i);
				} else if (strcmp(typeStr, "ElmT4n3D") == 0) {
					grid->createT4n3D(i);
				} else {
					std::cerr << "Nieobsługiwany typ elementu " << "\"" << typeStr << "\"" << std::endl;
				}
				type = grid->elemType(i);
				grid->elemNr(i) = nr;
				grid->elemSubdNr(i) = subdNr;
				std::cout << "E: " << (i+1) << " ";
				for (int j = 0; j < type; ++j) {
					sscanf(line, " %d %[ 0123456789]", &nodeNr, line);
					grid->elemNodeNr(i, j) = nodeNr;
					std::cout << nodeNr << " ";
				}
				std::cout << std::endl;
			} else {
				std::cerr
					<< "Nie udało się odczytać listy elementów :-(" << "\n"
					<< "Oto feralna linijka:" << "\n"
					<< line << std::endl;
				return false;
			}
		}
		std::cout << "Odczytano " << grid->noElems() << " elementów" << std::endl;
		return true;
	}

	bool GridFEReader::_findHash()
	{
		// ignoruje wszystkie znaki w poszukiwaniu '#'
		while (_input.get() != '#') { }
		// ignoruje białe znaki po '#'
		while (isspace(_input.get())) { }
		// oddaje pierwszy nie biały znak po '#' czyli początek jakiegoś mięcha
		_input.unget();
		return true;
	}

	bool GridFEReader::read(GridFE *grid)
	{
		if (!_readInfo(grid)) {
			return false;
                }
		if (!_readBndInds(grid)) {
			return false;
		}
		if (!_findHash()) {
			return false;
		}
		if (!_readNodes(grid)) {
			return false;
		}
		if (!_findHash()) {
			return false;
                }
		if (!_readElems(grid)) {
			return false;
                }
		return true;
	}

} // namespace memorph

#if 0

#include <iostream>
#include <GridFE.h>
#include <GridFEReader.h>

using namespace memorph;
using namespace std;

int main(int, char **)
{
	GridFE grid;
	GridFEReader gridReader("/home/borysiam/grid/sphere-c.grid");
	gridReader.read(&grid);

	for (int i = 0; i < grid.noBnds(); i++) {
		cout << grid.bnds(i).nr << " - " << grid.bnds(i).name << ":" << endl;
		for (int j = 0; j < grid.bnds(i).noNodes; j++)
			cout << "\t" << grid.bnds(i).nodes[j] << endl;
	}

	return 0;
}

#endif
