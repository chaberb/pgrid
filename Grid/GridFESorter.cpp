#include "GridFESorter.h"
#include "GridFE.h"

namespace memorph {

	GridFESorter::GridFESorter():
		_grid(0),
		_noNodes(0),
		_noSpaceDim(0),
		_coords(0) 
	{ 
		_init();
	}

	GridFESorter::~GridFESorter() 
	{ 
		_free();
	}

	void GridFESorter::_init() { }

	void GridFESorter::_free()
	{
		_freeNodesData();
		_freeElemsData();
	}

	void GridFESorter::setGrid(GridFE *grid) { _grid = grid; }
	
	void GridFESorter::_initNodesData()
	{
		int noNodes = _grid->noNodes();
		int noSpaceDim = _grid->noSpaceDim();

		_coords = new real *[noNodes];
		
		for (int i = 0; i < noNodes; ++i) {
			_coords[i] = new real[noSpaceDim + 1];
//			memset(_coords[i], 0, (sizeof(real) * noSpaceDim + 1));
		}

		_noNodes = noNodes;
		_noSpaceDim = noSpaceDim;
	}

	void GridFESorter::_initElemsData()
	{
		// TODO
	}

	void GridFESorter::_freeNodesData()
	{
		int noNodes = _grid->noNodes();

		for (int i = 0; i < noNodes; ++i) {
			delete[] _coords[i];
		}

		delete[] _coords;
		_coords = 0;
		_noNodes = 0;

	}

	void GridFESorter::_freeElemsData()
	{
		// TODO
	}

	void GridFESorter::_createNodesData()
	{
		assertNotNull(_grid);
		assertNotNull(_coords);

		for (int i = 0; i < _noNodes; ++i) {
			/* kopiuje węzły */
			for (int j = 0; j < _noSpaceDim; ++j) {
				_coords[i][j] = _grid->coord(i, j);
			}
			/* dopisuje atrybut (numer węzła) */
			_coords[i][_noSpaceDim] = _grid->nodeNr(i);
		}

	}

	void GridFESorter::_createElemsData()
	{
		// TODO
	}

	int nodesCmpZ(const void *nodeA, const void *nodeB);
	
	void GridFESorter::sortNodes(int (*comparator)(const void *, const void *), int *numb)
	{
		assertNotNull(numb);
		qsort(_coords, _noNodes, sizeof(real *), comparator);

		for (int i = 0; i < _noNodes; ++i) {
			numb[i] = (int) _coords[i][_noSpaceDim];
		}
	}

	void GridFESorter::sortElems(int (*comparator)(const void *, const void *))
	{
		// TODO
	}

	void GridFESorter::sort(GridFE *sorted)
	{
		assertNotNull(_grid);

		_initNodesData();
		_createNodesData();

		int *numb = new int[_noNodes];
#if 0
		for (int i = 0; i < _noNodes; ++i) {
			for (int j = 0; j < 4; ++j) {
				std::cout << _coords[i][j] << " ";
			}
			std::cout << std::endl;
		}
#endif
		sortNodes(nodesCmpZ, numb);

		_grid->renumber(numb, sorted);
#if 1
		for (int i = 0; i < _noNodes; ++i) {
			std::cout << sorted->nodeNr(i) << " ";
			for (int j = 0; j < 3; ++j) {
				std::cout << sorted->coord(i, j) << " ";
			}
			std::cout << std::endl;
		}
#endif
		delete[] numb;
	}

	inline int nodesCmpZ(const void *nodeA, const void *nodeB)
	{
		real **coordsA = (real **) nodeA;
		real **coordsB = (real **) nodeB;
		real result = (*coordsA)[2] - (*coordsB)[2];
		return sign(result);
	}

} // namespace memorph
