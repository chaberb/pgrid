#include "GridFEAuxReader.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include "GridFEAux.h"

namespace memorph {

	GridFEAuxReader::GridFEAuxReader() { } 

	GridFEAuxReader::GridFEAuxReader(const std::string &prefix) 
	{ 
		setPath(prefix);
	}

	GridFEAuxReader::~GridFEAuxReader() { } 

	static const int exitErrCode = 1;

	void GridFEAuxReader::_open(const std::string &path, std::ifstream &input) const
	{
		input.open(path.c_str());

		if (!input.is_open()) {
			/* TODO - jakieś zwalnianie przed exit() */
			std::cerr 
				<< "Nie udało się otworzyć pliku " << path << " do odczytu"
				<< std::endl;
			exit(exitErrCode);
		} else {
			std::cout
				<< "Plik " << path << " został otwarty do odczytu"
				<< std::endl;
		}
	}

	void GridFEAuxReader::_close(std::ifstream &input) const
	{
		input.close();
		std::cout 
			<< "Odczyt z pliku zakończonył się powodzeniem"
			<< std::endl;
	}

	void GridFEAuxReader::_good(std::ifstream &input) const
	{
		if (!input.good()) {
			/* TODO - wyrzucić... chyba niemożliwe przy odczycie... */
			if (input.eof()) {
				std::cerr << "Nieoczekiwany koniec pliku" << std::endl;
			} else {
				std::cerr << "Błąd strumienia" << std::endl;
			}
			exit(exitErrCode);
		}
	}

	/* odczytuje współrzędne węzłów z pliku .node */
	bool GridFEAuxReader::readNodes(const std::string &path, GridFEAux *grid) const
	{
		std::ifstream input;

		_open(path, input);
		
		std::string line;
		std::string word;

		bool found = false;

		int noSlices, noPtsInSlice;
		/* odczytaj liczbę węzłów i wymiarów przestrzeni */
		while (!found) {
			
			/* przeczytaj niepustą linię */
			_readLine(input, line);

			/* podziel linię na słowa */
			std::stringstream words(line);

			/* odczytaj liczbę węzłów w pliku */
			words >> word;
			noSlices = atoi(word.c_str());
			words >> word;
			noPtsInSlice = atoi(word.c_str());

			grid->initNodes(noSlices, noPtsInSlice);
			std::cout << "Liczba węzłów: " << grid->noNodes() << std::endl;

			words >> word;
#if 0
			grid->setNoSpaceDim(atoi(word.c_str()));
#endif
			std::cout << "Liczba wymiarów przestrzeni: " << word << std::endl;

			found = true;
		}

		int noNodes = grid->noNodes();
		int noSpaceDim = grid->noSpaceDim();

		for (int i = 0; i < noNodes; ++i) {
			_readLine(input, line);
			std::stringstream words(line);
			
			words >> word;
			grid->nodeNr(i) = atoi(word.c_str());

			for (int j = 0; j < noSpaceDim; ++j) {
				words >> word;
				grid->coord(i, j) = atof(word.c_str());
			}
#if 0
			std::cout << "Node: " 
				<< grid->nodeNr(i) << " "
				<< grid->coords(i, 0) << " "
				<< grid->coords(i, 1) << " "
				<< grid->coords(i, 2) << std::endl;
#endif
		}
	
		_close(input);

		return true;
	}

	bool GridFEAuxReader::readElems(const std::string &path, GridFEAux *grid) const
	{
		std::ifstream input;

		_open(path, input);

		std::string line, word;
		int noElems;

		{
			_readLine(input, line);
			std::stringstream words(line);
			words >> word;
			noElems = atoi(word.c_str());
		}

		grid->initElems(noElems);
		std::cout << "Liczba elementów: " << grid->noElems() << std::endl;

		for (int i = 0; i < noElems; ++i) {
			_readLine(input, line);
			std::stringstream words(line);
			
			words >> word; 
			grid->elemNr(i) = atoi(word.c_str());

			words >> word;
			
			if (word == "ElmT3n2D") { 
				grid->initElemNodes(i, GridFE::T3n2D);
			} else if (word == "ElmT4n3D") {
				grid->initElemNodes(i, GridFE::T4n3D);
			} else {
				std::cout << "Nieznany typ elementu" << std::endl;
			}

#if 0
			words >> word;
			grid->elemSubdNr(i, atoi(word.c_str()));
#endif
			
			for (int j = 0; j < grid->noElemNodes(i); ++j) {
				words >> word;
				grid->elemNodeNr(i, j) = atof(word.c_str());
			}

#if 0
			std::cout << "Elem: "
				<< "form line: " << line << "\t"
				<< grid->elemNr(i) << " "
				<< grid->elemType(i) << " "
				<< grid->elemSubdNr(i) << " "
				<< grid->elemNodeNr(i, 0) << " "
				<< grid->elemNodeNr(i, 1) << " "
				<< grid->elemNodeNr(i, 2) << std::endl;
#endif
		}


		_close(input);

		return true;
	}

	bool GridFEAuxReader::readVects(const std::string &path, GridFEAux *grid) const
	{
		// TODO...
		return true;
	}

	bool GridFEAuxReader::readAxis(const std::string &path, GridFEAux *grid) const
	{
		std::ifstream input;

		_open(path, input);

		std::string line, word;
		int noAxisPts;

		{
			_readLine(input, line);
			std::stringstream words(line);
			words >> word;
			noAxisPts = atoi(word.c_str());
		}

//		grid->initAxisPts(noAxisPts, true);
		std::cout << "Liczba punktów osi: " << grid->noSlices() << std::endl;

		for (int i = 0; i < noAxisPts; ++i) {
			_readLine(input, line);
			std::stringstream words(line);
			
			words >> word;  // numer punktu - pomijany
//			grid->elemNr(i) = atoi(word.c_str());

			for (int j = 0; j < grid->noSpaceDim(); ++j) {
				words >> word;
				grid->axisPt(i, j) = atof(word.c_str());
			}
		}

		_close(input);

		return true;
	}

	bool GridFEAuxReader::_readLine(std::ifstream &input, std::string &line) const
	{
		bool empty = true;

		while (empty) {

			/* pobiera linię do stringa */
			getline(input, line, '\n');

			if (line.length() == 0) {
				continue;
			}

			size_t hash = line.find_first_of('#');
			size_t beg = line.find_first_not_of(" \t");
			size_t end = line.find_last_not_of(" \t", hash);

			if (beg >= end) {
				continue;
			}

			/* odcina białe znaki na początku i na końcu linii oraz wszystko
			 * co następuje po '#' (czyli komentarz) */
			line = line.substr(beg, end-beg + ((hash == std::string::npos) ? 1 : 0));

			if (line.length() > 0) {
				empty = false;
			}
		}

		return input.good() && !input.eof();
	}

	void GridFEAuxReader::setNodePath(const std::string &nodePath)
	{
		_nodePath = nodePath;
	}

	void GridFEAuxReader::setElemPath(const std::string &elemPath)
	{
		_elemPath = elemPath;
	}

	void GridFEAuxReader::setVectPath(const std::string &vectPath)
	{
		_vectPath = vectPath;
	}

	void GridFEAuxReader::setAxisPath(const std::string &axisPath)
	{
		_axisPath = axisPath;
	}

	void GridFEAuxReader::setPath(const std::string &pathPref)
	{
		setNodePath(pathPref + ".node");
		setElemPath(pathPref + ".elem");
		setVectPath(pathPref + ".vect");
		setAxisPath(pathPref + ".axis");
	}

	bool GridFEAuxReader::read(const std::string &pathPref, GridFEAux *grid)
	{
		setPath(pathPref);
		return read(grid);
	}

	bool GridFEAuxReader::read(GridFEAux *grid) const
	{
		if (!readNodes(_nodePath, grid)) {
			return false;
		}
		if (!readElems(_elemPath, grid)) {
			return false;
		}
		if (!readVects(_vectPath, grid)) {
			return false;
		}
		if (!readAxis(_axisPath, grid)) {
			return false;
		}
		return true;
	}

} // namespace memorph
