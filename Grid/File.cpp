#include "File.h"

namespace memorph {

	File::File():
			_path("") { }

	File::File(const std::string &path):
			_path(path) { }

	File::~File() { }

}
