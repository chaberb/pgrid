#include "ConfigReader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include "Config.h"

namespace memorph {

	ConfigReader::ConfigReader() { }

	ConfigReader::ConfigReader(const std::string &path)
	{ 
		setPath(path);
	}

	ConfigReader::~ConfigReader() { }

	static const int exitErrCode = 1;

	void ConfigReader::_open(const std::string &path, std::ifstream &input) const
	{
		input.open(path.c_str());

		if (!input.is_open()) {
			/* TODO - jakieś zwalnianie przed exit() */
			std::cerr 
				<< "Nie udało się otworzyć pliku " << path << " do odczytu"
				<< std::endl;
			exit(exitErrCode);
		} else {
			std::cout
				<< "Plik " << path << " został otwarty do odczytu"
				<< std::endl;
		}
	}

	void ConfigReader::_close(std::ifstream &input) const
	{
		input.close();
		std::cout 
			<< "Odczyt z pliku zakończonył się powodzeniem"
			<< std::endl;
	}

	void ConfigReader::_good(std::ifstream &input) const
	{
		if (!input.good()) {
			/* TODO - wyrzucić... chyba niemożliwe przy odczycie... */
			if (input.eof()) {
				std::cerr << "Nieoczekiwany koniec pliku" << std::endl;
			} else {
				std::cerr << "Błąd strumienia" << std::endl;
			}
			exit(exitErrCode);
		}
	}

	void ConfigReader::setPath(const std::string &path)
	{
		_path = path;
	}

	bool ConfigReader::read(const std::string &path, Config *config)
	{
		setPath(path);
		return read(config);
	}

	bool ConfigReader::read(Config *config) const
	{
		std::ifstream input;

		/* otwarcie pliku, wypisanie komunikatu i ewentualne zakończenie */
		_open(_path, input);

		/*  */
		std::string line; 
		std::string word;

		/* wczytanie liczbę pomiarów */
		int noMsrmnts;

		/* blok wewnętrzny na potrzeby zmiennej words - TODO*/
		{
			_readLine(input, line);
			std::stringstream words(line);
			
			words >> word;
			noMsrmnts = atoi(word.c_str());
		}

		config->init(noMsrmnts);

		std::cout << "Liczba pomiarów: " << config->noMsrmnts() << std::endl;

		/* wczytuje wszystkie pomiary */

		for (int i = 0; i < noMsrmnts; ++i) {
			/* pobiera linię i dzieli ją na słowa */	
			_readLine(input, line);
			std::stringstream words(line);
	
			/* pobiera numer pomiaru */
			words >> word;
//			std::cout << atoi(word.c_str());

			/* pobiera współrzędną na osi, wzdłuż której morfujemy */
			words >> word;
//			std::cout << atof(word.c_str());
			config->msrmnts(i, 0) =  atof(word.c_str());

			/* pobiera obwód przekroju poprzecznego modelu */
			words >> word;
//			std::cout << atof(word.c_str());
			config->msrmnts(i, 1) =  atof(word.c_str());

			_good(input);
		}

		_close(input);

		return true;
	}

	bool ConfigReader::_readLine(std::ifstream &input, std::string &line) const
	{
		bool empty = true;

		while (empty) {

			/* pobiera linię do stringa */
			getline(input, line, '\n');

			if (line.length() == 0) {
				continue;
			}

			size_t hash = line.find_first_of('#');
			size_t beg = line.find_first_not_of(" \t");
			size_t end = line.find_last_not_of(" \t", hash);

			if (beg >= end) {
				continue;
			}

			/* odcina białe znaki na początku i na końcu linii oraz wszystko
			 * co następuje po '#' (czyli komentarz) */
			line = line.substr(beg, end-beg + ((hash == std::string::npos) ? 1 : 0));

			if (line.length() > 0) {
				empty = false;
			}
		}

		return input.good() && !input.eof();
	}

} // namespace memorph
