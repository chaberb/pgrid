#ifndef GRIDFE_BOUNDER_H
#define GRIDFE_BOUNDER_H

#include "GridFE.h"

namespace memorph {

	class GridFEBounder: public GridFE {
	public:
		static const size_t facetMemSize = sizeof (int) * 6;

	private:
		int **_facets; // ściany: nr, nr-y węzłów, nr domeny, nr elementu
//		bool *_extFacets;
//		bool *_extNodes;
		int *_bndData; // nr-y zewnętrznych
		int _noFacets;

	public:
		GridFEBounder();
		~GridFEBounder();

		void initFacets(int noFacets);
		void freeFacets();
		void initBndData();
		void freeBndData();
		int countFacets() const;
		void sortFacets();
		void createFacets();
		void markExternals(); // oznacza zewnętrzne ściany i węzły
		void getBndGrid(GridFE *grid) const;

		int externalNode(int nodeIdx) const;
		bool externalFacet(int facetIdx) const;

		int elemNoFacets(int i) const;
		int noFacets() const;

		const int &facetNr(int facetIdx) const;
		const int &facetNodeNr(int facetIdx, int nodeIdx) const;
		const int &facetSubdNr(int facetIdx) const;
		const int &facetElemNr(int facetIdx) const;

		int &facetNr(int facetIdx);
		int &facetNodeNr(int facetIdx, int nodeIdx);
		int &facetSubdNr(int facetIdx);
		int &facetElemNr(int facetIdx);
	};

}

namespace memorph {

	inline int GridFEBounder::noFacets() const { return _noFacets; }

	inline const int &GridFEBounder::facetNr(int facetIdx) const
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][0];
	}

	inline const int &GridFEBounder::facetNodeNr(int facetIdx, int nodeIdx) const
	{
		assertArrIndex(facetIdx, _noFacets);
		assertArrIndex(nodeIdx, 3);
		return _facets[facetIdx][1+nodeIdx];
	}

	inline const int &GridFEBounder::facetSubdNr(int facetIdx) const
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][4];
	}

	inline const int &GridFEBounder::facetElemNr(int facetIdx) const
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][5];
	}

	inline int &GridFEBounder::facetNr(int facetIdx)
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][0];
	}

	inline int &GridFEBounder::facetNodeNr(int facetIdx, int nodeIdx)
	{
		assertArrIndex(facetIdx, _noFacets);
		assertArrIndex(nodeIdx, 3);
		return _facets[facetIdx][1+nodeIdx];
	}

	inline int &GridFEBounder::facetSubdNr(int facetIdx)
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][4];
	}

	inline int &GridFEBounder::facetElemNr(int facetIdx)
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][5];
	}

	inline int GridFEBounder::externalNode(int nodeIdx) const
	{
		assertArrIndex(nodeIdx, _noFacets);
		return _bndData[nodeIdx];
	}

	inline bool GridFEBounder::externalFacet(int facetIdx) const
	{
		assertArrIndex(facetIdx, _noFacets);
		return _facets[facetIdx][5]; // teraz 5. oznacza czy zewnętrzna
	}

}

#endif // GRIDFE_BOUNDER_H
