#include "GridFEAux.h"

namespace memorph {

	GridFEAux::GridFEAux():
		GridFE(),
		_axisPts(0),
		_vects(0),
		_noSlices(0),
		_noPtsInSlice(0)
	{
		_noSpaceDim = 3;
		_maxNoNodesInElem = 3;
		_sameElems = true;
	}

	GridFEAux::~GridFEAux()
	{
		// TODO - jakieś sprzątanie ???
	}

	void GridFEAux::init(int noSlices, int noPtsInSlice)
	{
		initNodes(noSlices, noPtsInSlice);
		initElems((noSlices - 1) * noPtsInSlice);
	}

	void GridFEAux::free()
	{
		freeNodes();
		freeElems();
	}

	void GridFEAux::initNodes(int noSlices, int noPtsInSlice)
	{
		if (_coords != 0 || _vects != 0 || _axisPts != 0) {
			freeNodes();
		}
		
		int noNodes = noSlices * noPtsInSlice;

		_coords = new real *[noNodes];
		_vects = new real *[noNodes];
		_nodes = new int *[noNodes];

		for (int i = 0; i < noNodes; ++i) {
			_coords[i] = new real[_noSpaceDim];
			_vects[i] = new real[_noSpaceDim];
			_nodes[i] = new int[noNodeParams];

			memset(_nodes[i], 0, sizeof(int) * noNodeParams);
		}

		_axisPts = new real *[noSlices];

		for (int i = 0; i < noSlices; ++i) {
			_axisPts[i] = new real[_noSpaceDim];
		}

		_noNodes = noNodes;
		_noSlices = noSlices;
		_noPtsInSlice = noPtsInSlice;
	}

	void GridFEAux::freeNodes()
	{
		for (int i = 0; i < _noNodes; ++i) {
			delete[] _nodes[i];
			delete[] _coords[i];
			delete[] _vects[i];
		}

		delete[] _nodes;
		delete[] _coords;
		delete[] _vects;
		
		_nodes = 0;
		_coords = 0;
		_vects = 0;

		for (int i = 0; i < _noSlices; ++i) {
			delete[] _axisPts[i];
		}
	
		delete[] _axisPts;

		_axisPts = 0;

		_noNodes = 0;
		_noSlices = 0;
		_noPtsInSlice = 0;

	}

	void GridFEAux::initElems(int noElems)
	{
		if (_elems != 0 || _elemsNodes != 0) {
			freeElems();
		}

		_elemsNodes = new int *[noElems];
		_elems = new int *[noElems];

		memset(_elemsNodes, 0, sizeof(int *) * noElems);
		
		for (int i = 0; i < noElems; ++i) {
			_elems[i] = new int[noElemParams];
			memset(_elems[i], 0, sizeof(int) * noElemParams);
		}

		_noElems = noElems;
	}

	void GridFEAux::freeElems()
	{
		for (int i = 0; i < _noElems; ++i) {
			delete[] _elemsNodes[i];
		}

		delete[] _elemsNodes;	
		_elemsNodes = 0;
		_noElems = 0;
	}

	void GridFEAux::getSrcGrid(GridFE *grid)
	{
		grid->initNodes(_noNodes);
		for (int i = 0; i < _noNodes; ++i) {
			grid->nodeNr(i) = nodeNr(i);
			for (int j = 0; j < _noSpaceDim; ++j) {
				grid->coord(i, j) = coord(i, j);
			}
		}

		grid->initElems(_noElems);
		
		for (int i = 0; i < _noElems; ++i) {
			grid->elemNr(i) = elemNr(i);
			grid->elemSubdNr(i) = 1;//elemSubdNr(i);
			grid->initElemNodes(i, elemType(i));
			for (int j = 0; j < noElemNodes(i); ++j) {
				grid->elemNodeNr(i, j) = elemNodeNr(i, j);
			}
		}
	}

	void GridFEAux::getDstGrid(GridFE *grid)
	{
		grid->initNodes(_noNodes);
		for (int i = 0; i < _noNodes; ++i) {
			grid->nodeNr(i) = nodeNr(i);
			for (int j = 0; j < _noSpaceDim; ++j) {
				grid->coord(i, j) = coord(i, j) + vect(i, j);
			}
		}

		grid->initElems(_noElems);
		
		for (int i = 0; i < _noElems; ++i) {
			grid->elemNr(i) = elemNr(i);
			grid->elemSubdNr(i) = 1;//elemSubdNr(i);
			grid->initElemNodes(i, elemType(i));
			for (int j = 0; j < noElemNodes(i); ++j) {
				grid->elemNodeNr(i, j) = elemNodeNr(i, j);
			}
		}
	}

} // namespace memorph
