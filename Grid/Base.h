#ifndef BASE_H
#define BASE_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include "Math.h"

#define assertInRange(i, lb, ub) \
	if (((i) < (lb)) || ((i) > (ub))) {\
		std::cerr \
			<< __FILE__ << "(" << __LINE__ << "):\n\t" \
			<< "Wartość " << (i) \
			<< " nie mieści się w przedziale <" \
			<< (lb) << ", " << (ub) << ">\n"; \
                abort(); \
	}

#define assertArrIndex(i, arrSize) \
	assertInRange((i), 0, (arrSize))

#define assertNotNull(ptr) \
	if ((ptr) == 0) {\
		std::cerr \
			<< __FILE__ << "(" << __LINE__ << "):\n\t" \
			<< "Wskaźnik " << #ptr \
			<< " == NULL\n"; \
		abort(); \
	}

#define assertNoEqual(x, y) \
	if ((x) == (y)) {\
		std::err \
			<< __FILE__ << "(" << __LINE__ << "):\n\t" \
			<< #x << " == " << #y << std::endl; \
	}

#define assertEqual(x, y) \
	if ((x) != (y)) { \
		std::cerr \
			<< __FILE__ << "(" << __LINE__ << "):\n\t" \
			<< "Wartości " << #x << " i " << #y \
			<< " nie są równe" << std::endl; \
		abort(); \
	}
 

/* nakładki na funkcja obsługujące pamięć
 * - wykonują rzutowanie
 */
#define memAlloc(ptr, memSize) \
	memorph::mem_alloc((void **)(ptr), memSize)

#define memRealloc(ptr, newMemSize, oldMemSize) \
	memorph::mem_realloc((void **)(ptr), newMemSize, oldMemSize)

#define memFree(ptr) \
	memorph::mem_free((void **)(ptr))

namespace memorph {

	inline void mem_alloc(void **ptr, size_t memSize)
	{
		*ptr = malloc(memSize);
		memset(*ptr, 0, memSize);
		assertNotNull(*ptr);
	}

	inline void mem_realloc(void **ptr, size_t newMemSize, size_t oldMemSize)
	{
		*ptr = realloc(*ptr, newMemSize);
		if (newMemSize > oldMemSize) {
//			memset(*ptr + oldMemSize, 0, newMemSize - oldMemSize);
		}
		assertNotNull(*ptr);
	}

	inline void mem_free(void **ptr)
	{
		free(*ptr);
		*ptr = 0;
	}

#if 0
	class Mem {
	public:
		static void *alloc(size_t memSize);
		static void *realloc(size_t memSize, size_t oldMemSize = 0);
		static void *free(void *ptr);
	};

	inline void *Mem::alloc(size_t memSize)
	{

	}

	inline void *realloc(void *ptr)
	{

	}

	inline void 
#endif
}

#endif // BASE_H
