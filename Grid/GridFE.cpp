#include "GridFE.h"
#include <cstdio>
namespace memorph {

	GridFE::GridFE():
		_coords(0),
		_nodes(0),
		_nodesBndInds(0),
		_elems(0),
		_elemsNodes(0),
		_bnds(0),
		_bndsNames(0),
		_noNodes(0),
		_noElems(0),
		_noBnds(0),
		_noSubds(0),
		_noSpaceDim(3),
		_maxNoNodesInElem(0),
		_sameElems(true),
		_onlyOneSubd(false) { }

	GridFE::~GridFE()
	{
//		log("~GridFE()");
		free();
	}

	void GridFE::init(int noNodes, int noElems, int noBnds)
	{
		initNodes(noNodes);
		initElems(noElems);
		initBnds(noBnds);
//		initSubds();
	}

	void GridFE::free()
	{
		freeNodes();
		freeElems();
		freeBnds();
//		freeSubds();
	}

	void GridFE::copy(GridFE *out) const
	{
		out->initNodes(_noNodes);
		for (int i = 0; i < _noNodes; ++i) {
			out->nodeNr(i) = nodeNr(i);
			out->initNodeBndInds(i, noNodeBndInds(i));
			for (int j = 0; j < noNodeBndInds(i); ++j) {
				out->nodeBndInd(i, j) = nodeBndInd(i, j);
			}
			for (int j = 0; j < _noSpaceDim; ++j) {
				out->coord(i, j) = coord(i, j);
			}
		}
		out->initElems(_noElems);
		for (int i = 0; i < _noElems; ++i) {
			out->elemNr(i) = elemNr(i);
			out->elemSubdNr(i) = elemSubdNr(i);
			int type = noElemNodes(i);
			out->initElemNodes(i, type);
			for (int j = 0; j < type; ++j) {
				out->elemNodeNr(i, j) = elemNodeNr(i, j);
			}
		}
		out->initBnds(_noBnds);
		for (int i = 0; i < _noBnds; ++i) {
			out->bndNr(i) = bndNr(i);
			out->bndName(i, bndName(i));
		}
		out->initSubds(_noSubds);
	}

	void GridFE::assign(const GridFE *grid, const real **coords)
	{
		init(grid->noNodes(), grid->noElems(), grid->noBnds());

		_noSpaceDim = grid->noSpaceDim();

		/* przypisanie powierzchni */
		for (int i = 0; i < _noBnds; ++i) {
			bndNr(i) = grid->bndNr(i);
			bndName(i, grid->bndName(i));
		}

		/* przypisanie węzłów */
		int noBndInds;

		/* jeżeli nie podano nowej tablicy współrzędnych i jest co kopiować (siatka ma niezerową liczbę węzłów), to przypisuje współrzędne z siatki */
		if (_coords == 0 && _noNodes > 0) {
			_coords = (real **) &grid->coord(0, 0);
		}

		for (int i = 0; i < _noNodes; ++i) {
			nodeNr(i) = grid->nodeNr(i);
			noBndInds = grid->noNodeBndInds(i);
			initNodeBndInds(i, noBndInds);
			for (int j = 0; j < noNodeBndInds(i); ++j) {
				nodeBndInd(i, j) = grid->nodeBndInd(i, j);
			}
			for (int j = 0; j < _noSpaceDim; ++j) {
				_coords[i][j] = coords[i][j];
			}
		}

		/* przypisanie elementów */
		int type;

		for (int i = 0; i < _noElems; ++i) {
			elemNr(i) = grid->elemNr(i);
			elemSubdNr(i) = grid->elemSubdNr(i);
			type = grid->noElemNodes(i);
			initElemNodes(i, type);
			for (int j = 0; j < type; ++j) {
				elemNodeNr(i, j) = grid->elemNodeNr(i, j);
			}
		}
	}

	void GridFE::renumber(const int *nodesNumb, GridFE *grid) const
	{
		/* tablica odwrotna do renumaracji węzłów w elementach */
		int *nodesInv = new int[_noNodes];
	
		for (int i = 0; i < _noNodes; ++i) {
			std::cout << nodesNumb[i] << std::endl;
		}

		/* wypełnia tablicę odwrotną */
		for (int i = 0; i < _noNodes; ++i) {
			nodesInv[nodesNumb[i]-1] = i + 1;
		}

		/* przypisuje węzły w nowej kolejności */
		int nodeIdx, noBndInds;
		grid->initNodes(_noNodes);
		for (int i = 0; i < _noNodes; ++i) {
			nodeIdx = nodesNumb[i] - 1;
			grid->nodeNr(i) = i+1;//nodesNumb[i];
			noBndInds = noNodeBndInds(nodeIdx);
			grid->initNodeBndInds(i, noBndInds);
			for (int j = 0; j < noBndInds; ++j) {
				grid->nodeBndInd(i, j) = nodeBndInd(nodeIdx, j);
			}
			for (int j = 0; j < _noSpaceDim; ++j) {
				grid->coord(i, j) = coord(nodeIdx, j);
			}
		}

		/* przepisuje elementy uwzględniając nową numerację węzłów */
		grid->initElems(_noElems);
		for (int i = 0; i < _noElems; ++i) {
			grid->elemNr(i) = elemNr(i);
			grid->elemSubdNr(i) = elemSubdNr(i);
			int type = noElemNodes(i);
			grid->initElemNodes(i, type);
			for (int j = 0; j < type; ++j) {
				grid->elemNodeNr(i, j) = nodesInv[elemNodeNr(i, j)-1];
			}
		}

		/* przepisuje ograniczenia */
		grid->initBnds(_noBnds);
		for (int i = 0; i < _noBnds; ++i) {
			grid->bndNr(i) = bndNr(i);
			grid->bndName(i, bndName(i));
		}

		grid->initSubds(_noSubds);
	}

	void GridFE::copyCoords(int limit, real **coords) const
	{
		assertNotNull(coords);
		for (int i = 0; i < limit && i < _noNodes; ++i) {
			assertNotNull(coords[i]);
			memcpy(coords[i], _coords[i], sizeof(int) * _noSpaceDim);
			/* dodatkowo kopiowany jest numer węzła */
			coords[i][_noSpaceDim] = _nodes[i][0];
		}
	}

	void GridFE::initNodes(int noNodes)
	{
		if (_nodes != 0) {
			freeNodes();
		}

		_coords = new real *[noNodes];
		_nodes = new int *[noNodes];
		_nodesBndInds = new int *[noNodes];

		memset(_nodesBndInds, 0, sizeof(int *) * noNodes);

		for (int i = 0; i < noNodes; ++i) {
			_coords[i] = new real[_noSpaceDim];
			_nodes[i] = new int[noNodeParams];
			memset(_nodes[i], 0, sizeof(int) * noNodeParams);
		}

		_noNodes = noNodes;
	}

	void GridFE::initElems(int noElems)
	{
		if (_elems != 0) {
			freeElems();
		}

		_elems = new int *[noElems];
		_elemsNodes = new int *[noElems];

		memset(_elemsNodes, 0, sizeof(int *) * noElems);

		for (int i = 0; i < noElems; ++i) {
			_elems[i] = new int[noElemParams];
		}

		_noElems = noElems;
	}

	void GridFE::initBnds(int noBnds)
	{
		if (_bnds != 0) {
			freeBnds();
		}

		_bnds = new int *[noBnds];
		_bndsNames = new char *[noBnds];

		for (int i = 0; i < noBnds; ++i) {
			_bnds[i] = new int [noBndParams];
		}

		_noBnds = noBnds;
	}

	void GridFE::initSubds(int noSubds)
	{
		_noSubds = noSubds;
		// TODO...
	}

	void GridFE::freeNodes()
	{
                for (int i = 0; i < _noNodes; ++i) {
			delete[] _nodesBndInds[i];
			delete[] _nodes[i];
			delete[] _coords[i];
		}

		delete[] _nodesBndInds;
		delete[] _nodes;
		delete[] _coords;

		_nodesBndInds = 0;
		_nodes = 0;
		_coords = 0;

		_noNodes = 0;
	}

	void GridFE::freeElems()
	{
		for (int i = 0; i < _noElems; ++i) {
			delete[] _elems[i];
			delete[] _elemsNodes[i];
		}

		delete[] _elems;
		delete[] _elemsNodes;

		_elems = 0;
		_elemsNodes = 0;

		_noElems = 0;
	}

	void GridFE::freeBnds()
	{
		for (int i = 0; i < _noBnds; ++i) {
			delete[] _bnds[i];
			delete[] _bndsNames[i];
		}

		delete[] _bnds;
		delete[] _bndsNames;

		_bnds = 0;
		_bndsNames = 0;

		_noBnds = 0;
	}

	void GridFE::freeSubds()
	{
		_noSubds = 0;
		// TODO...
	}

}
