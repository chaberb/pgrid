#include "GridFEWrapper.h"
#include "GridFE.h"
#include "GridFEAux.h"
#include "GridFESorter.h"

namespace memorph {

	GridFEWrapper::GridFEWrapper():
		_grid(0),
		_nodes(0),
		_noNodes(0),
		_noSlices(16),
		_noPtsInSlice(8),
		_lowLimit(0),
		_uppLimit(0) { }

	GridFEWrapper::~GridFEWrapper() 
	{ 
		delete _lowLimit;
		delete _uppLimit;
		_lowLimit = 0;
		_uppLimit = 0;
	}

	void GridFEWrapper::setGrid(GridFE *grid) { _grid = grid; }
	void GridFEWrapper::setNoSlices(int noSlices) { _noSlices = noSlices; }
	void GridFEWrapper::setNoPtsInSlice(int noPtsPerSlice) { _noPtsInSlice = noPtsPerSlice; } 

	void GridFEWrapper::setLimits(real lower, real upper)
	{
		if (lower == upper) {
			abort();
		}

		_lowLimit = new real;	
		_uppLimit = new real;

		*_uppLimit = (lower > upper) ? lower : upper;
		*_lowLimit = (lower > upper) ? upper : lower;
	}

	inline int GridFEWrapper::noElemEdges(int elemIdx) const
	{
		int type = _grid->elemType(elemIdx);
		switch (type) {
			case GridFE::T3n2D:
				return 3;
			case GridFE::T4n3D:
				return 6;
		}
		return 0;
	}

	int GridFEWrapper::countEdges() const
	{
		int noElems = _grid->noElems();
		int noEdges = 0;
		for (int i = 0; i < noElems; ++i) {
			noEdges += noElemEdges(i);
		}
		return noEdges;
	}

	inline int edgesCmpFunc(const void *a, const void *b)
	{
		int **n1 = (int **)a;
		int **n2 = (int **)b;
		int i = 0, temp;
		while (i < 2) {
			temp = (*n1)[i] - (*n2)[i];
			if (temp != 0)
				return temp;
			i++;
		}
		return 0;
	}

	inline bool edgesEqual(int *e1, int *e2)
	{
		return e1[0] == e2[0] && e1[1] == e2[1];
	}

	void GridFEWrapper::wrap(GridFEAux *grid, real eps)
	{
		_eps = eps;
		_createAuxData();
		_wrap(grid);
	}

	void GridFEWrapper::_wrap(GridFEAux *grid)
	{
		if (_lowLimit == 0 || _uppLimit == 0) {
			_computeLimits();
		}

		int noSlices = _noSlices;
		int noPtsPerSlice = _noPtsInSlice;

		real eps = _eps;

		real begSliceCoord = *_lowLimit + eps;
		real endSliceCoord = *_uppLimit - eps;

		real dSliceCoord = (endSliceCoord - begSliceCoord) / (noSlices - 1);
		real dAngle = 2 * M_PI / noPtsPerSlice;
		

		grid->initNodes(noSlices + 2, noPtsPerSlice);

		/* robimy węzły */
		/****************/

		/* obliczone punkty przecięcia - potencjalnie wszystkie krawędzie mogą się przecinać (alokacja) */
		real **intersects = (real **) malloc(sizeof(real *) * _noEdges);


		for (int i = 0; i < noSlices; ++i) 
		{
			real currSliceCoord = begSliceCoord + dSliceCoord * i;

			/* liczba znalezionych punktów przecięcia krawędzi z aktualnym przekrojem */
			int noIntersects = 0;

			/* pętla iteruje po wszystkich unikalnych krawędziach w siatce i znajduje punkty przecięcia */
			for (int j = 0; j < _noEdges; ++j) {

				/* współrzędne z końców sprawdzanej krawędzi */
				real edgeBegCoord = _nodes[_edges[j][0]-1][2];
				real edgeEndCoord = _nodes[_edges[j][1]-1][2];

				/* czy prosta nie leży na sprawdzanej płaszczyźnie */
				if (edgeBegCoord == edgeEndCoord) { // TODO - pogrubić płaszczyznę
					/* nie możemy znaleźć jej przecięcia z płaszczyzną */
					continue;
				}

				/* czy prosta przecina płaszczyznę */
				if (edgeBegCoord <= currSliceCoord && edgeEndCoord >= currSliceCoord) {
					/* kopiuje współrzędne do postaci zrozumiałej dla funkcji */
					real begCoords[3];
					real endCoords[3];
					
					begCoords[0] = _nodes[_edges[j][0]-1][0];
					begCoords[1] = _nodes[_edges[j][0]-1][1];
					begCoords[2] = _nodes[_edges[j][0]-1][2];

					endCoords[0] = _nodes[_edges[j][1]-1][0];
					endCoords[1] = _nodes[_edges[j][1]-1][1];
					endCoords[2] = _nodes[_edges[j][1]-1][2];
				
					/* wylicza punkt przecięcia */
					intersects[noIntersects] = (real *) malloc(sizeof(real) * 2);
					intersectionZ(begCoords, endCoords, currSliceCoord, intersects[noIntersects]);

					++noIntersects;
				}


			}

#if 1
			if (noIntersects == 0) {
				abort();
			}
#endif

			/* wylicza środek ciężkości na podstawie znalezionych punktów */
			real center[2];
			barycenter2D(intersects, noIntersects, center);
			grid->setAxisPt(i + 1, center[0], center[1], currSliceCoord);
				
			/* przesuwa punkty do układu współrzędnych środka ciężkości */
			real centerInv[2];
			inverse2D(center, centerInv);
			translate2D(intersects, noIntersects, center);

			/* sortuje znalezione punkty */
			sort2D(intersects, noIntersects); // TODO - wokół znalezionego środka (przekazać tam center i odjąć w funkcji)


			/* wyliczanie punktów przecięcia */

			/* indeksy końców krawędzi w ramach wielokąta */
			int edgeBegIdx = noIntersects - 1;
			int edgeEndIdx = 0;

			real centrCoords[2];
			centrCoords[0] = 0;//c.z;
			centrCoords[1] = 0;//c.y;

			/* promień wodzący - opisywany przez wektor jednostkowy */
			real rayCoords[2];

			/* współrzędne końców krawędzi */
			real edgeBegCoords[2];
			real edgeEndCoords[2];

			/* punkt przecięcia promienia z otoczką */
			real intrCoords[2];

			/* wartości funkcji alpha - do porównywania kątów */
			real rayAlpha, edgeBegAlpha, edgeEndAlpha;

			/* pętla znajduje punkty przecięcia promienia wodzącego z krawędziami wielościanu */
			for (int currAngleIdx = 0; currAngleIdx < noPtsPerSlice; ) {

				real currAngle = currAngleIdx * dAngle;
				rayCoords[0] = cos(currAngle); // TODO - center
				rayCoords[1] = sin(currAngle); // TODO - center

				edgeBegCoords[0] = intersects[edgeBegIdx][0];
				edgeBegCoords[1] = intersects[edgeBegIdx][1];
				edgeEndCoords[0] = intersects[edgeEndIdx][0];
				edgeEndCoords[1] = intersects[edgeEndIdx][1];

				/* czy promień wodzący przetnie krawędź wielościanu */
				rayAlpha = alpha(rayCoords[0], rayCoords[1]);
				edgeBegAlpha = alpha(edgeBegCoords[0], edgeBegCoords[1]);
				edgeEndAlpha = alpha(edgeEndCoords[0], edgeEndCoords[1]);

				/* TODO - dopracować to (chodzi o to, żeby dało się porównywać ze sobą kąty z 1. i 4. ćwiartki */
				if ((edgeBegIdx == noIntersects-1) && (edgeEndIdx == 0)) {
					if (/* TODO - dopracować warunek */ (rayAlpha + 4) < 5) {
						rayAlpha += 4;
					}
					edgeEndAlpha += 4;
				}

				if ((rayAlpha > edgeBegAlpha && rayAlpha < edgeEndAlpha)) {
					/* promień przecina aktualną krawędź wielokąta */
					segmIntr(centrCoords, rayCoords, edgeBegCoords, edgeEndCoords, intrCoords);
				} else if (edgeBegAlpha == edgeEndAlpha) {
					/* oba końce krawędzi znajdują się w tym samym punkcie - olać aktualny koniec krawędzi i przejść do następnego */
					edgeBegIdx += 1;
					continue;
				} else if (rayAlpha > edgeEndAlpha) {
					/* promień wyszedł poza krawędź - należy wziąć następną */					
					edgeBegIdx = (edgeEndIdx);
					edgeEndIdx = (edgeEndIdx + 1) % noIntersects;
					continue;
				} else if (rayAlpha < edgeBegAlpha) {
					/* TODO - taka sytuacja nie może wystąpić !!! */
					abort();
				} else if (rayAlpha == edgeBegAlpha) {
					/* początek krawędzi leży na promieniu - nie trzeba nic liczyć */
					intrCoords[0] = edgeBegCoords[0];
					intrCoords[1] = edgeBegCoords[1];
				} else if (rayAlpha == edgeEndAlpha) {
					/* koniec krawędzi leży na promieniu - j.w. */
					intrCoords[0] = edgeEndCoords[0];
					intrCoords[1] = edgeEndCoords[1];
				} else {
					/* TODO - taka też nie może !!! */
					abort();
				}

				int nodeIdx = (i + 1) * noPtsPerSlice + currAngleIdx;
				grid->nodeNr(nodeIdx) = nodeIdx + 1;
				grid->setCoords(nodeIdx, intrCoords[0] + center[0], intrCoords[1] + center[1], currSliceCoord);
//				grid->initNodeBndInds(nodeIdx, 0); // TODO - zrobić f-cję, która explicite powie, że nie ma wskaźników powierzchni
//				grid->coord(nodeIdx, x, intrCoords[1] + c.y, intrCoords[0] + c.z);
				++currAngleIdx;
			}
		}

		/* dosztukowujemy węzły */
		/************************/

		{
			real currSliceCoord = begSliceCoord - 2 * eps;
			for (int currAngleIdx = 0; currAngleIdx < noPtsPerSlice; ++currAngleIdx) 
			{
				int nodeIdx = currAngleIdx;
				int copyIdx = noPtsPerSlice + currAngleIdx;

				real x = grid->coord(copyIdx, 0);
				real y = grid->coord(copyIdx, 1);
				real z = currSliceCoord;

				grid->nodeNr(nodeIdx) = nodeIdx + 1;
				grid->setCoords(nodeIdx, x, y, z);
				grid->setAxisPt(0, grid->axisPt(1, 0), grid->axisPt(1, 1), z);
			}
		}

		{
			real currSliceCoord = endSliceCoord + 2 * eps;
			for (int currAngleIdx = 0; currAngleIdx < noPtsPerSlice; ++currAngleIdx) 
			{
				int nodeIdx = (noSlices + 1) * noPtsPerSlice + currAngleIdx;
				int copyIdx = (noSlices + 0) * noPtsPerSlice + currAngleIdx;

				real x = grid->coord(copyIdx, 0);
				real y = grid->coord(copyIdx, 1);
				real z = currSliceCoord;

				grid->nodeNr(nodeIdx) = nodeIdx + 1;
				grid->setCoords(nodeIdx, x, y, z);
				grid->setAxisPt(noSlices + 1, grid->axisPt(noSlices, 0), grid->axisPt(noSlices, 1), z);
			}
		}

		
		/* robimy trójkąty */
		/*******************/
		noSlices += 2;
		int noTriangles = (2 * noPtsPerSlice) * (noSlices - 1);
		grid->initElems(noTriangles);

		for (int sliceIdx = 0; sliceIdx < noSlices-1; sliceIdx++) {
			for (int ptIdx = 0; ptIdx < noPtsPerSlice-1; ptIdx++) {
				/* wypełniamy 1. trójkąt */
				int baseNodeIdx = (sliceIdx * noPtsPerSlice) + ptIdx;
				int elemIdx = 2 * baseNodeIdx;
				grid->elemNr(elemIdx) = elemIdx + 1;
				grid->createT3n2D(elemIdx);
				grid->elemSubdNr(elemIdx) = 1;
				grid->elemNodeNr(elemIdx, 2) = baseNodeIdx + (1);
				grid->elemNodeNr(elemIdx, 1) = baseNodeIdx + 1 + (1);
				grid->elemNodeNr(elemIdx, 0) = baseNodeIdx + noPtsPerSlice + (1);
				/* wypełniamy 2. trójkąt */
				elemIdx += 1;
				grid->elemNr(elemIdx) = elemIdx + 1;
				grid->createT3n2D(elemIdx);
				grid->elemSubdNr(elemIdx) = 1;
				grid->elemNodeNr(elemIdx, 2) = baseNodeIdx + 1 + (1);
				grid->elemNodeNr(elemIdx, 1) = baseNodeIdx + noPtsPerSlice + 1 + (1);
				grid->elemNodeNr(elemIdx, 0) = baseNodeIdx + noPtsPerSlice +  (1);
			}
			int baseNodeIdx = (sliceIdx + 1) * noPtsPerSlice - 1;
			int elemIdx = 2 * baseNodeIdx;
			/* TODO - przerzucić to do pętli */
			grid->elemNr(elemIdx) = elemIdx + 1;
			grid->createT3n2D(elemIdx);
			grid->elemSubdNr(elemIdx) = 1;
			grid->elemNodeNr(elemIdx, 2) = baseNodeIdx + (1);
			grid->elemNodeNr(elemIdx, 1) = sliceIdx * noPtsPerSlice + (1);
			grid->elemNodeNr(elemIdx, 0) = baseNodeIdx + noPtsPerSlice + (1);
			/* wypełniamy 2. trójkąt */
			elemIdx += 1;
			grid->elemNr(elemIdx) = elemIdx + 1;
			grid->createT3n2D(elemIdx);
			grid->elemSubdNr(elemIdx) = 1;
			grid->elemNodeNr(elemIdx, 2) = sliceIdx * noPtsPerSlice + (1);
			grid->elemNodeNr(elemIdx, 1) = (sliceIdx + 1) * noPtsPerSlice + (1);
			grid->elemNodeNr(elemIdx, 0) = (sliceIdx + 2) * noPtsPerSlice -1 + (1);
		}
	}

	void GridFEWrapper::_computeLimits()
	{
		assertNotNull(_grid);

		int noNodes = _grid->noNodes();
		real currCoord;
		real minCoord = _grid->coord(0, 2);
		real maxCoord = _grid->coord(0, 2);
		for (int i = 1; i < noNodes; ++i) {
			currCoord = _grid->coord(i, 2);
			minCoord = (currCoord < minCoord) ? currCoord : minCoord;
			maxCoord = (currCoord > maxCoord) ? currCoord : maxCoord;
		}

		setLimits(minCoord, maxCoord);

		std::cout 
			<< "Współrzędne płaszczyzn ograniczajacych: " 
			<< minCoord << " "
			<< maxCoord << std::endl;
	}

	void GridFEWrapper::_createAuxData()
	{
		/**** ROBIMY POSORTOWANĄ TABLICĘ WĘZŁÓW ****/

		GridFESorter *gridSorter = new GridFESorter();
		GridFE *sorted = new GridFE();
		gridSorter->setGrid(_grid);
		gridSorter->sort(sorted);
		delete gridSorter;

		_grid = sorted;
		
		int noNodes = _grid->noNodes();
		_nodes = new real *[noNodes];

		for (int i = 0; i < noNodes; ++i) {
			_nodes[i] = new real[3];
			for (int j = 0; j < 3; ++j) {
				_nodes[i][j] = _grid->coord(i, j);
//				std::cout << _nodes[i][j] << " ";
			}
//			std::cout << std::endl;
		}

		_noNodes = noNodes;

//		delete sorted;
	
		/**** ROBIMY LISTĘ KRAWĘDZI ****/

		int noEdges = countEdges();
		int noElems = _grid->noElems();
		const size_t edgeMemSize = sizeof(int) * 2;
		int temp;

		/* tablica edges temp zawiera wskaźniki do 2-wymiarowych tablic 
		 * początek i koniec krawędzi posortowane po numerze wierzchołków */
		int **edgesTemp = (int **) malloc(sizeof(int *) * noEdges);
		memset(edgesTemp, 0, sizeof(int *) * noEdges); // TODO - wywalić

		/* iterujemy po każdym elemencie i wybieramy z niego pary
		 * wierzchołków bez powtórzeń */
		for (int i = 0, j = 0; i < noElems; ++i) {
			int noElemNodes = _grid->noElemNodes(i);
			for (int k = 0; k < noElemNodes; ++k) {
				for (int l = k+1; l < noElemNodes; ++l) {
					edgesTemp[j] = (int *) malloc(edgeMemSize);
					memset(edgesTemp[j], 0, edgeMemSize); // TODO - wywalić
					edgesTemp[j][0] = _grid->elemNodeNr(i, k);
					edgesTemp[j][1] = _grid->elemNodeNr(i, l);
					if (edgesTemp[j][0] > edgesTemp[j][1]) {
						temp = edgesTemp[j][0];
						edgesTemp[j][0] = edgesTemp[j][1];
						edgesTemp[j][1] = temp;
					}
					j++;
				}
			}
		}

		// sortujemy krawędzie po numerach węzłów
		qsort(edgesTemp, noEdges, sizeof(int *), edgesCmpFunc);

		int **uniqEdges = (int **) malloc(sizeof(int *) * noEdges);

		int i = 0, noUniqEdges = 0;
		while (i < noEdges) {
			uniqEdges[noUniqEdges] = (int *) malloc(sizeof(int) * 2);
			memcpy(uniqEdges[noUniqEdges], edgesTemp[i], sizeof(int) * 2);	
			while (i < noEdges) {
				if (edgesEqual(uniqEdges[noUniqEdges], edgesTemp[i])) {
					i++;
				} else {
					break;
				}
			}
			noUniqEdges++;
		} 

		uniqEdges = (int **) realloc(uniqEdges, sizeof(int *) * noUniqEdges);
		
		for (int i = 0; i < noEdges; ++i) {
			free(edgesTemp[i]);
		}
		free(edgesTemp);

		_edges = uniqEdges;
		_noEdges = noUniqEdges;
	}

	void GridFEWrapper::_freeAuxData()
	{
		for (int i = 0; i < _grid->noNodes(); ++i) {
			delete[] _nodes[i];
		}

		delete[] _nodes;

		for (int i = 0; i < _noEdges; ++i) {
			delete[] _edges[i];
		}

		delete _edges;

		delete _lowLimit;
		delete _uppLimit;

		_lowLimit = 0;
		_uppLimit = 0;

		_nodes = 0;
		_edges = 0;
		_noNodes = 0;
		_noEdges = 0;

	}

} // namespace memorph
