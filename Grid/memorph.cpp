#include <iostream>
#include <string>

#include "GridFE.h"
#include "GridFESorter.h"
#include "GridFEReader.h"
#include "GridFEWriter.h"
#include "GridFEAux.h"
#include "GridFEAuxReader.h"
#include "Config.h"
#include "ConfigReader.h"
#include "MorpherCPU.h"

using namespace memorph;

int main(int argc, char **argv)
{
	if (argc < 2) {
		std::cout << argv[0] << " - morfuje" << std::endl;
		std::cout << "Argumenty: " << std::endl;
		std::cout << "\t" << "argv[1] - siatka wejściowa" << std::endl;
		std::cout << "\t" << "argv[2] - siatka pomocnicza" << std::endl;
		std::cout << "\t" << "argv[3] - plik z pomiarami" << std::endl;
		std::cout << "\t" << "argv[4] - siatka wyjściowa" << std::endl;
	}
#if 0
			std::cout 
				<< "# GridFE (\"" << argv[2] << "\")\n";
#endif

	GridFE *grid = new GridFE();
	GridFEReader *gridReader = new GridFEReader(argv[1]);
	gridReader->read(grid);
	delete gridReader;

	GridFEAux *gridAux = new GridFEAux();
	GridFEAuxReader *gridAuxReader = new GridFEAuxReader(argv[2]);
	gridAuxReader->read(gridAux);
	delete gridAuxReader;

	Config *config = new Config();
	ConfigReader *configReader = new ConfigReader(argv[3]);
	configReader->read(config);
	delete configReader;

	Morpher *morpher = new Morpher();
	morpher->setConfig(config);
	morpher->setAuxGrid(gridAux);
	morpher->morph(grid);
	delete morpher;

	GridFEWriter *gridWriter = new GridFEWriter(argv[4]);
	gridWriter->write(*grid);
	delete gridWriter;

	GridFE *srcGrid = new GridFE();
	GridFE *dstGrid = new GridFE();

	gridAux->getSrcGrid(srcGrid);
	gridAux->getDstGrid(dstGrid);

	std::string gridFEPath = argv[4];

	GridFEWriter gridSrcWriter(gridFEPath + ".src.grid");
	gridSrcWriter.write(*srcGrid);
	GridFEWriter gridDstWriter(gridFEPath + ".dst.grid");
	gridDstWriter.write(*dstGrid);

	return 0;
}
