include(Grid.pri)
QT += core
QT -= gui
QT -= opengl
QT -= xml
TARGET = meshextract
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
OBJECTS_DIR = obj
MOC_DIR = moc
UI_DIR = ui
SOURCES += main.cpp
QMAKE_CFLAGS+=-g
QMAKE_CXXFLAGS+=-g
QMAKE_LFLAGS+=-g
