#include "MorpherCPU.h"
#include <cassert>
#include "GridFE.h"
#include "GridFEAux.h"
#include "Config.h"
#include "CubicSpline.h"

namespace memorph {

	Morpher::Morpher():
		_auxGrid(0),
		_config(0) { }

	Morpher::~Morpher() { }

	void Morpher::setAuxGrid(GridFEAux *auxGrid)
	{
		_auxGrid = auxGrid;
	}

	void Morpher::setConfig(Config *config)
	{
		_config = config;
	}

	void Morpher::compute()
	{
		int noSlices = _auxGrid->noSlices();
		int noPtsInSlice = _auxGrid->noPtsInSlice();

//		std::cout << noSlices << " " << noPtsInSlice << std::endl;

		int noMsrmnts = _config->noMsrmnts();
		real *coords = new real[noMsrmnts];
		real *perims = new real[noMsrmnts];

		for (int i = 0; i < noMsrmnts; ++i) {
			coords[i] = _config->msrmnts(i, 0);
			perims[i] = _config->msrmnts(i, 1);
		}

		CubicSpline *spline = new CubicSpline(coords, perims, noMsrmnts);

		/* liczy wektory przesunięcia
		 * w tym celu iteruje po wszystkich przekrojach siatki pomocniczej
		 * i wylicza ich obwód a następnie na podstawie interpolowanego obwodu
		 * wylicza długość wektora tak, aby wszystkie punkty na siatce docelowej
		 * dawały porządaną (interpolowaną) długość obwodu */

		/* współrzędne aktualnego i następnego węzła */
		real center[3];
		real curr[3];
		real next[3];
		real ray[3];
		real vect[3];

		real actual;
		real target;
		real ratio;
		real coord;
		int node;
		int base;

		for (int slice = 1; slice < noSlices - 1; ++slice) {

			/* liczy rzeczywisty obwód przekroju */			
			actual = 0;
			base = slice * noPtsInSlice;
			coord = _auxGrid->coord(base, 2);

			for (int pt = 0; pt < noPtsInSlice; ++pt) 
			{

				node = base + pt;				
				_auxGrid->getCoords(node, curr);
				_auxGrid->getCoords(base + (pt + 1) % noPtsInSlice, next);
				actual += distance(curr, next);
			}

			/* liczy docelowe położenie węzłów i na tej podstawie wylicza wektory morfujące */
			target = spline->interpolate(coord);
			ratio = target; //ratio = target / actual;
			_auxGrid->getAxisPt(slice, center);
			
			for (int pt = 0; pt < noPtsInSlice; ++pt) {
				node = base + pt;
				_auxGrid->getCoords(node, curr);
				create(center, curr, ray);
				product(ray, ratio - 1, vect);
				_auxGrid->setVect(node, vect[0], vect[1], vect[2]);
			}
		}

		

		/* Generowanie wektorów do pierwszego i ostaniego
		 * przekroju. Są to przekoje pomocniczy, zapewniające
		 * prawidłowe zachowanie się algorytmu znajdującego
		 * trójkąty na końcach morfowanej bryły. */
		{

			base = 0;
			for (int pt = 0; pt < noPtsInSlice; ++pt) {
				node = base + pt;
				_auxGrid->getVect(node + noPtsInSlice, vect);
				_auxGrid->setVect(node, vect);
			}

			base = (noSlices - 1) * noPtsInSlice;
			for (int pt = 0; pt < noPtsInSlice; ++pt) {
				node = base + pt;
				_auxGrid->getVect(node - noPtsInSlice, vect);
				_auxGrid->setVect(node, vect);
			}
		}
	}

	void Morpher::morph(GridFE *_grid)
	{
		compute();

		real vertex[3];
		real center[3];
		real sector[3][3];
		real vector[3][3];

		real intersect[3];
		real axis[2][3];
		real norm[3];
		real ray[3];

		bool found;

		for (int i = 0; i < _grid->noNodes(); ++i) 
		{
#if 0
			if (!(i > 8000 && i < 8016)) { 
//			if (!(i == 265)) {
				continue;
			}
#endif

			/* pobiera współrzędne węzła */
			_grid->getCoords(i, vertex);
//			vertex[2] += 0.01f;

			found = false;

			/* znajduje odcinek osi dla węzła */
			for (int j = 0; j+1 < _auxGrid->noSlices(); ++j) 
			{
				if (vertex[2] >= _auxGrid->axisPt(j, 2) && vertex[2] <= _auxGrid->axisPt(j+1, 2)) 
				{		
					/* kopiuje punkt początkowy */
					axis[0][0] = _auxGrid->axisPt(j, 0);
					axis[0][1] = _auxGrid->axisPt(j, 1);
					axis[0][2] = _auxGrid->axisPt(j, 2);
					
					/* kopiuje punkt końcowy */
					axis[1][0] = _auxGrid->axisPt(j+1, 0);
					axis[1][1] = _auxGrid->axisPt(j+1, 1);
					axis[1][2] = _auxGrid->axisPt(j+1, 2);

					found = true;
					break;
				}
			}

			assert(found);
			
			/* znajduje tymczasowy punkt centralny */
			projection3D(vertex, axis[0], axis[1], center);

			create(center, vertex, ray);

			found = false;

			/* wylicza sektor, w którym znajduje się węzeł */
			/* iteruje po wszystkich trójkątach siatki pomocniczej */
			for (int j = 0; j < _auxGrid->noElems(); ++j) 
			{
				/* pobiera współrzędne wierzchołków trójkąta */
				_auxGrid->getElemCoords(j, 0, sector[0]);
				_auxGrid->getElemCoords(j, 2, sector[1]);
				_auxGrid->getElemCoords(j, 1, sector[2]);

				/* wylicza normalną do powierzchni trójkąta */
				normal(sector[0], sector[1], sector[2], norm);

				/* jeżeli wierzchołek i trójkąt znajdują się po tej samej stronie,
				 * to iloczyn skalarny promienia i normalnej do powierzchni trójąkta 
				 * jest nieujemny */
				if (dotProduct(norm, ray) >= 0) 
				{
					/* wylicza punkt przecięcia promienia z płaszczyzną określona
					 * przez wierzchołki trójkąta - jeżeli punkt ten leży wewnątrz,
					 * to znaczy, że znaleziono... */
//					normalize(ray);
					intersection(center, ray, sector[0], sector[1], sector[2], intersect);

#if 1
					real vect[3]; copy(ray, vect);
			
					product(vect, 0.3f, vector[0]);
					product(vect, 0.3f, vector[1]);
					product(vect, 0.3f, vector[2]);

#endif


//					if (inside(intersect, sector[0], sector[1], sector[2])) 
					if (almostInside(intersect, sector[0], sector[1], sector[2], 1E-20)) 
					{

#if 1					
						_auxGrid->getElemVect(j, 0, vector[0]);
						_auxGrid->getElemVect(j, 2, vector[1]);
						_auxGrid->getElemVect(j, 1, vector[2]);
#endif


						if(displace(center, intersect, sector[0], sector[1], sector[2],
							vector[0], vector[1], vector[2], vertex)) {
							std::cout << 
								"Waga większ dla wietrzchołka: " 
								<< i + 1 << " element: "<< j+1<< std::endl;
						}

						

						_grid->coord(i, 0) = vertex[0];
						_grid->coord(i, 1) = vertex[1];		
						_grid->coord(i, 2) = vertex[2];

						found = true;
						break;
					}
				}
			}

			if (!found) {
				std::cout << "Problem z wierzchołkiem nr " << i+1 << std::endl;
				std::cout << "# Line ( "
					<< center[0] << " "
					<< center[1] << " "
					<< center[2] << " "
					<< vertex[0] << " "
					<< vertex[1] << " "
					<< vertex[2] << ") 3 \n";

				abort();

			}

		}

	} // Morpher::morph()

} // namespace memorph
