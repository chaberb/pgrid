#ifndef READER_H
#define READER_H

#include "File.h"
#include <fstream>

namespace memorph {

	class Reader: public File {
	protected:
		std::ifstream _input;
	public:
		Reader();
		Reader(const std::string &);
		virtual ~Reader();

		virtual void assure() const;

		static const size_t maxLineLen = 256;
	};
}

#endif // READER_H
