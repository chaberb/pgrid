#ifndef GRIDFE_WRAPPER_H
#define GRIDFE_WRAPPER_H

#include "Base.h"

namespace memorph {

	class GridFE;
	class GridFEAux;

	class GridFEWrapper {
	public:
		GridFE *_grid;
	
		int **_edges;
		int _noEdges;

		real **_nodes;
		int _noNodes;

//		int *_numb;
//		int *_numbInv;

		int _noSlices;
		int _noPtsInSlice;

		real *_lowLimit;
		real *_uppLimit;

		real _eps;

	public:
		GridFEWrapper();
		virtual ~GridFEWrapper();

		void setGrid(GridFE *grid);
		void setNoSlices(int noSlices);
		void setNoPtsInSlice(int noPtsPerSlice);
		void setLimits(real lower, real upper);
	
		void wrap(GridFEAux *grid, real eps=0);

		int noElemEdges(int elemIdx) const;
		int countEdges() const;
		
	private:
		void _wrap(GridFEAux *grid);
		void _computeLimits();
		void _createAuxData();
		void _initAuxData();
		void _freeAuxData();
	};

} // namespace memorph

#endif // GRIDFE_WRAPPER_H
