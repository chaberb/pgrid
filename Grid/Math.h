#ifndef MATH_H
#define MATH_H

#include <cstdlib>
#include <cmath>
#include <cstdio>

namespace memorph {

	typedef float real; // liczby rzeczywiste

	struct Vector {
		union {
			struct {
				real x, y, z;
			};
			real coords[3];
		};
	};

	void fill(real x, real y, real z, Vector *out);
	void fill(real coords[3], Vector *out);
	void copy(const Vector &v, Vector *out);

	void add(const Vector &v, const Vector &w, Vector *out);
	void sub(const Vector &v, const Vector &w, Vector *out);
	void mul(const Vector &v, real t, Vector *out);
	void div(const Vector &v, real t, Vector *out);

	void norm(const Vector &v, Vector *out);
	void norm(Vector *v);

	real len(const Vector &v);
	real lenSq(const Vector &v);

	real dist(const Vector &v, const Vector &w);
	real dist(const Vector &v, const Vector &org, const Vector &dir);

	real distSq(const Vector &v, const Vector &w);
	real distSq(const Vector &v, const Vector &org, const Vector &dir);

	real   dotProduct(const Vector &v, const Vector &w);
	void crossProduct(const Vector &v, const Vector &w, Vector *out);

	void proj(const Vector &v, const Vector &org, const Vector &dir, Vector *out);
	void proj(const Vector &v, const Vector &axs, Vector *out);

	void centr(Vector *points, int noPoints, Vector *out);

	real alpha(real x, real y);
}

namespace memorph {

	inline void fill(real x, real y, real z, Vector *out)
	{
		out->x = x;
		out->y = y;
		out->z = z;
	}

	inline void fill(real coords[3], Vector *out)
	{
		out->coords[0] = coords[0];
		out->coords[1] = coords[1];
		out->coords[2] = coords[2];
	}

	inline void copy(const Vector &v, Vector *out)
	{
		out->x = v.x;
		out->y = v.y;
		out->z = v.z;
	}

	inline void add(const Vector &v, const Vector &w, Vector *out)
	{
		out->x = v.x + w.x;
		out->y = v.y + w.y;
		out->z = v.z + w.z;
	}

	inline void sub(const Vector &v, const Vector &w, Vector *out)
	{
		out->x = v.x - w.x;
		out->y = v.y - w.y;
		out->z = v.z - w.z;
	}

	inline void mul(const Vector &v, real t, Vector *out)
	{
		out->x = v.x * t;
		out->y = v.y * t;
		out->z = v.z * t;
	}

	inline void div(const Vector &v, real t, Vector *out)
	{
		out->x = v.x / t;
		out->y = v.y / t;
		out->z = v.z / t;
	}

	inline void norm(const Vector &v, Vector *out)
	{
		real l = len(v); l = (l == 0.0f) ? 1 : l;
		out->x = v.x / l;
		out->y = v.y / l;
		out->z = v.z / l;
	}

	inline void norm(Vector *out)
	{
		real l = len(*out); l = (l == 0.0f) ? 1 : l;
		out->x /= l;
		out->y /= l;
		out->z /= l;
	}

	inline real lenSq(const Vector &v)
	{
		return v.x*v.x + v.y*v.y + v.z*v.z;
	}

	inline real len(const Vector &v)
	{
		return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	}

	inline real distSq(const Vector &v, const Vector &w)
	{
		real dx = v.x - w.x;
		real dy = v.y - w.y;
		real dz = v.z - w.z;
		return dx*dx + dy*dy + dz*dz;
	}

	inline real distSq(const Vector &v, const Vector &org, const Vector &dir)
	{
		Vector p; proj(v, org, dir, &p);
		return distSq(v, p);
	}

	inline real dist(const Vector &v, const Vector &w)
	{
		real dx = v.x - w.x;
		real dy = v.y - w.y;
		real dz = v.z - w.z;
		return sqrt(dx*dx + dy*dy + dz*dz);
	}

	inline real dist(const Vector &v, const Vector &org, const Vector &dir)
	{
		Vector p; proj(v, org, dir, &p);
		return dist(v, p);
	}

	inline real dotProduct(const Vector &v, const Vector &w)
	{
		return (v.x * w.x) + (v.y * w.y) + (v.z * w.z);
	}

	inline void crossProduct(const Vector &v, const Vector &w, Vector *out)
	{
		out->x = v.y*w.z - v.z*w.y;
		out->y = v.z*w.x - v.x*w.z;
		out->z = v.x*w.y - v.y*w.x;
	}

	/* funkcja zwraca znak liczby (-1 - ujemna; 0 - zero; 1 - dodatnia) */
	inline int sign(real x)
	{
		if (x < 0) {
			return -1;
		} else if (x == 0) { // TODO - może lepiej sprawdzać czymś w rodzaju around(x, 0)
			return 0;
		} else {
			return 1;
		}
	}

	inline real alpha(real x, real y)
	{
		real absX = fabs(x);
		real absY = fabs(y);
		real d = absX + absY;
#if 0
		std::cout << "absX = " << absX << " absY = " << absY << std::endl;
#endif
		if (d == 0) {
			abort();
		}

		if (x == 0) {
			// dodatnia półoś y
			if (y > 0) {
				return 1;
			// ujemna półoś y
			} else if (y < 0) {
				return 3;
			}
		}

		if (y == 0) {
			// dodatnia półoś x
			if (x > 0) {
				return 0;
			// ujemna półoś x
			} else if (x < 0) {
				return 2;
			}
		}

		if (x > 0) {
			// 1. ćwiartka
			if (y > 0) {
				return y / d;
			// 2. ćwiartka
			} else {
				return 4 - absY / d;
			}
		} else {
			// 3. ćwiartka
			if (y > 0) {
				return 2 - y / d;
			// 4. ćwiartka
			} else {
				return 2 + absY / d;
			}
		}
	}

	// przecięcie odcinka beg-end z płaszczyzną równoległą do OXY
	inline void intrX(const Vector &beg, const Vector &end, real x, Vector *out)
	{
//		assertNoEqual(beg.x, end.x);
		real ratio = (x - beg.x) / (end.x - beg.x);
		out->x = x;
		out->y = beg.y + (end.y - beg.y) * ratio;
		out->z = beg.z + (end.z - beg.z) * ratio;
//		std::cout << ratio << ": " << out->x << " " << out->y << " " << out->z << std::endl;
	}

	inline void segmIntr(real vBeg[2], real vEnd[2], real wBeg[2], real wEnd[2], real *out)
	{
		real x1 = vBeg[0]; real y1 = vBeg[1];
		real x2 = vEnd[0]; real y2 = vEnd[1];
		real x3 = wBeg[0]; real y3 = wBeg[1];
		real x4 = wEnd[0]; real y4 = wEnd[1];
#if 0
		real x1y2 = x1 * y2;
		real x3y4 = x3 * y4;
		real y1x2 = y1 * x2;
		real y3x4 = y3 * x4;
		real x1_x2 = x1 - x2;
		real y1_y2 = y1 - y2;
		real x3_x4 = x3 - x4;
		real y3_y4 = y3 - y4;
#endif
		real px = 
			((x1*y2 - y1*x2) * (x3 - x4) - (x1 - x2) * (x3*y4 - y3*x4)) /
			((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
		real py =
			((x1*y2 - y1*x2) * (y3 - y4) - (y1 - y2) * (x3*y4 - y3*x4)) /
			((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
		out[0] = px;
		out[1] = py;
	}

	/* tworzy wektor z dwóch wektorów o początku w beg i końcu w end */
	inline void create(real beg[3], real end[3], real *out)
	{
		out[0] = end[0] - beg[0];
		out[1] = end[1] - beg[1];
		out[2] = end[2] - beg[2];
	}

	/* kopiuje wektor */
	inline void copy(real v[3], real *out)
	{
		out[0] = v[0];
		out[1] = v[1];
		out[2] = v[2];
	}

	/* mnoży wektor przez skalar */
	inline void product(real v[], real t, real *out)
	{
		out[0] = v[0] * t;
		out[1] = v[1] * t;
		out[2] = v[2] * t;
	}

	/* długość wektora */
	inline real length(real v[3])
	{	
		return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	}

	inline real distanceSq(real v[3], real w[3])
	{
		real dx = v[0] - w[0];
		real dy = v[1] - w[1];
		real dz = v[2] - w[2];
		return dx*dx + dy*dy + dz*dz;
	}

	/* oblicza przecięcie odcinka z płaszczyzną równoległą do YOZ */
	inline void intersectionX(real beg[3], real end[3], real x, real *out)
	{
		real ratio = (x - beg[0]) / (end[0] - beg[0]);
		out[0] = x;
		out[1] = beg[1] + (end[1] - beg[1]) * ratio;
		out[2] = beg[2] + (end[2] - beg[2]) * ratio;
	}

	/* oblicza przecięcie odcinka z płaszczyzną równoległą do XOZ */
	inline void intersectionY(real beg[3], real end[3], real y, real *out)
	{
		// TODO - sprawdzić
		real ratio = (y - beg[1]) / (end[1] - beg[1]);
		out[0] = beg[0] + (end[0] - beg[0]) * ratio;
		out[1] = y;
		out[2] = beg[2] + (end[2] - beg[2]) * ratio;
	}

	/* oblicza przecięcie odcinka z płaszczyzną równoległą do XOY */
	inline void intersectionZ(real beg[3], real end[3], real z, real *out)
	{
		real ratio = (z - beg[2]) / (end[2] - beg[2]);
		out[0] = beg[0] + (end[0] - beg[0]) * ratio;
		out[1] = beg[1] + (end[1] - beg[1]) * ratio;
		out[2] = z;
	}

	/* oblicza środek ciężkości zbioru wierzchołków na płaszczyźnie 2D */
	inline void barycenter2D(real **pts, int noPts, real *out)
	{
		out[0] = 0.0f;
		out[1] = 0.0f;
		for (int i = 0; i < noPts; ++i) {
			out[0] += pts[i][0];
			out[1] += pts[i][1];
		}
		out[0] /= noPts;
		out[1] /= noPts;
	}

	inline void barycenter3D(real a[3], real b[3], real c[3], real *out)
	{
		out[0] = (a[0] + b[0] + c[0]) / 3;
		out[1] = (b[1] + b[1] + c[1]) / 3;
		out[2] = (b[2] + b[2] + c[2]) / 3;
	}

	/* funkcja do porównywania kątów dwóch punktów (do qsort) */
	inline int alphaCmpFunc(const void *a, const void *b)
	{
		real **p1 = (real **)a;
		real **p2 = (real **)b;
#if 0
		return sign(alpha((*p1)[0], (*p1)[1]) - alpha((*p2)[0], (*p2)[1]));
	}
#else
		real p1Res = alpha((*p1)[0], (*p1)[1]);
		real p2Res = alpha((*p2)[0], (*p2)[1]);
		real res = p1Res - p2Res;
		if (res < 0) {
			return -1;
		} else if (res == 0) {
			return 0;
		} else {
			return 1;
		}
	}
#endif

	/* sortuje zbiór punktów 2D ustawiając je w porządku CCW */
	inline void sort2D(real **pts, int noPts)
	{
		qsort(pts, noPts, sizeof(real *), alphaCmpFunc);
	}

	inline real dotProduct(real v[3], real w[3])
	{
		return (v[0] * w[0]) + (v[1] * w[1]) + (v[2] * w[2]);
	}

	inline void crossProduct(real v[3], real w[3], real *out)
	{
		out[0] = v[1]*w[2] - v[2]*w[1];
		out[1] = v[2]*w[0] - v[0]*w[2];
		out[2] = v[0]*w[1] - v[1]*w[0];
	}

	inline real crossProductLen(real v[3], real w[3])
	{
		real temp[3];

		temp[0] = v[1]*w[2] - v[2]*w[1];
		temp[1] = v[2]*w[0] - v[0]*w[2];
		temp[2] = v[0]*w[1] - v[1]*w[0];

		return length(temp);
	}

	/* przesuwa zbiór punktów o zadany wektor TODO - odwrócić */
	inline void translate2D(real **pts, int noPts, real t[2])
	{		
		for (int i = 0; i < noPts; ++i) {
			pts[i][0] -= t[0];
			pts[i][1] -= t[1];
		}
	}

	/* zmienia wektor na przeciwny */
	inline void inverse2D(real v[2], real *out)
	{
		out[0] = -v[0];
		out[1] = -v[1];
	}

	/* bada czy wartość znajduje się w epsilonowym otoczeniu x-a */
	inline bool around(real x, real val, real eps)
	{
		return fabs(x - val) <= eps;
	}

	/* odległośc między dwoma punktami */
	inline real distance(real v[3], real w[3])
	{	
		real d0 = v[0] - w[0];
		real d1 = v[1] - w[1];
		real d2 = v[2] - w[2];
		return sqrt(d0 * d0 + d1 * d1 + d2 * d2);
	}

	inline void normalize(real v[3], real *out)
	{
		real len = length(out);
		if (len == 0) abort();
		out[0] = v[0] / len;
		out[1] = v[1] / len;
		out[2] = v[2] / len;
	}

	/* normalizuje wektor w miejscu */
	inline void normalize(real *out)
	{	
		real len = length(out);
		//if (len == 0) abort();
		return;
		out[0] /= len;
		out[1] /= len;
		out[2] /= len;
	}

	/* normalna do powierzchni vert1, vert2, vert3 */
	inline void normal(real vrt1[3], real vrt2[3], real vrt3[3], real *out)
	{
		/* edge12: krawędź łącząca wierzcholki 1-2 */
		real edge12[3];
		edge12[0] = vrt2[0] - vrt1[0];
		edge12[1] = vrt2[1] - vrt1[1];
		edge12[2] = vrt2[2] - vrt1[2];

		/* edge13: krawędź łącząca wierzchołki 1-3 */
		real edge13[3];
		edge13[0] = vrt3[0] - vrt1[0];
		edge13[1] = vrt3[1] - vrt1[1];
		edge13[2] = vrt3[2] - vrt1[2];

		crossProduct(edge12, edge13, out);
		normalize(out);
	}

	/* rzut punktu na odcinek */
	inline void projection3D(real v[3], real beg[3], real end[3], real *out)
	{
		real vrt[3], dir[3];

		/* vrt: v w układzie współrzędnych beg */
		vrt[0] = v[0] - beg[0];
		vrt[1] = v[1] - beg[1];
		vrt[2] = v[2] - beg[2];

		/* dir - kierunek odcinka beg-end */
		dir[0] = end[0] - beg[0];
		dir[1] = end[1] - beg[1];
		dir[2] = end[2] - beg[2];

		/* vrtNorm, dirNorm - znormalizowane */
		real vrtNorm[3], dirNorm[3];

		real vrtLen = length(vrt);
		real dirLen = length(dir);

		dirNorm[0] = dir[0] / dirLen;
		dirNorm[1] = dir[1] / dirLen;
		dirNorm[2] = dir[2] / dirLen;

		vrtNorm[0] = vrt[0] / vrtLen;
		vrtNorm[1] = vrt[1] / vrtLen;
		vrtNorm[2] = vrt[2] / vrtLen;

		real t = dotProduct(dirNorm, vrtNorm) * vrtLen;

		out[0] = beg[0] + dirNorm[0] * t;
		out[1] = beg[1] + dirNorm[1] * t;
		out[2] = beg[2] + dirNorm[2] * t;
	}

	/* oblicza punkt przecięcia promienia z płaszczyzną */
	inline void intersection(real org[3], real dir[3], real pVrt1[3], real pVrt2[3], real pVrt3[3], real *out)
	{	
//		real pVrt1[3];
//		real pVrt2[3];
//		real pVrt3[3];

		real beg[3]; copy(pVrt1, beg);
		
		beg[0] -= org[0];
		beg[1] -= org[1];
		beg[2] -= org[2];

		/* ... */
		real edge12[3];
		edge12[0] = pVrt2[0] - pVrt1[0];
		edge12[1] = pVrt2[1] - pVrt1[1];
		edge12[2] = pVrt2[2] - pVrt1[2];

		/* ... */
		real edge13[3];
		edge13[0] = pVrt3[0] - pVrt1[0];
		edge13[1] = pVrt3[1] - pVrt1[1];
		edge13[2] = pVrt3[2] - pVrt1[2];

		real norm[3];
//		normal(pVrt1, pVrt2, pVrt3, norm);
		crossProduct(edge12, edge13, norm);
//		normalize(norm);

//		real d = -norm[0] * pVrt1[0] - norm[1] * pVrt1[1] - norm[2] * pVrt1[2];
//		real t = (-dotProduct(norm, pVrt1) + d) / dotProduct(norm, dir);
		real t = dotProduct(norm, beg) / dotProduct(norm, dir);

		out[0] = org[0] + dir[0] * t;
		out[1] = org[1] + dir[1] * t;
		out[2] = org[2] + dir[2] * t;
	}

	/* tworzy trójkąt... */
	inline void convert(real vrt1[3], real vrt2[3], real vrt3[3], real *pt, real *edge12, real *edge13)
	{
		/* ... */
		pt[0] = vrt1[0];
		pt[1] = vrt1[1];
		pt[2] = vrt1[2];

		/* ... */
		edge12[0] = vrt2[0] - vrt1[0];
		edge12[1] = vrt2[1] - vrt1[1];
		edge12[2] = vrt2[2] - vrt1[2];

		/* ... */
		edge13[0] = vrt3[0] - vrt1[0];
		edge13[1] = vrt3[1] - vrt1[1];
		edge13[2] = vrt3[2] - vrt1[2];
	}

	/* sprawdza czy punkt leży wewnątrz trójkąta */
	inline bool inside(real pt[3], real vrt1[3], real vrt2[3], real vrt3[3])
	{
		real *a = vrt1;
        real *b = vrt2;
        real *c = vrt3;
        real *d = pt;

        real ab[3]; create(a, b, ab);
		real ac[3]; create(a, c, ac);
		real ad[3]; create(a, d, ad);
		real ba[3]; create(b, a, ba);
		real bc[3]; create(b, c, bc);
		real bd[3]; create(b, d, bd);
		real ca[3]; create(c, a, ca);
		real cb[3]; create(c, b, cb);
		real cd[3]; create(c, d, cd);

		real cross1[3]; crossProduct(ab, ac, cross1);
		real cross2[3]; crossProduct(bc, ba, cross2);
		real cross3[3]; crossProduct(ca, cb, cross3);
		real cross4[3]; crossProduct(ab, ad, cross4);
		real cross5[3]; crossProduct(bc, bd, cross5);
		real cross6[3]; crossProduct(ca, cd, cross6);

		real dot1 = dotProduct(cross1, cross4);
		real dot2 = dotProduct(cross2, cross5);
		real dot3 = dotProduct(cross3, cross6);

		return dot1 >= 0 && dot2 >= 0 && dot3 >= 0;
	}

	inline bool almostInside(real pt[3], real vrt1[3], real vrt2[3], real vrt3[3], real eps=0)
	{
		real *a = vrt1;
        real *b = vrt2;
        real *c = vrt3;
        real *d = pt;

		if (distance(pt, vrt1) < eps) {
			return true;
		} else if (distance(pt, vrt2) < eps) {
			return true;
		} else if (distance(pt, vrt3) < eps) {
			return true;
		}

        real ab[3]; create(a, b, ab);
		real ac[3]; create(a, c, ac);
		real ad[3]; create(a, d, ad);
		real ba[3]; create(b, a, ba);
		real bc[3]; create(b, c, bc);
		real bd[3]; create(b, d, bd);
		real ca[3]; create(c, a, ca);
		real cb[3]; create(c, b, cb);
		real cd[3]; create(c, d, cd);

		real cross1[3]; crossProduct(ab, ac, cross1);
		real cross2[3]; crossProduct(bc, ba, cross2);
		real cross3[3]; crossProduct(ca, cb, cross3);
		real cross4[3]; crossProduct(ab, ad, cross4);
		real cross5[3]; crossProduct(bc, bd, cross5);
		real cross6[3]; crossProduct(ca, cd, cross6);

		real dot1 = dotProduct(cross1, cross4);
		real dot2 = dotProduct(cross2, cross5);
		real dot3 = dotProduct(cross3, cross6);

		real epsInv = -fabs(eps);

		return dot1 >= epsInv && dot2 >= epsInv && dot3 >= epsInv;

	}

	inline bool displace(
			real center[3], 
			real intersect[3], 
			real sector1[3], 
			real sector2[3], 
			real sector3[3], 
			real vector1[3],
			real vector2[3],
			real vector3[3],
			real *vertex)
	{
		/* ... */
		real edge12[3];
		edge12[0] = sector2[0] - sector1[0];
		edge12[1] = sector2[1] - sector1[1];
		edge12[2] = sector2[2] - sector1[2];

		/* ... */
		real edge13[3];
		edge13[0] = sector3[0] - sector1[0];
		edge13[1] = sector3[1] - sector1[1];
		edge13[2] = sector3[2] - sector1[2];

		// pole trójkąta (jako długość wektora normalnego do powierzchni trójkąta)
		real area = crossProductLen(edge12, edge13);

		// wektory utworzone z punktu przecięcia i każdego z wierzchołków trójkąta
		real intersect1[3]; create(intersect, sector1, intersect1);
		real intersect2[3]; create(intersect, sector2, intersect2);
		real intersect3[3]; create(intersect, sector3, intersect3);

		// wagi polowe
		real area1 = crossProductLen(intersect2, intersect3) / area;
		real area2 = crossProductLen(intersect1, intersect3) / area;
		real area3 = crossProductLen(intersect1, intersect2) / area;

		

		// waga długościowa
//		real dist = length(vert - org)  / length(intr - org);
		real dist = distance(vertex, center) / distance(intersect, center);

		// wagi
		real weight1 = area1 * dist;
		real weight2 = area2 * dist;
		real weight3 = area3 * dist;


		vertex[0] += vector1[0] * weight1 + vector2[0] * weight2 + vector3[0] * weight3;
		vertex[1] += vector1[1] * weight1 + vector2[1] * weight2 + vector3[1] * weight3;
		vertex[2] += vector1[2] * weight1 + vector2[2] * weight2 + vector3[2] * weight3;

//		Vector displ0; mul(triangle.vectors[0], areaWeight0 * distWeight, &displ0);
//		Vector displ1; mul(triangle.vectors[1], areaWeight1 * distWeight, &displ1);
//		Vector displ2; mul(triangle.vectors[2], areaWeight2 * distWeight, &displ2);

		// dodaje przesunięcia; i przesuwa wektor
//		Vector temp;
//		add(displ0, displ1, displ2, &temp);
//		add(vert, temp, out);
		if (area1 > 1 || area2 > 1 || area3 > 1)
			return true;
		return false;
	}

        inline real area(Vector element[3])
        {
            Vector ab; // a-b
            Vector ac; // a-c
            Vector u; // ab x ac
            real area; // |ab x ac]/2

            sub(element[0], element[1], &ab);
            sub(element[0], element[2], &ac);
            crossProduct(ab, ac, &u);

            area = fabs(len(u))/2.0;

            return area;
        }

        inline bool myisnan(real var) {
            volatile real d = var;
            return d != d;
        }

        inline real volume(Vector tetrahedron[4])
        {
            Vector ad; // a-d
            Vector bd; // b-d
            Vector cd; // c-d
            Vector u; // (b-d) x (c-d)
            real v; // (a-d) * u

            sub(tetrahedron[0], tetrahedron[3], &ad);
            sub(tetrahedron[1], tetrahedron[3], &bd);
            sub(tetrahedron[2], tetrahedron[3], &cd);
            crossProduct(bd, cd, &u);

            v = dotProduct(ad, u);
            real volume = fabs(v)/6.0;

            return volume;
        }
#if 0
	inline void centr(Vector *points, int noPoints, Vector *out)
	{
		out->x = 0.0f;
		out->y = 0.0f;
		out->z = 0.0f;
		for (int i = 0; i < noPoints; ++i) {
			out->x += points[i].x;
			out->y += points[i].y;
			out->z += points[i].z;
		}
		out->x /= noPoints;
		out->y /= noPoints;
		out->z /= noPoints;
	}

	inline void centr(const real **points, int noPoints, real out[3])
	{
		out[0] = 0.0f;
		out[1] = 0.0f;
		out[2] = 0.0f;
		for (int i = 0; i < noPoints; ++i) {
			out[0] += points[i][0];
			out[1] += points[i][1];
			out[2] += points[i][2];
		}
		out[0] /= noPoints;
		out[1] /= noPoints;
		out[2] /= noPoints;
	}
#endif

#if 0
	// przecięcie odcinka beg-end z płaszczyzną równoległą do O...
	inline void intrY(const Vector &org, const Vector &dir, real y, Vector *out)
	{
		// TODO...
	}

	// przecięcie odcinka beg-end z płaszczyzną równoległą do O...
	inline void intrZ(const Vector &org, const Vector &dir, real z, Vector *out)
	{
		// TODO...
	}
#endif
	// rzut punktu na promień
	inline void proj(const Vector &v, const Vector &org, const Vector &dir, Vector *out)
	{
		Vector vrt;     sub(v, org, &vrt);
		Vector dirNorm; norm(dir, &dirNorm);
		Vector vrtNorm; norm(vrt, &vrtNorm);
		real t = dotProduct(dirNorm, vrtNorm) * len(vrt);
		Vector temp;    mul(dirNorm, t, &temp);
		add(org, temp, out);
	}

	// rzut punktu na oś
	inline void proj(const Vector &vrt, const Vector &axs, Vector *out)
	{
		Vector axsNorm; norm(axs, &axsNorm);
		Vector vrtNorm; norm(vrt, &vrtNorm);
		real t = dotProduct(axsNorm, vrtNorm) * len(vrt);
		mul(axsNorm, t, out);
	}
}

#endif // MATH_H
