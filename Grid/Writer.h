#ifndef WRITER_H
#define WRITER_H

#include "File.h"
#include <fstream>

namespace memorph {

	class Writer: public File {
	protected:
		std::ofstream _output;
	public:
		Writer();
		Writer(const std::string &);
		virtual ~Writer();

		virtual void assure() const;
	};

}

#endif // WRITER_H
