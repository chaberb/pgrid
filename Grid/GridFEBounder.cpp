#include "GridFEBounder.h"

#include <iostream>
using namespace std;

namespace memorph {

	GridFEBounder::GridFEBounder():
		GridFE(),
		_facets(0),
		_noFacets(0) { }

	GridFEBounder::~GridFEBounder()
	{
		// TODO - jakieś sprzątanie ???
	}

	int GridFEBounder::elemNoFacets(int elemIdx) const
	{
		assertArrIndex(elemIdx, _noElems);
		int type = elemType(elemIdx);
		switch (type) {
		case T3n2D:
			return 1;
		case T4n3D:
			return 4;
		}
		return 0;
	}

	void GridFEBounder::initFacets(int noFacets)
	{
		if (_noFacets != 0) {
			freeFacets();
		}
		memAlloc(&_facets, sizeof(int *) * noFacets);
		for (int i = 0; i < noFacets; ++i) {
			memAlloc(&_facets[i], facetMemSize);
		}
		_noFacets = noFacets;
	}

	void GridFEBounder::initBndData()
	{
		memAlloc(&_bndData, sizeof(int) * _noNodes);
		// TODO - trzeba zrobić jakieś zwalnianie warunkowe
	}

	void GridFEBounder::freeFacets()
	{
		for (int i = 0; i < _noFacets; ++i) {
			memFree(&_facets[i]);
		}
		memFree(&_facets);
		_noFacets = 0;
	}

	void GridFEBounder::freeBndData()
	{
		memFree(&_bndData);
		// TODO...
	}

	int GridFEBounder::countFacets() const
	{
		int noFacets = 0;
		for (int i = 0; i < noElems(); ++i) {
			noFacets += elemNoFacets(i);
		}
		return noFacets;
	}

	void GridFEBounder::createFacets()
	{
		initFacets(countFacets());
		int noFacets, noNodes;
		for (int i = 0, j = 0; i < noElems(); ++i) {
			noFacets = elemNoFacets(i);
#if 1
			noNodes = elemType(i);
			for (int k = 0; k < noNodes; ++k) {
				for (int l = k + 1; l < noNodes; ++l) {
					for (int m = l + 1; m < noNodes; ++m) {
						_facets[j][0] = j + 1;
						_facets[j][1] = elemNodeNr(i, k);
						_facets[j][2] = elemNodeNr(i, l);
						_facets[j][3] = elemNodeNr(i, m);
						_facets[j][4] = elemSubdNr(i);
						_facets[j][5] = 0; //elemNr(i); // miał być nr elementu ale..
						j++;
					}
				}
			}
#else
				_facets[j][0] = j + 1;
				_facets[j][1] = elemNodeNr(i, 0);
				_facets[j][2] = elemNodeNr(i, 1);
				_facets[j][3] = elemNodeNr(i, 2);
				_facets[j][4] = elemSubdNr(i);
				_facets[j][5] = elemNr(i);
				j++;
			if (noFacets < 4) {
				continue;
			}
				_facets[j][0] = j + 1;
				_facets[j][1] = elemNodeNr(i, 0);
				_facets[j][2] = elemNodeNr(i, 1);
				_facets[j][3] = elemNodeNr(i, 3);
				_facets[j][4] = elemSubdNr(i);
				_facets[j][5] = elemNr(i);
				j++;
				_facets[j][0] = j + 1;
				_facets[j][1] = elemNodeNr(i, 0);
				_facets[j][2] = elemNodeNr(i, 2);
				_facets[j][3] = elemNodeNr(i, 3);
				_facets[j][4] = elemSubdNr(i);
				_facets[j][5] = elemNr(i);
				j++;
				_facets[j][0] = j + 1;
				_facets[j][1] = elemNodeNr(i, 1);
				_facets[j][2] = elemNodeNr(i, 2);
				_facets[j][3] = elemNodeNr(i, 3);
				_facets[j][4] = elemSubdNr(i);
				_facets[j][5] = elemNr(i);
				j++;
#endif
		}
	}

	// porównaj węzły należące do ściany
	inline int nodesCmp(const void *a, const void *b)
	{
		int *n1 = (int *)a;
		int *n2 = (int *)b;
		return *n1 - *n2;
	}

	// porównaj ściany - qsort
	inline int facetsCmp(const void *a, const void *b)
	{
		int f1[3];
		int f2[3];
		int i = 0, temp;

		for (int i=0; i<3; i++) {
			f1[i] = (*(int**)a)[i+1];
			f2[i] = (*(int**)b)[i+1];
		}

//		std::cout << f1[0] << ", " << f1[1] << ", " << f1[2] <<"\t";
		qsort(f1, 3, sizeof(int), nodesCmp);
//		std::cout << f1[0] << ", " << f1[1] << ", " << f1[2] << std::endl;
		qsort(f2, 3, sizeof(int), nodesCmp);
	
		while (i < 3) {
			temp = f1[i] - f2[i];
			if (temp != 0) {
			   return temp;
			}
			i++;
		}
		return 0;
	}

	// porównaj ściany - do rozdzielania
	inline int facetsNodesEq(int *f1, int *f2)
	{
		int matches = 0;
		if (f1[1] == f2[1] ||
                    f1[1] == f2[2] ||
                    f1[1] == f2[3]) matches++;
		if (f1[2] == f2[1] ||
                    f1[2] == f2[2] ||
                    f1[2] == f2[3]) matches++;
		if (f1[3] == f2[1] ||
                    f1[3] == f2[2] ||
                    f1[3] == f2[3]) matches++;
		if (matches == 3) return 1;
		return 0;
	}

	void GridFEBounder::sortFacets()
	{
		//for (int i = 0; i < noFacets(); ++i) {
		//	qsort(_facets[i] + 1, 3, sizeof **_facets, nodesCmp);
		//}
		qsort(_facets, _noFacets, sizeof *_facets, facetsCmp);
		for (int i = 0; i < _noFacets; ++i) {
			int f[3];
			for (int j=0; j<3; j++)
				f[j] = _facets[i][j+1];
			_facets[i][0] = i+1;
			//qsort(f, 3, sizeof(int), nodesCmp);
			//std::cout << ">>> " << f[0] << ", " << f[1] << ", " << f[2] << std::endl;
		}
	}

	void GridFEBounder::markExternals()
	{
		initBndData();
		for (int i = 0; i < _noFacets; ) {

			if (i == _noFacets-1 || facetsNodesEq(_facets[i], _facets[i+1]) != 1) {
				_facets[i][5] = 1;				
				for (int j = 1; j < 4; ++j) {
					_bndData[_facets[i][j]-1] = 1;
				}
				i++;
			} else {
#if 0
				// to jest bardzo złe...
				for (int k = 0; k < 2; ++k) {
					for (int j = 1; j < 4; ++j) {
						_bndData[_facets[i+k][j]-1] = 0;
					}
				}
#endif
				i += 2;
			}
		}
		
		for (int i = 0; i < _noFacets; i++) {
			int f[3];
			for (int j=0; j<3; j++)
				f[j] = _facets[i][j+1];
			//qsort(f, 3, sizeof(int), nodesCmp);
			//std::cout << "$$$ " << f[0] << ", " << f[1] << ", " << f[2] << "\t\t" << _facets[i][5] << std::endl;
		}
	}

	void GridFEBounder::getBndGrid(GridFE *grid) const
	{
		// policz węzły zewnętrzne
		int noBndNodes = 0;
		for (int i = 0; i < noNodes(); ++i) {
			if (_bndData[i] == 1) {
				noBndNodes++;
			}
		}
		// policz ściany zewnętrzne
		int noBndFacets = 0;
		for (int i = 0; i < noFacets(); ++i) {
			if (_facets[i][5] == 1) {
				noBndFacets++;
			}
		}
		// nowe numery dla węzłów
		int *nodesNumb; memAlloc(&nodesNumb, sizeof(int) * noBndNodes);
		for (int i = 0, j = 0; i < noNodes(); ++i) {
			if (_bndData[i] == 1) {
				nodesNumb[j++] = i;
			}
		}
		// nowe numery dla węzłów - odwrócone
		int *nodesNumbInv; memAlloc(&nodesNumbInv, sizeof(int) * _noNodes);
		for (int i = 0; i < noBndNodes; ++i) {
			nodesNumbInv[nodesNumb[i]] = i+1;
		}
		// nowe numery dla elementów (ścian)
		int *facetsNumb; memAlloc(&facetsNumb, sizeof(int) * noBndFacets);
		for (int i = 0, j = 0; i < noFacets(); ++i) {
			if (_facets[i][5] == 1) {
				facetsNumb[j++] = i;
			}
		}
		// tworzymy siatkę
//		grid->initBnds(1);
		grid->initNodes(noBndNodes);
		grid->initElems(noBndFacets);

		for (int i = 0; i < noBndNodes; ++i) {
			grid->nodeNr(i) = i+1;
			grid->setCoords(i, 
				_coords[nodesNumb[i]][0],
				_coords[nodesNumb[i]][1],
				_coords[nodesNumb[i]][2]
			);
		}

		for (int i = 0; i < noBndFacets; ++i) {
			grid->elemNr(i) = i+1;
			grid->elemSubdNr(i) = 1;
			grid->createT3n2D(i);
			for (int j = 0; j < 3; ++j) {
				grid->elemNodeNr(i, j)
					= nodesNumbInv[_facets[facetsNumb[i]][j+1]-1];
			}
		}
	}
} // namespace memorph
