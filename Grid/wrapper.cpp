#include <iostream>
#include <string>
#include <cstdlib>
#include "GridFE.h"
#include "GridFEBounder.h"
#include "GridFEWrapper.h"
#include "GridFEReader.h"
#include "GridFEWriter.h"
#include "GridFEAux.h"
#include "GridFEAuxWriter.h"

using namespace std;
using namespace memorph;

int main(int argc, char **argv)
{
	if (argc < 2) {
		std::cout << argv[0] << " - owija model siatką pomocniczą (z zadanym epsilonem)" << std::endl;
		std::cout << "Argumenty: " << std::endl;
		std::cout << "\t" << "argv[1] - siatka wejściowa" << std::endl;
		std::cout << "\t" << "argv[2] - siatka wyjściowa" << std::endl;
		std::cout << "\t" << "argv[3] - liczba przekrojów" << std::endl;
		std::cout << "\t" << "argv[4] - liczba punktów w przekroju" << std::endl;
		std::cout << "\t" << "argv[5] - epsilon" << std::endl;
	}

	real eps = 0;

	if (argc > 5) {
		eps = atof(argv[5]);
	}

	GridFEBounder *gridBnd = new GridFEBounder();
	GridFEAux *gridAux = new GridFEAux();

	GridFEReader *gridReader = new GridFEReader(argv[1]);
	gridReader->read(gridBnd);
	delete gridReader;

	GridFE *surface = new GridFE();

	gridBnd->createFacets();
	gridBnd->sortFacets();
	gridBnd->markExternals();
	gridBnd->getBndGrid(surface);

	GridFEWrapper *gridWrapper = new GridFEWrapper();
	gridWrapper->setGrid(surface);
	gridWrapper->setNoSlices(atof(argv[3]));
	gridWrapper->setNoPtsInSlice(atof(argv[4]));
	gridWrapper->wrap(gridAux, eps);
	delete gridWrapper;

	GridFEAuxWriter *gridAuxWriter= new GridFEAuxWriter(argv[2]);
	gridAuxWriter->write(gridAux);
	delete gridAuxWriter;

//	grid->free();
//	gridAux->getSrcGrid(grid);

	GridFEWriter *gridWriter = new GridFEWriter(argv[2]);
	gridWriter->write(*((GridFE *)gridAux));
	delete gridWriter;
	
	gridWriter = new GridFEWriter(argv[6]);
	gridWriter->write(*surface);
	delete gridWriter;

	return 0;
}
