#ifndef CONFIG_H
#define CONFIG_H

#include "Base.h"

namespace memorph {

	class Config {
	public:
		static const size_t noParams = 2;

	private:
		int _noMsrmnts;
		real **_msrmnts;

	public:
		Config();
		Config(int noMsrmnts);
		virtual ~Config();

		void init(int noMsrmnts);
		void free();

		const real &msrmnts(int i, int j) const;
		real &msrmnts(int i, int j);

		int noMsrmnts() const;

	}; // class Config

} // namespace memorph

#endif // CONFIG_H
