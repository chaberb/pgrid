#include "GridFEWriter.h"
#include "GridFE.h"

#include <iostream>
#include <iomanip>

namespace memorph {

	GridFEWriter::GridFEWriter():
			Writer() { }

	GridFEWriter::GridFEWriter(const std::string &path):
			Writer(path) { }

	GridFEWriter::~GridFEWriter() { }

	bool GridFEWriter::_writeInfo(const GridFE &grid)
	{
		_output << std::endl
				<< std::endl;
		_output << "Finite element mesh (GridFE):"
				<< std::endl;
		_output << std::endl;

		_output << "  Number of space dim. =  " << grid.noSpaceDim() << std::endl;
		_output << "  Number of elements   =  " << grid.noElems() << std::endl;
		_output << "  Number of nodes      =  " << grid.noNodes() << std::endl;
		_output << std::endl;


		_output << "  All elements are of the same type : dpTRUE" << std::endl;
		_output << "  Max number of nodes in an element: 4" << std::endl;
		_output << "  Only one subdomain: dpFALSE" << std::endl;
		_output << "  Lattice data ? 0" << std::endl;
		_output << std::endl
				<< std::endl
				<< std::endl;

		return _output.good();
	}

	bool GridFEWriter::_writeBndInds(const GridFE &grid)
	{
		_output << "  " << grid.noBnds() << " Boundary indicators:";
		for (int i = 0; i < grid.noBnds(); ++i)
			_output << " " << grid.bndName(i);
		_output << std::endl
				<< std::endl;
		return _output.good();
	}

	bool GridFEWriter::_writeNodes(const GridFE &grid)
	{
		_output << "  Nodal coordinates and nodal boundary indicators," << std::endl;
		_output << "  the columns contain:" << std::endl;
		_output << "   - node number" << std::endl;
		_output << "   - coordinates" << std::endl;
		_output << "   - no of boundary indicators that are set (ON)" << std::endl;
		_output << "   - the boundary indicators that are set (ON) if any." << std::endl;
		_output << "#" << std::endl;

		for (int i = 0; i < grid.noNodes(); ++i) {
			_output.width(7);
			_output << std::right << grid.nodeNr(i);
			_output << " ( " << std::fixed << std::setprecision(6)
				<< grid.coord(i, 0) << ", "
				<< grid.coord(i, 1) << ", "
				<< grid.coord(i, 2) << ") ";
			_output << "[" << grid.noNodeBndInds(i) << "]";

			for (int j = 0; j < grid.noNodeBndInds(i); ++j) {
				_output << " " << grid.nodeBndInd(i, j);
			}
			_output << std::endl;

		}

		_output << std::endl;
		return _output.good();
	}

	bool GridFEWriter::_writeElems(const GridFE &grid)
	{
		_output << "  Element types and connectivity" << std::endl;
		_output << "  the columns contain:" << std::endl;
		_output << "   - element number" << std::endl;
		_output << "   - element type" << std::endl;
		_output << "   - subdomain number" << std::endl;
		_output << "   - the global node numbers of the nodes in the element." << std::endl;
		_output << "#" << std::endl;

		int elemType;

		for (int i = 0; i < grid.noElems(); ++i)
		{
			_output.width(7);
			_output << std::right << grid.elemNr(i);
			elemType = grid.elemType(i);
			if (elemType == GridFE::T3n2D) {
				_output << "  ElmT3n2D    " <<
						//grid._elems[i].subdNr
						grid.elemSubdNr(i)
						<< "   ";
			} else if (elemType == GridFE::T4n3D) {
				_output << "  ElmT4n3D    " <<
						//grid._elems[i].subdNr
						grid.elemSubdNr(i)
						<< "   ";
			} else {
				std::cerr << "Nieobsługiwany typ elementu "
						<< "\""
						<< elemType
						<< "\""
						<< std::endl;
			}

			for (int j = 0; j < elemType; ++j) {
				_output.width(8);
				//_output << std::right << grid._elems[i].nodes[j];
				_output << std::right << grid.elemNodeNr(i,j);
			}

			_output << std::endl;
		}
		return _output.good();
	}

	bool GridFEWriter::write(const GridFE &grid)
	{
		if (!_writeInfo(grid)) {
			return false;
		}
		if (!_writeBndInds(grid)) {
			return false;
		}
		if (!_writeNodes(grid)) {
			return false;
		}
		if (!_writeElems(grid)) {
			return false;
		}
		return true;
	}

} // namespace memorph

#if 0

#include "GridFEReader.h"
#include "GridFEWriter.h"

#include <iostream>

using namespace std;
using namespace memorph;

int main(int, char **)
{
	cout << "Jazda!" << endl;
	GridFE grid1;
	GridFE grid2;
	GridFEReader gridReader("/home/borysiam/grid/sphere-c.grid");
	gridReader.read(&grid1);
	grid2.initBnds(grid1.noBnds());
	for (int i = 0; i < grid1.noBnds(); ++i) {
		grid2.bndNr(i) = grid1.bndNr(i);
		grid2.bndName(i, grid1.bndName(i));
	}
	grid2.initNodes(grid1.noNodes());
	for (int i = 0; i < grid1.noNodes(); ++i) {
		grid2.nodeNr(i) = grid1.nodeNr(i);
		grid2.initNodeBndInds(i, grid1.noNodeBndInds(i));
		for (int j = 0; j < grid1.noNodeBndInds(i); ++j) {
			grid2.nodeBndInd(i, j) = grid1.nodeBndInd(i, j);
		}
		grid2.coords(i, 0) = grid1.coords(i, 0);
		grid2.coords(i, 1) = grid1.coords(i, 1);
		grid2.coords(i, 2) = grid1.coords(i, 2);

	}
	grid2.initElems(grid1.noElems());
	for (int i = 0; i < grid1.noElems(); ++i) {
		grid2.elemNr(i) = grid1.elemNr(i);
		grid2.initElemNodes(i, grid1.elemType(i));
//		cout << grid1.elemType(i) << endl;
		for (int j = 0; j < grid1.noElemNodes(i); ++j) {
			grid2.elemNodeNr(i, j) = grid1.elemNodeNr(i, j);
		}
	}

	GridFEWriter gridWriter("/home/borysiam/grid/sphere-c.out.grid");
	gridWriter.write(grid2);
	return 0;
}

#endif
