#include "GridFEAuxWriter.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>

#include "GridFEAux.h"

namespace memorph {

	GridFEAuxWriter::GridFEAuxWriter() { }

	GridFEAuxWriter::GridFEAuxWriter(const std::string &prefix) 
	{ 
		setPath(prefix);
	}

	GridFEAuxWriter::~GridFEAuxWriter() { }

	static const int exitErrCode = 1;

	void GridFEAuxWriter::_open(const std::string &path, std::ofstream &output) const
	{
		output.open(path.c_str());

		if (!output.is_open()) {
			/* TODO - jakieś zwalnianie przed exit() */
			std::cerr 
				<< "Nie udało się otworzyć pliku " << path << " do zapisu"
				<< std::endl;
			exit(exitErrCode);
		} else {
			std::cout
				<< "Plik " << path << " został otwarty do zapisu"
				<< std::endl;
		}
	}

	void GridFEAuxWriter::_close(std::ofstream &output) const
	{
		output.close();
		std::cout 
			<< "Zapis do pliku zakończonył się powodzeniem"
			<< std::endl;
	}

	void GridFEAuxWriter::_good(std::ofstream &output) const
	{
		if (!output.good()) {
			/* TODO - wyrzucić... chyba niemożliwe przy odczycie... */
			if (output.eof()) {
				std::cerr << "Nieoczekiwany koniec pliku" << std::endl;
			} else {
				std::cerr << "Błąd strumienia" << std::endl;
			}
			exit(exitErrCode);
		}
	}

	bool GridFEAuxWriter::writeNodes(const std::string &path, const GridFEAux *grid) const
	{
		std::ofstream output;

		_open(path, output);

		output 
			<< grid->noSlices() << " " 
			<< grid->noPtsInSlice() << " "
			<< grid->noSpaceDim() << std::endl;

		int noNodes = grid->noNodes();
		int noSpaceDim = grid->noSpaceDim();

		for (int i = 0; i < noNodes; ++i) {

			output << grid->nodeNr(i) << " ";

			for (int j = 0; j < noSpaceDim; ++j) {
				output << grid->coord(i, j) << " ";
			}

			output << std::endl;

			_good(output);

		}

		_close(output);

		return true;
	}

	bool GridFEAuxWriter::writeElems(const std::string &path, const GridFEAux *grid) const
	{
		std::ofstream output;

		_open(path, output);

		int noElems = grid->noElems();

		output << std::endl;
		output << noElems << std::endl;
		output << std::endl;
		output << std::endl;

		int elemType;
		for (int i = 0; i < noElems; ++i) {
			output << grid->elemNr(i) << " ";
			elemType = grid->elemType(i);
			if (elemType == GridFE::T3n2D) {
				output << "ElmT3n2D";
			} else if (elemType == GridFE::T4n3D) {
				output << "ElmT4n3D";
			} else {
				std::cerr << "Nieobsługiwany typ elementu" << std::endl;
				return false;
			}
			output << " ";
			for (int j = 0; j < grid->noElemNodes(i); ++j) {
				output << grid->elemNodeNr(i, j) << " ";
			}
			output << std::endl;

			_good(output);
		}

		_close(output);

		return true;
	}

	bool GridFEAuxWriter::writeVects(const std::string &path, const GridFEAux *grid) const
	{
#if 0
		std::ofstream output;

		_open(path, output);

		output << grid->noVects() << std::endl;

		int noVects = grid->noNodes();
		int noSpaceDim = grid->noSpaceDim();

		for (int i = 0; i < noVects; ++i) {

			output << grid->vectNr(i) << " ";

			for (int j = 0; j < noSpaceDim; ++j) {
				output << grid->vect(i, j) << " ";
			}

			output << std::endl;

			_good(output);

			}
		}

		_close(output);
#endif
		return true;
	}

	bool GridFEAuxWriter::writeAxis(const std::string &path, const GridFEAux *grid) const
	{
		std::ofstream output;

		_open(path, output);

		int noAxisPts = grid->noSlices();

		output << std::endl;
		output << noAxisPts <<  std::endl;
		output << std::endl;

		for (int i = 0; i < noAxisPts; ++i) {
			output 
				<< i + 1 << " "
				<< grid->axisPt(i, 0) << " " 
				<< grid->axisPt(i, 1) << " "
				<< grid->axisPt(i, 2) << std::endl;

			_good(output);
		}

		_close(output);

		return true;
	}

	void GridFEAuxWriter::setNodePath(const std::string &nodePath)
	{
		_nodePath = nodePath;
	}

	void GridFEAuxWriter::setElemPath(const std::string &elemPath)
	{
		_elemPath = elemPath;
	}

	void GridFEAuxWriter::setVectPath(const std::string &vectPath)
	{
		_vectPath = vectPath;
	}

	void GridFEAuxWriter::setAxisPath(const std::string &axisPath)
	{
		_axisPath = axisPath;
	}

	void GridFEAuxWriter::setPath(const std::string &pathPref)
	{
		setNodePath(pathPref + ".node");
		setElemPath(pathPref + ".elem");
		setVectPath(pathPref + ".vect");
		setAxisPath(pathPref + ".axis");
	}

	bool GridFEAuxWriter::write(const std::string &pathPref, const GridFEAux *grid)
	{
		setPath(pathPref);
		return write(grid);
	}

	bool GridFEAuxWriter::write(const GridFEAux *grid) const
	{
		if (!writeNodes(_nodePath, grid)) {
			return false;
		}
		if (!writeElems(_elemPath, grid)) {
			return false;
		}
		if (!writeVects(_vectPath, grid)) {
			return false;
		}
		if (!writeAxis(_axisPath, grid)) {
			return false;
		}
		return true;
	}

} // namespace memorph
