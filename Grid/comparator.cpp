#include <iostream>
#include "GridFE.h"
#include "GridFEReader.h"

using namespace memorph;

int main(int argc, char **argv)
{
	if (argc < 2) {
		std::cout << argv[0] << " - porównuje dwie siatki z zadanym epsilonem" << std::endl;
		std::cout << "Argumenty: " << std::endl;
		std::cout << "\t" << "argv[1] - siatka a" << std::endl;
		std::cout << "\t" << "argv[2] - siatka b" << std::endl;
		std::cout << "\t" << "argv[3] - epsilon (dokładność, z jaką porównywane są węzły)" << std::endl;
	}

	GridFE *gridA = new GridFE();
	GridFE *gridB = new GridFE();

	GridFEReader *gridReader = new GridFEReader(argv[1]);
	gridReader->read(gridA);
	delete gridReader;

	gridReader = new GridFEReader(argv[2]);
	gridReader->read(gridB);
	delete gridReader;

	real eps = (argc > 3) ? atof(argv[3]) : 0;

	std::cout << argc << " " << eps << std::endl;

	if (gridA->noSpaceDim() != gridB->noSpaceDim()) {
		std::cout << "Siatki różnią się liczbą wymiarów przestrzeni" << std::endl;
		return 1;
	}

	if (gridA->noNodes() != gridB->noNodes()) {
		std::cout << "Siatki różnią się liczbą węzłów" << std::endl;
		return 1;
	}

	if (gridA->noElems() != gridB->noElems()) {
		std::cout << "Siatki różnią się liczbą elementów" << std::endl;
		return 1;
	}

	int noNodes = gridA->noNodes();
	int noSpaceDim = gridA->noSpaceDim();

	real coordsA[3];
	real coordsB[3];

	for (int i = 0; i < noNodes; i++) {
		for (int j = 0; j < noSpaceDim; j++) {
			gridA->getCoords(i, coordsA);
			gridB->getCoords(i, coordsB);
			if (distanceSq(coordsA, coordsB) > eps)  {
				std::cout << "Siatki różnią się węzłem o numerze " << gridA->nodeNr(i) << std::endl;
				return 1;
			}
		}
	}

	int noElems = gridA->noElems();

	for (int i = 0; i < noElems; i++) {
		int noElemNodes = gridA->noElemNodes(i);
		for (int j = 0; j < noElemNodes; j++) {
			if (gridA->elemNodeNr(i, j) != gridB->elemNodeNr(i, j)) {
				std::cout << "Siatki różnią się elementem o numerze " << i + 1 << std::endl;
				return 1;
			}
		}
	}

	std::cout << "Siatki są identyczne" << std::endl;

	return 0;
}
