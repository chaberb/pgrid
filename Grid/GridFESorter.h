#ifndef GRIDFE_SORTER_H
#define GRIDFE_SORTER_H

#include "Base.h"

namespace memorph {

	class GridFE;

	class GridFESorter {
	public:
		GridFE *_grid;

		int _noNodes;
		int _noSpaceDim;

		real **_coords;

	public:
		GridFESorter();
		virtual ~GridFESorter();

		void setGrid(GridFE *grid);

		void sortNodes(int (*comparator)(const void *, const void *), int *numb);
		void sortElems(int (*comparator)(const void *, const void *));
		void sort(GridFE *sorted);

	private:
		void _init();
		void _free();

		void _initNodesData();
		void _initElemsData();

		void _freeNodesData();
		void _freeElemsData();

		void _createNodesData();
		void _createElemsData();

	};

	
} // namespace memorph

#endif // GRIDFE_SORTER_H
