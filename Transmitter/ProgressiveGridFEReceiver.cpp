#include <ProgressiveGridFEReader.h>
#include <ProgressiveGridFEWriter.h>
#include "ProgressiveGridFEReceiver.h"

using namespace memorph;

ProgressiveGridFEReceiver::ProgressiveGridFEReceiver(std::string source, QObject *parent) :
    QObject(parent),
    _source(source),
    _pgrid(NULL),
    _splitsReceived(0),
    _maxNoSplits(-1)
{
}

ProgressiveGridFEReceiver::~ProgressiveGridFEReceiver() {
}

bool ProgressiveGridFEReceiver::receiveBaseGrid() {
    ProgressiveGridFEReader reader(_source);
    _pgrid = new ProgressiveGridFE();

    _splitsReceived = 0;

    if (!reader.read(_pgrid)) {
        return false;
    }

    _proc.process(_pgrid);

    if (_maxNoSplits < 0) {
        _maxNoSplits = _pgrid->noSplits();
    }

    emit receivedBaseGrid();
    return true;
}

bool ProgressiveGridFEReceiver::receiveSplit() {
    if (_pgrid == NULL) {
        return false;
    } else if (_splitsReceived >= _maxNoSplits) {
        std::cout << "Bazinga!" << std::endl;
        ProgressiveGridFEWriter* writer = new ProgressiveGridFEWriter("out.grid");
        int i = _pgrid->noSplits()-1;
	int j = 0;
	while (i > j) {
		int a = _pgrid->splitNr(i) - _maxNoSplits;
		int b = _pgrid->splitNr(j) - _maxNoSplits;
		_pgrid->splitNr(i) = a;
		_pgrid->splitNr(j) = b;
                _pgrid->swapSplits(i,j);
		i--; j++;
	} 
          
        writer->write(*_pgrid);
        delete writer;
        return false;
    } else {
        int splitIdx = _splitsReceived;
        int nr = _pgrid->splitNr(splitIdx);
        int a = _pgrid->splitNodeNr(splitIdx, 0);
        int b = _pgrid->splitNodeNr(splitIdx, 1);
        int c0 = _pgrid->splitCommonNodeNr(splitIdx, 0);
        int c1 = _pgrid->splitCommonNodeNr(splitIdx, 1);
        int d = _pgrid->splitDirectionNodeNr(splitIdx);
	int cfg0 = _pgrid->splitFaceConfiguration(splitIdx, 0);
	int cfg1 = _pgrid->splitFaceConfiguration(splitIdx, 1);
        real dx = _pgrid->splitVector(splitIdx, 0);
        real dy = _pgrid->splitVector(splitIdx, 1);
        real dz = _pgrid->splitVector(splitIdx, 2);

        _splitsReceived++;

        _proc.split(nr, a, b, c0, c1, d, cfg0, cfg1, dx, dy, dz);

        emit receivedNodeSplit();
        return true;
    }
}
