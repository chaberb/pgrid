include(../Grid/Grid.pri)
!contains(SOURCES, ProgressiveGridFESender.cpp) { 
    message(Project Transmitter-Sender included)
    
    INCLUDEPATH += $$PWD
    DEPENDPATH += $$PWD
    
    HEADERS += ProgressiveGridFESender.h

    SOURCES += ProgressiveGridFESender.cpp
}

HEADERS += \
    ProgressiveGridFESender.h
