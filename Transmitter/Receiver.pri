include(../Grid/Grid.pri)
!contains(SOURCES, ProgressiveGridFEReceiver.cpp) { 
    message(Project Transmitter-Receiver included)
    
    INCLUDEPATH += $$PWD
    DEPENDPATH += $$PWD
    
    HEADERS += ProgressiveGridFEReceiver.h

    SOURCES += ProgressiveGridFEReceiver.cpp
}

HEADERS += ProgressiveGridFEReceiver.h
