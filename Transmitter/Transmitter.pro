include(Sender.pri)
include(Receiver.pri)
QT += core
QT -= gui
QT -= opengl
QT -= xml
TARGET = meshsend
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
OBJECTS_DIR = obj
MOC_DIR = moc
UI_DIR = ui
SOURCES += main.cpp
