#ifndef PROGRESSIVEGRIDFERECEIVER_H
#define PROGRESSIVEGRIDFERECEIVER_H

#include <QObject>
#include <GridFE.h>
#include <ProgressiveGridFE.h>
#include <ProgressiveGridFEProcessor.h>

using namespace memorph;

class ProgressiveGridFEReceiver : public QObject
{
    Q_OBJECT
private:
    std::string _source;
    ProgressiveGridFE *_pgrid;
    ProgressiveGridFEProcessor _proc;
    int _splitsReceived;
    int _maxNoSplits;
public:
    explicit ProgressiveGridFEReceiver(std::string source, QObject *parent = 0);
    virtual ~ProgressiveGridFEReceiver();
    GridFE *grid() { return _pgrid; }
    ProgressiveGridFE *pgrid() { return _pgrid; }
    void setNoSplits(int noSplits) { _maxNoSplits = noSplits; }
signals:
    void receivedBaseGrid();
    void receivedNodeSplit();
public slots:
    bool receiveBaseGrid();
    bool receiveSplit();
};

#endif // PROGRESSIVEGRIDFERECEIVER_H
