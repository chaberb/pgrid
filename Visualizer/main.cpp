#include <getopt.h>
#include <GridFE.h>
#include <Errors.h>
#include <QApplication>
#include <VisualizerWindow.h>
#include <GridFEViewer.h>
#include <ProgressiveGridFEReceiver.h>
#include <Timer.h>
using namespace memorph;

long Timer::_counter = 1;

int main(int argc, char** argv) {
    Timer tBaseMesh;
    Timer tSplits;
    std::string source = "../Processor/arm.pgrid";
    int noSplits = -1;

    int c;

    while (true) {
        static struct option long_options[] = {
            {"no-splits",  required_argument, 0, 'n'},
            {"source",    required_argument, 0, 's'},
            {0, 0, 0, 0}
        };
        int option_index = 0;
        c = getopt_long (argc, argv, "n:s:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c) {
        case 0: break;
        case 'n': noSplits = atoi(optarg); break;
        case 's': source = std::string(optarg); break;;
        case '?': break;

        default:
            abort ();
        }
    }

    QApplication app(argc, argv);

    ProgressiveGridFEReceiver recv(source);
    GridFEViewer vis(&recv);
    recv.setNoSplits(noSplits);

    app.connect(&recv, SIGNAL(receivedBaseGrid()),
                &vis, SLOT(centerAt()));
    app.connect(&recv, SIGNAL(receivedNodeSplit()),
                &vis, SLOT(redraw()));
    app.connect(&vis, SIGNAL(redrawed()),
                &recv, SLOT(receiveSplit()));

    tBaseMesh.start();
    recv.receiveBaseGrid();
    tBaseMesh.stop().print("Reciving base mesh", false);

    tSplits.start();
    recv.receiveSplit();
    tSplits.stop().print("Reciving splits", false);

    vis.show();
    return app.exec();
}
