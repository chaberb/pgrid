#ifndef GRIDVIEWER_H
#define GRIDVIEWER_H

#include <QObject>
#include <QGLViewer/qglviewer.h>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QFont>
#include <QString>
#include <GridFE.h>

#include <ProgressiveGridFEReceiver.h>

using namespace memorph;

typedef ProgressiveGridFEReceiver GridFESource;

class GridFEViewer : public QGLViewer
{
    Q_OBJECT
private:
    GridFESource *_source;
    bool _showNodeNumbers;
    bool _showElemNumbers;
    //bool _showEdgeNumbers;
    bool _fillFaces;
    bool _wireframe;
    bool _showNormal;
    bool _shading;
    bool _blend;
    real **_normals;
    real center[3];
public:
    explicit GridFEViewer(GridFESource *source, QWidget *parent = NULL);
    ~GridFEViewer();
    virtual void draw();
    virtual void keyReleaseEvent(QKeyEvent *);
    void calculateNormals(GridFE *grid);

public slots:
    void centerAt();
    void redraw() {
        update();
        emit redrawed();
    }

signals:
    void redrawed();
};

#endif
