/********************************************************************************
** Form generated from reading UI file 'VisualizerWindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VISUALIZERWINDOW_H
#define UI_VISUALIZERWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VisualizerWindow
{
public:
    QWidget *centralwidget;

    void setupUi(QMainWindow *VisualizerWindow)
    {
        if (VisualizerWindow->objectName().isEmpty())
            VisualizerWindow->setObjectName(QString::fromUtf8("VisualizerWindow"));
        VisualizerWindow->resize(343, 259);
        centralwidget = new QWidget(VisualizerWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        VisualizerWindow->setCentralWidget(centralwidget);

        retranslateUi(VisualizerWindow);

        QMetaObject::connectSlotsByName(VisualizerWindow);
    } // setupUi

    void retranslateUi(QMainWindow *VisualizerWindow)
    {
        VisualizerWindow->setWindowTitle(QApplication::translate("VisualizerWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class VisualizerWindow: public Ui_VisualizerWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VISUALIZERWINDOW_H
