include(Visualizer.pri)
include(../Processor/Processor.pri)
include(../Transmitter/Receiver.pri)
QT += core
QT += gui
QT += opengl
QT += xml
LIBS += -lGL -lGLU
TARGET = meshview
CONFIG += console
CONFIG += app_bundle
TEMPLATE = app
OBJECTS_DIR = obj
MOC_DIR = moc
UI_DIR = ui
SOURCES += main.cpp

HEADERS += VisualizerWindow.h
