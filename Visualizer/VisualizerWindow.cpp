#include "VisualizerWindow.h"
#include "ui_VisualizerWindow.h"

VisualizerWindow::VisualizerWindow(ProgressiveGridFEProcessor *proc, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VisualizerWindow)
{
    ui->setupUi(this);
}

VisualizerWindow::~VisualizerWindow()
{
    delete ui;
}
