/****************************************************************************
** Meta object code from reading C++ file 'ProgressiveGridFEProcessor.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Processor/ProgressiveGridFEProcessor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProgressiveGridFEProcessor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProgressiveGridFEProcessor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   27,   27,   27, 0x05,

 // slots: signature, parameters, type, tag, flags
      73,   39,   27,   27, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProgressiveGridFEProcessor[] = {
    "ProgressiveGridFEProcessor\0\0splitted()\0"
    "nr,a,b,c0,c1,d,cfg0,cfg1,dx,dy,dz\0"
    "split(int,int,int,int,int,int,int,int,real,real,real)\0"
};

void ProgressiveGridFEProcessor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProgressiveGridFEProcessor *_t = static_cast<ProgressiveGridFEProcessor *>(_o);
        switch (_id) {
        case 0: _t->splitted(); break;
        case 1: _t->split((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7])),(*reinterpret_cast< int(*)>(_a[8])),(*reinterpret_cast< real(*)>(_a[9])),(*reinterpret_cast< real(*)>(_a[10])),(*reinterpret_cast< real(*)>(_a[11]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProgressiveGridFEProcessor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProgressiveGridFEProcessor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ProgressiveGridFEProcessor,
      qt_meta_data_ProgressiveGridFEProcessor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProgressiveGridFEProcessor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProgressiveGridFEProcessor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProgressiveGridFEProcessor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProgressiveGridFEProcessor))
        return static_cast<void*>(const_cast< ProgressiveGridFEProcessor*>(this));
    return QObject::qt_metacast(_clname);
}

int ProgressiveGridFEProcessor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ProgressiveGridFEProcessor::splitted()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
