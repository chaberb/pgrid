include(../Grid/Grid.pri)

!contains(SOURCES, GridFEViewer.cpp) {
    message(Project Visualizer included)
    INCLUDEPATH += $$PWD
    DEPENDPATH += $$PWD

    SOURCES += GridFEViewer.cpp
    SOURCES += VisualizerWindow.cpp

    HEADERS += GridFEViewer.h
    HEADERS += VisualizerWindow.h

    FORMS += VisualizerWindow.ui

    LIBS += -lQGLViewer -L$$PWD/QGLViewer
}

