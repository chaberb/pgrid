#include "GridFEViewer.h"
#include <QMouseEvent>
#include <QKeyEvent>
#include <QtDebug>
#include <QPainter>
#include <qgl.h>
#include <cstdio>

#include <ProgressiveGridFE.h>

using namespace qglviewer;

GridFEViewer::GridFEViewer(GridFESource *source, QWidget *parent) : QGLViewer(parent),
    _source(source),
    _showNodeNumbers(false),
    _showElemNumbers(false),
    //_showEdgeNumbers(false),
    _fillFaces(true),
    _wireframe(false),
    _showNormal(false),
    _shading(true),
    _blend(false),
    _normals(NULL)
{
    setAutoFillBackground(false);
    centerAt();
}

GridFEViewer::~GridFEViewer() {
    if (_normals != NULL) {
        delete[] _normals[0];
        delete[] _normals;
    }
}

void GridFEViewer::centerAt() {
    GridFE *grid = _source->grid();
    if (grid != NULL && grid->noNodes() > 0) {
        real min[3];
        real max[3];
        grid->getCoords(0, min);
        grid->getCoords(0, max);
        center[0] = min[0];
        center[1] = min[1];
        center[2] = min[2];
        for(int i=1; i<grid->noNodes(); ++i)
        {
            real p[3];
            grid->getCoords(i, p);
            if(p[0] < min[0]) min[0] = p[0];
            if(p[1] < min[1]) min[1] = p[1];
            if(p[2] < min[2]) min[2] = p[2];
            if(p[0] > max[0]) max[0] = p[0];
            if(p[1] > max[1]) max[1] = p[1];
            if(p[2] > max[2]) max[2] = p[2];
            center[0] += p[0];
            center[1] += p[1];
            center[2] += p[2];
        }
        center[0] /= grid->noNodes();
        center[1] /= grid->noNodes();
        center[2] /= grid->noNodes();

        setSceneBoundingBox(Vec(min[0], min[1], min[2]), Vec(max[0], max[1], max[2]));
        camera()->showEntireScene();
        update();
    }
}

void GridFEViewer::keyReleaseEvent(QKeyEvent *e) {
    switch (e->key()) {
        case Qt::Key_V : _showNodeNumbers = !_showNodeNumbers; break;
        case Qt::Key_E : _showElemNumbers = !_showElemNumbers; break;
        case Qt::Key_F : _fillFaces = !_fillFaces; break;
        case Qt::Key_W : _wireframe = !_wireframe; break;
        case Qt::Key_N : _showNormal= !_showNormal; break;
        case Qt::Key_L : _shading = !_shading; break;
        case Qt::Key_T : _blend = !_blend; break;
        default:
            QGLViewer::keyPressEvent(e);
    }

    updateGL();
}

void GridFEViewer::draw()
{
    GridFE *grid = _source->grid();
    if (grid == NULL)
        return;

    setBackgroundColor(QColor(255, 255, 255, 255));
    if (_shading) glEnable(GL_LIGHTING);
    else glDisable(GL_LIGHTING);
    if (_blend) glEnable (GL_BLEND);
    else glDisable(GL_BLEND);
    if (_blend) glDisable(GL_DEPTH_TEST);
    else glEnable(GL_DEPTH_TEST);
    if (_blend) glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_POLYGON_OFFSET_FILL);
    glPolygonOffset (1., 1.);
    glPolygonMode(GL_FRONT, GL_FILL);
    glLineWidth(1.0f);

    for (int i=0; i<grid->noElems(); i++) {
        int a = grid->elemNodeNr(i, 0)-1;
        int b = grid->elemNodeNr(i, 1)-1;
        int c = grid->elemNodeNr(i, 2)-1;
        Vector va, vb, vc;
        grid->getCoords(a, va.coords);
        grid->getCoords(b, vb.coords);
        grid->getCoords(c, vc.coords);

        if (_fillFaces) {
            glBegin(GL_TRIANGLES);
            //glColor4d(0.8, 0.7, 0.7, 0.6);
            glColor3d(1.0, 1.0, 1.0);
	    Vector u, v, n;
	    sub(vc, vb, &u);
	    sub(va, vb, &v);
	    crossProduct(u, v, &n);

	    real nlen = length(n.coords);
	    n.x /= nlen;
	    n.y /= nlen;
	    n.z /= nlen;
	    if (_shading)
		glNormal3f(n.x,n.y,n.z);
            glVertex3f(va.x, va.y, va.z);
            glVertex3f(vb.x, vb.y, vb.z);
            glVertex3f(vc.x, vc.y, vc.z);
            glEnd();
        }

	if (_wireframe) {
            glBegin(GL_LINE_LOOP);
            glColor3d(0.0, 0.0, 0.0);
            glVertex3f(va.x, va.y, va.z);
            glVertex3f(vb.x, vb.y, vb.z);
            glVertex3f(vc.x, vc.y, vc.z);
            glEnd();
	}
    }

    if(_showNodeNumbers) {
        glColor3d(0.0, 0.0, 0.0);
        for (int i=0; i<grid->noNodes(); i++) {
            int nr = grid->nodeNr(i);
            if (nr > 0) {
                real v[3];
                grid->getCoords(i, v);
                renderText(v[0], v[1], v[2], QString::number(nr), QFont("Droid Sans", 12));
            }
        }
    }

    if(_showElemNumbers) {
        glColor3d(0.0, 0.0, 0.0);
        for (int i=0; i<grid->noElems(); i++) {
            int nr = grid->elemNr(i);
            if (nr > 0) {
                real u[3];
                real v[3];
                v[0] = v[1] = v[2] = 0.0;
                for(int j=0; j<3; j++) {
                    grid->getElemNodeCoords(i, j, u);
                    v[0] += u[0]/3.0;
                    v[1] += u[1]/3.0;
                    v[2] += u[2]/3.0;
                }
                renderText(v[0], v[1], v[2], QString::number(nr), QFont("Droid Sans", 12));
            }
        }
    }
}
