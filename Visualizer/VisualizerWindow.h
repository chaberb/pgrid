#ifndef VISUALIZERWINDOW_H
#define VISUALIZERWINDOW_H

#include <QMainWindow>
#include <ProgressiveGridFEProcessor.h>

namespace Ui {
    class VisualizerWindow;
}

class VisualizerWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit VisualizerWindow(ProgressiveGridFEProcessor *proc, QWidget *parent = 0);
    ~VisualizerWindow();

private:
    Ui::VisualizerWindow *ui;
};

#endif // VISUALIZERWINDOW_H
