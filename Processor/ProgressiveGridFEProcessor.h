#ifndef PROGRESSIVEGRIDFEPROCESSOR
#define PROGRESSIVEGRIDFEPROCESSOR

#include <QObject>
#include <GridFE.h>
#include <ProgressiveGridFE.h>

using namespace memorph;

class ProgressiveGridFEProcessor : public QObject
{
    Q_OBJECT
private:
    ProgressiveGridFE *_pgrid;

public:
    explicit ProgressiveGridFEProcessor(QObject *parent = 0);
    virtual ~ProgressiveGridFEProcessor() {}

public:
    void process(ProgressiveGridFE *);
public slots:
    void split(int nr, int a, int b, int c0, int c1, int d, int cfg0, int cfg1, real dx, real dy, real dz);
signals:
    void splitted();
};

#endif
