include(Processor.pri)
QT += core
QT -= gui
QT -= opengl
QT -= xml
TARGET = meshproc
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
OBJECTS_DIR = obj
MOC_DIR = moc
UI_DIR = ui
SOURCES += main.cpp
#QMAKE_CFLAGS+=-pg
#QMAKE_CXXFLAGS+=-pg
#QMAKE_LFLAGS+=-pg
