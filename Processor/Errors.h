#ifndef ERRORS_H
#define ERRORS_H

#include <iostream>

inline void print_split(int nr, int a, int b, int c0, int c1, int d, float dx, float dy, float dz) {
    std::cout << "== Split nr ";
    std::cout << nr << " on nodes ";
    std::cout << a << " and ";
    std::cout << b << " connected with ";
    std::cout << c0 << " and ";
    std::cout << c1 << " in direction of ";
    std::cout << d << " vector: ";
    std::cout << dx << ", ";
    std::cout << dy << ", ";
    std::cout << dz << std::endl;
}

inline void err_replace_edge_failed() {
    std::cout << "Nie powiodło się zastąpienie krawędzi węzłem." << std::endl;
}

inline void err_couldnt_reconnect_faces() {
    std::cout << "Nie można odnaleźć listy ścian do przepięcia węzłów. ";
    std::cout << "Powodem jest prawdopodobnie niepoprawna definicja operacji ";
    std::cout << "podziału węzła." << std::endl;
}

inline void err_couldnt_recieve_base(const std::string &source) {
    std::cout << "Nie można odebrać siatki podstawowej z lokalizacji: ";
    std::cout << source << std::endl;
}

inline void err_couldnt_add_elem(int maxElems) {
    std::cout << "Nie można dodać więcej elementów niż zarezerwowane ";
    std::cout << maxElems << std::endl;
}

inline void err_couldnt_add_node(int maxNodes) {
    std::cout << "Nie można dodać więcej węzłów niż zarezerwowane ";
    std::cout << maxNodes << std::endl;
}

inline void err_couldnt_add_split(int maxSplits) {
    std::cout << "Nie można dodać więcej operacji podziału węzła niż zarezerwowane ";
    std::cout << maxSplits << std::endl;
}

inline void err_flips_orientation() {
    std::cout << "Nie można zwinąć krawędzi, ponieważ ";
    std::cout << "zmienia to orientację ściany.";
    std::cout << std::endl;
}

inline void err_too_many_edges(int noEdges, int noElems) {
    std::cout << "Siatka nie jest siatką powierzchniową,";
    std::cout << " ponieważ posiada więcej niż " << noEdges << " krawędzi";
    std::cout << " przy " << noElems << " ścianach.";
    std::cout << " Niektóre krawędzie łączą więcej niż dwie ściany.";
    std::cout << std::endl;
}

inline void err_too_few_edges(int noEdges, int noElems) {
    std::cout << "Siatka nie jest siatką powierzchniową,";
    std::cout << " ponieważ posiada mniej (" << noEdges << ") niż " << (3.0/2.0) * noElems << " krawędzi";
    std::cout << " przy " << noElems << " ścianach.";
    std::cout << " Nie wszystkie krawędzie łączą ze sobą dwie ściany.";
    std::cout << std::endl;
}

inline void err_too_many_common_nodes(int a, int b) {
    std::cout << "Siatka nie jest siatką powierzchniową,";
    std::cout << " ponieważ krawędź pomiędzy " << a << " i " << b;
    std::cout << " posiada więcej niż dwa 'wspólne węzły'.";
    std::cout << std::endl;
}

inline void err_to_many_invalid_faces() {
    std::cout << "Siatka nie jest siatką powierzchniową,";
    std::cout << " ponieważ posiada więcej niż dwie";
    std::cout << " nieprawidłowe ściany po zwinięciu.";
    std::cout << std::endl;
}

inline void err_to_many_invalid_edges() {
    std::cout << "Siatka nie jest siatką powierzchniową,";
    std::cout << " ponieważ posiada więcej niż trzy";
    std::cout << " nieprawidłowe krawędzie po zwinięciu.";
    std::cout << std::endl;
}

inline void err_edge_to_update_not_found(int idx) {
    std::cout << "Nie udało się odnaleźć krawędzi o indeksie " << idx;
    std::cout << " w celu jej uaktualnienia.";
    std::cout << std::endl;

}

inline void err_edge_to_remove_not_found(int idx) {
    std::cout << "Nie udało się odnaleźć krawędzi o indeksie " << idx;
    std::cout << " w celu jej usunięcia.";
    std::cout << std::endl;

}

inline void err_invalid_face_configuration() {
    std::cout << "Nieprawidłowa konfiguracja węzłów w elemencie.";
    std::cout << std::endl;
}
#endif // ERRORS_H
