#include <vector>
#include <algorithm>
#include <cmath>
#include "GridFEProcessor.h"
#include <Errors.h>

#include <Timer.h>

GridFEProcessor::GridFEProcessor():
    _minNodesNo(4),
    _minFacesNo(4),
    _maxVolume(99999.9),
    _maxTotalVolume(99999.9),
    _minAngle(0.0),
    _totalVolume(0.0) {}

void GridFEProcessor::process(GridFE &grid) {
    Timer tClone, tEdges, tSplits, tEval, tProcess;
    tProcess.start();

    std::cout << "Processing grid until one of following ";
    std::cout << "requirements:" << std::endl;
    std::cout << " - number of nodes < " << _minNodesNo << std::endl;
    std::cout << " - number of faces < " << _minFacesNo << std::endl;
    std::cout << " - max total volume change > " << _maxTotalVolume << std::endl;

    std::cout << "Cloning grid..." << std::endl;

    tClone.start();
    _pgrid.clone(grid);
    tClone.stop().print("Cloning grid");

    std::cout << "[done]" << std::endl;

    std::cout << "Initiating grid edges..." << std::endl;

    tEdges.start();
    _pgrid.initEdges();
    tEdges.stop().print("Init edges");

    std::cout << "[done]" << std::endl;

    std::cout << "Initiating splits..." << std::endl;

    tSplits.start();
    _pgrid.initSplits();
    tSplits.stop().print("Init spltis");

    std::cout << "[done]" << std::endl;

    std::cout << "Evaluating edges..." << std::endl;

    tEval.start();
    evaluateEdges();
    tEval.stop().print("Evaluation");

    std::cout << "[done]" << std::endl;
    std::cout << "Contracting edges..." << std::endl;

    Timer::resetCounter();
    while(contractNext()) /*void*/;

    tProcess.stop().print("Whole process");
}

bool GridFEProcessor::evaluateEdges() {
    for (int edgeIdx=0; edgeIdx < _pgrid.noEdges(); edgeIdx++) {
        Edge edge(edgeIdx);
        _evaluate(edge);
        _edges.push_back(edge);
    }
    _edges.sort();
    return true;
}

bool GridFEProcessor::_evaluate(Edge &edge) {
    int noFaces = 0;
    int noNodes = 0;
    int noEdges = 0;
    int* faceNrs = NULL;
    int* nodeNrs = NULL;
    int* edgeNrs = NULL;
    int a, b;

    Vector center;
    Vector vector;
    Vector position;
    Vector*** faceCoords;
    int*** faceNodes;
    int*** edgeNodes;

    a = _pgrid.edgeNodeNr(edge.idx, 0);
    b = _pgrid.edgeNodeNr(edge.idx, 1);
    if (a > b) swap(a, b);
    // std::cout << "::Evaluating edge (" << edge.sig << ") nr " << _pgrid.edgeNr(edge.idx);
    // std::cout << "  between " << a << " and " << b << std::endl;
    _pgrid.adjancentFaces(&noFaces, &faceNrs, a, b);
    _pgrid.adjancentNodes(&noNodes, &nodeNrs, a, b);
    _pgrid.adjancentEdges(&noEdges, &edgeNrs, a, b);

    _findVector(a, b, vector);
    _findNewPosition(a, vector, position);
    _findCenter(nodeNrs, noNodes, center);

    // std::cout << "Displacement vector " << vector.x << ", " << vector.y << ", " << vector.z << std::endl;
    // std::cout << "New node position " << position.x << ", " << position.y << ", " << position.z << std::endl;

    // init
    faceCoords = new Vector **[2];
    faceCoords[0] = new Vector *[noFaces];
    faceCoords[1] = new Vector *[noFaces];

    faceNodes = new int **[2];
    faceNodes[0] = new int *[noFaces];
    faceNodes[1] = new int *[noFaces];

    edgeNodes = new int **[2];
    edgeNodes[0] = new int *[noEdges];
    edgeNodes[1] = new int *[noEdges];

    for (int i=0; i<noFaces; i++) {
        faceCoords[0][i] = new Vector[3];
        faceCoords[1][i] = new Vector[3];
        faceNodes[0][i] = new int[3];
        faceNodes[1][i] = new int[3];

        for (int j=0; j<3; j++) {
            int nodeNr = _pgrid.elemNodeNr(faceNrs[i]-1, j);
            int nodeIdx = nodeNr-1;//NR2IDX

            faceNodes[0][i][j] = nodeNr;
            _pgrid.getCoords(nodeIdx, faceCoords[0][i][j].coords);

            if (nodeNr == a || nodeNr == b)
                copy(position, &faceCoords[1][i][j]);
            else
                _pgrid.getCoords(nodeIdx, faceCoords[1][i][j].coords);

            if (nodeNr == b)
                faceNodes[1][i][j] = a;
            else
                faceNodes[1][i][j] = nodeNr;
        }
    }
    for (int i=0; i<noEdges; i++) {
        edgeNodes[0][i] = new int[2];
        edgeNodes[1][i] = new int[2];

        for (int j=0; j<2; j++) {
            int nodeNr = _pgrid.edgeNodeNr(edgeNrs[i]-1, j);

            edgeNodes[0][i][j] = nodeNr;

            if (nodeNr == b)
                edgeNodes[1][i][j] = a;
            else
                edgeNodes[1][i][j] = nodeNr;
        }
    }

    _evaluateGeometry(noFaces, faceCoords, center, edge);
    _evaluateTopology(a, b, noFaces, faceNodes, noEdges, edgeNodes, edge);

    // std::cout << ":::: edge info: ";
    // std::cout << edge.dVolume << ", ";
    // if (edge.flipsFaces)
    //    std::cout << "[flips faces]";
    // if (edge.badCommonNodesNo)
    //    std::cout << "[bad common nodes number]";
    // if (edge.badInvalidFacesNo)
    //    std::cout << "[bad invalid faces number]";
    // if (edge.badInvalidEdgesNo)
    //    std::cout << "[bad invalid edges number]";
    // std::cout << std::endl;
    // cleaning
    for (int i=0; i<noFaces; i++) {
        delete[] faceCoords[0][i];
        delete[] faceCoords[1][i];
        delete[] faceNodes[0][i];
        delete[] faceNodes[1][i];
    }
    for (int i=0; i<noEdges; i++) {
        delete[] edgeNodes[0][i];
        delete[] edgeNodes[1][i];
    }
    delete[] faceCoords[0];
    delete[] faceCoords[1];
    delete[] faceNodes[0];
    delete[] faceNodes[1];
    delete[] edgeNodes[0];
    delete[] edgeNodes[1];
    delete[] faceNrs;
    delete[] edgeNrs;
    delete[] nodeNrs;
    return true;
}

bool GridFEProcessor::_evaluateGeometry(int noFaces, Vector*** facesCoords,
                                        const Vector &center,
                                        Edge &edge) {
    Vector tetra[4];
    // orientation
    Vector ori0, ori1;
    // volume
    real vol0 = 0.0;
    real vol1 = 0.0;
    // angle
    real minAngleCos = -1.0; // cos(pi) = -1;
    bool faceFlipped = false;

    copy(center, &tetra[3]);
    for (int i=0; i<noFaces; i++) {
        Vector a, b, c, m, n;
        real v=0;
        real angleCos=0.0;
        real alen, blen, clen;

        sub(facesCoords[0][i][0], facesCoords[0][i][1], &a);
        sub(facesCoords[0][i][0], facesCoords[0][i][2], &b);
        sub(facesCoords[0][i][1], facesCoords[0][i][2], &c);
        copy(facesCoords[0][i][0], &tetra[0]);
        copy(facesCoords[0][i][1], &tetra[1]);
        copy(facesCoords[0][i][2], &tetra[2]);
        alen = len(a);
        blen = len(b);
        clen = len(c);

        v = volume(tetra);
        vol0 += progrid::isnan(v)? 0.0 : v;

        crossProduct(a, b, &ori0);

        angleCos = std::fabs(dotProduct(a, b))/(alen*blen);
        minAngleCos = (angleCos > minAngleCos)? angleCos : minAngleCos;

        angleCos = std::fabs(dotProduct(b, c))/(blen*clen);
        minAngleCos = (angleCos > minAngleCos)? angleCos : minAngleCos;

        angleCos = std::fabs(dotProduct(a, c))/(alen*clen);
        minAngleCos = (angleCos > minAngleCos)? angleCos : minAngleCos;

        sub(facesCoords[1][i][0], facesCoords[1][i][1], &m);
        sub(facesCoords[1][i][0], facesCoords[1][i][2], &n);
        copy(facesCoords[1][i][0], &tetra[0]);
        copy(facesCoords[1][i][1], &tetra[1]);
        copy(facesCoords[1][i][2], &tetra[2]);

        v = volume(tetra);
        vol1 += progrid::isnan(v)? 0.0 : v;

        crossProduct(m, n, &ori1);

        if (len(ori0) > 0.0 && len(ori1) > 0.0 && dotProduct(ori0, ori1) < 0.0) // different signs
            faceFlipped = true;
    }

    edge.volumeChange = std::fabs(vol0-vol1);
    edge.minAngle = std::acos(minAngleCos);

    if (edge.minAngle < _minAngle)
        edge.tooSmallMinAngle = true;
    else
        edge.tooSmallMinAngle = false;

    if (edge.volumeChange > _maxVolume)
        edge.tooGreatVolumeChange = true;
    else
        edge.tooGreatVolumeChange = false;

    if (faceFlipped)
        edge.flipsFaces = true;
    else
        edge.flipsFaces = false;

    return true;
}

bool GridFEProcessor::_evaluateTopology(int a, int b,
                                        int noFaces, int*** faceNodes,
                                        int noEdges, int*** edgeNodes,
                                        Edge &edge) {
    int unused[3];
    TableEdges validEdges(edgeNodes[0], noEdges);
    if (_findCommonNodes(a, b, validEdges, unused) == false) {
        edge.badCommonNodesNo = true;
    } else {
        edge.badCommonNodesNo = false;
    }

    return true;
}

bool GridFEProcessor::contractNext() {
    Timer tReeval(true);

    bool valid = true;

    if (_edges.size() <= 0)
        return false;

    // std::cout << "\n========== CONTRACTION ============\n";
    // std::cout << "== list of edges ==" << std::endl;
    // std::list<Edge>::iterator it;
    // for (it = _edges.begin(); it != _edges.end(); it++) {
    //    Edge edge = (*it);
    //    std::cout.width(3);
    //    std::cout << _pgrid.edgeNr(edge.idx) << ". ";
    //    std::cout << " " << _pgrid.edgeNodeNr(edge.idx, 0);
    //    std::cout << "--" << _pgrid.edgeNodeNr(edge.idx, 1);
    //    std::cout << ", " ;
    //    std::cout.precision(2);
    //    std::cout.width(5);
    //    std::cout << edge.dVolume;
    //    std::cout << (edge.flipsFaces? " f": "");
    //    std::cout << (edge.badCommonNodesNo? " f": "");
    //    std::cout << (edge.badInvalidEdgesNo? " ie": "");
    //    std::cout << (edge.badInvalidFacesNo? " if": "");
    //    std::cout << std::endl;
    //// std::cout << ", valid: " << (edge.valid()? "yes" : "no") << std::endl;
    // }
    // std::cout << std::endl;

    // checkGrid("Before contraction");

    Edge edge = _edges.front();

    valid = valid && (_totalVolume + edge.volumeChange) <= _maxTotalVolume;
    valid = valid && (_pgrid.noNodes() - 1) >= _minNodesNo;
    valid = valid && (_pgrid.noElems() - 2) >= _minFacesNo;

    int a,b;
    a = _pgrid.edgeNodeNr(edge.idx, 0);
    b = _pgrid.edgeNodeNr(edge.idx, 1);
    if (a > b) swap(a, b);

    // std::cout << "::Picked: Edge (" << edge.sig << ") nr " << _pgrid.edgeNr(edge.idx) << ", ";
    // std::cout << a << "--" << b;
    // std::cout << " rated " << edge.dVolume << " is valid? ";
    // std::cout << (edge.valid() ? "yes" : "no") << std::endl;
    // std::cout << "Requirements are met? " << (valid? "yes" : "no") << std::endl;
    if (!valid) {
        if ((_totalVolume + edge.volumeChange) > _maxTotalVolume)
            std::cout << "Volume difference is above limit." << std::endl;
        if ((_pgrid.noNodes() - 1) < _minNodesNo)
            std::cout << "Number of nodes is below limit." << std::endl;
        if ((_pgrid.noElems() - 2) < _minFacesNo)
            std::cout << "Number of faces is below limit." << std::endl;
        return false;
    }

    if (edge.invalid()) {
        std::cout << "There is no valid edge for contraction"  << std::endl;
        return false;
    }

    bool ok = _replaceEdgeWithNode(a, b);

    if (!ok) {
        err_replace_edge_failed();
        return false;
    }

    _totalVolume += edge.volumeChange;
    std::cerr << _pgrid.noSplits() << ":" << _totalVolume << std::endl;
    // std::cout << "Contracted " << contractedVolume << " so far";
    tReeval.start();
    _reevaluate(a);
    tReeval.stop().print(" Reevaluation", true);

    _edges.sort();

    std::cout << "N: " << _pgrid.noNodes();
    std::cout << ", F: " << _pgrid.noElems();
    std::cout << ", V: " << _totalVolume << std::endl;
    // checkGrid("After contraction");
    return true;
}

bool GridFEProcessor::_replaceEdgeWithNode(int a, int b) {
    int* facesNrs = NULL;
    int* edgesNrs = NULL;
    int noFaces = 0;
    int noEdges = 0;
    int directionNodeNr = 0;
    int commonNodesNr[2] = {0, 0};
    int invalidFaceNrs[2] = {0, 0};
    int invalidEdgeNrs[3] = {0, 0, 0};
    int splitIdx=-1;

    _pgrid.adjancentFaces(&noFaces, &facesNrs, a, b);
    _pgrid.adjancentEdges(&noEdges, &edgesNrs, a, b);
    
    // std::cout << ":::: adj. faces: ";
    // std::cout << facesNrs[0];
    // for (int i=1; i<noFaces; i++) {
    //   std::cout << ", " << facesNrs[i];
    // }
    // std::cout << "." << std::endl;
    // std::cout << ":::: adj. edges: ";
    // std::cout << edgesNrs[0];
    // for (int i=1; i<noEdges; i++) {
    //    std::cout << ", " << edgesNrs[i];
    // }
    // std::cout << "." << std::endl;

    ProgressiveGridFEEdges edges(_pgrid, edgesNrs, noEdges);
    GridFEFaces faces(_pgrid, facesNrs, noFaces);

    Vector position;
    Vector newPosition;
    Vector vector;

    _findVector(a, b, vector);
    if (_findCommonNodes(a, b, edges, commonNodesNr) == false) {
        std::cout << "========== ERROR ==============" << std::endl;
        err_too_many_common_nodes(a, b);
        std::cout << "===============================" << std::endl;
        return false;
    }
    
    // std::cout << ":::: common nodes nrs: " << commonNodesNr[0] << ", " << commonNodesNr[1] << std::endl;
    _findDirectionNode(a, b, commonNodesNr[0], faces, directionNodeNr);
    // std::cout << ":::: direction node nr: " << directionNodeNr << std::endl;
    // checkGrid("Before reconnection");
    int cfg0 = _findFaceConfiguration(a, b, commonNodesNr[0], faces);
    int cfg1 = _findFaceConfiguration(a, b, commonNodesNr[1], faces);
    _pgrid.reconnectNode(b, a);
    _pgrid.getCoords(a-1, position.coords);
    sub(position, vector, &newPosition);
    _pgrid.setCoords(a-1, newPosition.coords);

    // checkGrid("After reconnection");
    // std::cout << ":::: reconnected nodes: " << b << " -> " << a << std::endl;

    if (_findInvalidEdges(edges, invalidEdgeNrs) == false) {
        std::cout << "========== ERROR ==============" << std::endl;
        err_to_many_invalid_edges();
        std::cout << "===============================" << std::endl;
        return false;
    }

    if (_findInvalidFaces(faces, invalidFaceNrs) == false) {
        std::cout << "========== ERROR ==============" << std::endl;
        err_to_many_invalid_faces();
        std::cout << "===============================" << std::endl;
        return false;
    }

    splitIdx = _pgrid.addSplit();
    _pgrid.splitNodeNr(splitIdx, 0) = a;
    _pgrid.splitNodeNr(splitIdx, 1) = b;
    _pgrid.splitCommonNodeNr(splitIdx, 0) = commonNodesNr[0];
    _pgrid.splitCommonNodeNr(splitIdx, 1) = commonNodesNr[1];
    _pgrid.splitDirectionNodeNr(splitIdx) = directionNodeNr;
    _pgrid.splitFaceConfiguration(splitIdx, 0) = cfg0;
    _pgrid.splitFaceConfiguration(splitIdx, 1) = cfg1;
    _pgrid.splitVector(splitIdx, 0) = vector.x;
    _pgrid.splitVector(splitIdx, 1) = vector.y;
    _pgrid.splitVector(splitIdx, 2) = vector.z;

    // checkGrid("Before face removal");
    // std::cout << ":::: " << _edges.size() << " rated edges on list before contraction. " << std::endl;
    // std::cout << ":::: " << _pgrid.noElems() << " faces before removal." << std::endl;
    // std::cout << ":::: " << _pgrid.noEdges() << " edges before removal." << std::endl;

    sort2(invalidFaceNrs);
    sort3(invalidEdgeNrs);

    // std::cout << ":::: Removing faces ";
    // std::cout << invalidFaceNrs[0] << " and ";
    // std::cout << invalidFaceNrs[1] << std::endl;

    // std::cout << ":::: Removing edges ";
    // std::cout << invalidEdgeNrs[0] << ", ";
    // std::cout << invalidEdgeNrs[1] << " and ";
    // std::cout << invalidEdgeNrs[2] << std::endl;

    _pgrid.removeNode(b-1);
    _pgrid.removeFace(invalidFaceNrs[0]-1);
    _pgrid.removeFace(invalidFaceNrs[1]-1);
    _pgrid.removeEdge(invalidEdgeNrs[0]-1);
    _pgrid.removeEdge(invalidEdgeNrs[1]-1);
    _pgrid.removeEdge(invalidEdgeNrs[2]-1);
    // std::cout << ":::: " << _pgrid.noEdges() << " edges after removal." << std::endl;
    // std::cout << ":::: " << _pgrid.noElems() << " faces after removal." << std::endl;

    // checkGrid("After face removal");

    delete[] facesNrs;
    delete[] edgesNrs;

    if (b <= _pgrid.noNodes()) {
        int c = _pgrid.nodeNr(b-1);
        _pgrid.reconnectNode(c, b);
        _pgrid.nodeNr(b-1) = b;
    }

    for (int i=0; i<2; i++) {
        int c = invalidFaceNrs[i];
        if (c <= _pgrid.noElems()) {
            // int d = _pgrid.elemNr(c-1);
            _pgrid.elemNr(c-1) = c;
        }
    }

    for (int i=0; i<3; i++) {
        int c = invalidEdgeNrs[i];
        if (c <= _pgrid.noEdges()) {
            int d = _pgrid.edgeNr(c-1);
            if(_removeEdgeFromList(c-1) == false)
                err_edge_to_remove_not_found(c-1);
            if(_updateEdgeInList(d-1, c-1) == false)
                err_edge_to_update_not_found(d-1);
            _pgrid.edgeNr(c-1) = c;
        } else {
            if(_removeEdgeFromList(c-1) == false)
                err_edge_to_remove_not_found(c-1);
        }
    }
    // std::cout << ":::: " << _edges.size() << " rated edges on list after contraction. " << std::endl;
    return true;
}

bool GridFEProcessor::_updateEdgeInList (int fromIdx, int toIdx) {
    // std::cout << "Replacing edge " << (fromIdx+1) << " with " << (toIdx+1) << std::endl;
    std::list<Edge>::iterator it;
    for (it=_edges.begin(); it != _edges.end(); it++) {
        int& idx = (*it).idx;

        if (idx == fromIdx) {
            idx = toIdx;
            return true;
        }
    }
    return false;
}

bool GridFEProcessor::_removeEdgeFromList(int removeIdx) {
    // std::cout << "Removing edge " << (removeIdx+1) << std::endl;
    std::list<Edge>::iterator it;
    for (it=_edges.begin(); it != _edges.end(); it++) {
        int idx = (*it).idx;

        if (idx == removeIdx) {
            it = _edges.erase(it);
            return true;
        }
    }
    return false;
}

bool GridFEProcessor::_reevaluate(int a) {
    int noEdges=0;
    int* adjancentEdgesNrs=NULL;

    _pgrid.adjancentEdges(&noEdges, &adjancentEdgesNrs, a, -1);

    for (int i=0; i<noEdges; i++) {
        int edgeNr = adjancentEdgesNrs[i];
        int edgeIdx = edgeNr-1;//NR2IDX

        // std::cout << "::Reevaluating edge " << edgeNr << std::endl;

        Edge edge(edgeIdx);
        std::list<Edge>::iterator found =
                std::find(_edges.begin(), _edges.end(), edge);

        if (found != _edges.end()) {
            _evaluate((*found));
        }
    }
    return true;
}

bool GridFEProcessor::_findVector(int a, int b, Vector &vector) {
    Vector coords[2];
    int i, j;
    i = a - 1;//NR2IDX
    j = b - 1;//NR2IDX
    _pgrid.getCoords(i, coords[0].coords);
    _pgrid.getCoords(j, coords[1].coords);

    sub(coords[0], coords[1], &vector);
    vector.x /= 2.0;
    vector.y /= 2.0;
    vector.z /= 2.0;
    return true;
}

bool GridFEProcessor::_findNewPosition(int a, const Vector &vector, Vector &position) {
    Vector v;
    int i = a-1;//NR2IDX
    _pgrid.getCoords(i, v.coords);
    sub(v, vector, &position);
    return true;
}

bool GridFEProcessor::_findCenter(int *nodeNrs, int noNodes, Vector &center) {
    center.x = 0.0;
    center.y = 0.0;
    center.z = 0.0;
    for (int i=0; i<noNodes; i++) {
        Vector v;
        int nodeNr = nodeNrs[i];
        int nodeIdx = nodeNr-1;//NR2IDX
        _pgrid.getCoords(nodeIdx, v.coords);
        center.x += v.x;
        center.y += v.y;
        center.z += v.z;
    }
    center.x /= (real) noNodes;
    center.y /= (real) noNodes;
    center.z /= (real) noNodes;

    return true;
}

bool GridFEProcessor::_findCommonNodes(int q, int r, IEdges &edges,
                                       int commonNodesNr[]) {
    std::list<int> left(0);
    std::list<int> right(0);

    for (int i=0; i<edges.size(); i++) {
        int a = edges.nodeNr(i, 0);
        int b = edges.nodeNr(i, 1);

        if (a == q && b != r)
            left.push_back(b);
        if (b == q && a != r)
            left.push_back(a);
        if (a != q && b == r)
            right.push_back(a);
        if (b != q && a == r)
            right.push_back(b);
    }

    left.sort();
    right.sort();

    int noCommonNodes = 0;
    while(left.size()>0 && right.size()>0 ) {
        int a = left.front();
        int b = right.front();
        if(a < b)
          left.pop_front();
        else if(a > b)
          right.pop_front();
        else { // a == b
            noCommonNodes++;
            if (noCommonNodes <= 2) {
                // std::cout << "Found common node: " << a << std::endl;
                commonNodesNr[noCommonNodes-1] = a;
            } else {
                // std::cout << "Found EXTRA common node: " << a << std::endl;
                return false;
            }
            left.pop_front();
            right.pop_front();
        }
    }

    return (noCommonNodes==2);
}

bool GridFEProcessor::_findDirectionNode(int q, int r, int s, IFaces &faces,
                                         int &directionNodeNr) {
    directionNodeNr = -1; // in case there is no direction node
    // std::cout << "Looking for face with edge " << r << ", " << s << std::endl;
    for (int i=0; i<faces.size(); i++) {
        int noNodesMatches=0;
        int o = -1;
        int a = faces.nodeNr(i, 0);
        int b = faces.nodeNr(i, 1);
        int c = faces.nodeNr(i, 2);

        if (a == r || a == s) noNodesMatches++;
        else o = a;

        if (b == r || b == s) noNodesMatches++;
        else o = b;

        if (c == r || c == s) noNodesMatches++;
        else o = c;

        if (noNodesMatches == 2 && o != q) {
            directionNodeNr = o;
        }
    }
    return true;
}

bool GridFEProcessor::_findInvalidEdges(IEdges &edges, int* invalidEdgesNrs) {
    int noInvalidEdges= 0;
    for (int i=0; i<edges.size(); i++) {
        int a = edges.nodeNr(i, 0);
        int b = edges.nodeNr(i, 1);

        if(a == b) {
            if (noInvalidEdges < 3) {
                // std::cout << "Found " << noInvalidEdges << " collapsed edge between " << a << " and " << b << " (" << edges.nr(i) << ")" << std::endl;
                invalidEdgesNrs[noInvalidEdges++] = edges.nr(i);
            } else {
                // std::cout << "Found EXTRA collapsed edge between " << a << " and " << b << " (" << edges.nr(i) << ")" << std::endl;
                return false;
            }
        } else {
            for (int j=i-1; j>=0; j--) {
                int c = edges.nodeNr(j, 0);
                int d = edges.nodeNr(j, 1);

                if((a == c && b == d) || (a == d && b == c)) {
                    if (noInvalidEdges < 3) {
                        // std::cout << "Found " << noInvalidEdges << " invalid edge between " << a << " and " << b << " (" << edges.nr(j) << ")" << std::endl;
                        invalidEdgesNrs[noInvalidEdges++] = edges.nr(j);
                    } else {
                        // std::cout << "Found EXTRA invalid edge between " << a << " and " << b << " (" << edges.nr(j) << ")" << std::endl;
                        return false;
                    }
                }
            }
        }
    }
    return (noInvalidEdges==3);
}

bool GridFEProcessor::_findInvalidFaces(IFaces &faces, int* invalidFacesNrs) {
    int noInvalidFaces = 0;
    for (int i=0; i<faces.size(); i++) {
        int a = faces.nodeNr(i, 0);
        int b = faces.nodeNr(i, 1);
        int c = faces.nodeNr(i, 2);

        if(a == b || a == c || b == c) {
            noInvalidFaces++;
            if (noInvalidFaces <= 2) {
                // std::cout << "Found invalid face: " << faces.nr(i) << std::endl;
                invalidFacesNrs[noInvalidFaces-1] = faces.nr(i);
            } else {
                // std::cout << "Found EXTRA invalid face: " << faces.nr(i) << std::endl;
                return false;
            }
        }
    }
    return (noInvalidFaces==2);
}

int GridFEProcessor::_findFaceConfiguration(int a, int b, int c, IFaces &faces) {
    int noInvalidFaces = 0;
    for (int i=0; i<faces.size(); i++) {
        int p = faces.nodeNr(i, 0);
        int q = faces.nodeNr(i, 1);
        int r = faces.nodeNr(i, 2);

        if (p == a && q == b && r == c) return 1; else
	if (p == b && q == c && r == a) return 2; else
        if (p == c && q == a && r == b) return 3; else
        if (p == a && q == c && r == b) return 4; else
        if (p == c && q == b && r == a) return 5; else
        if (p == b && q == a && r == c) return 6;
    }
    return 0; // not found
}

bool GridFEProcessor::checkGrid(const std::string &msg) {
    // std::cout << std::endl;
    // std::cout << "Checking mesh consistency: " << msg << ": ";

    int correct = true;
    for (int i=0; i<_pgrid.noElems(); i++) {
        int a = _pgrid.elemNodeNr(i, 0);
        int b = _pgrid.elemNodeNr(i, 1);
        int c = _pgrid.elemNodeNr(i, 2);

        if(a == b || a == c || b == c) {
            if (correct) {
                correct = false;
                // std::cout << "[INVALID]" << std::endl;
            }
            // std::cout << "Face ";
            // std::cout << _pgrid.elemNodeNr(i, 0) << ", ";
            // std::cout << _pgrid.elemNodeNr(i, 1) << ", ";
            // std::cout << _pgrid.elemNodeNr(i, 2) << " ";
            // std::cout << " is invalid." << std::endl;
            correct = false;
        }
    }
    if (correct) {
        // std::cout << "[OK]" << std::endl;
        // std::cout << std::endl;
    }
    return correct;
}
