#ifndef PROGRESSIVEGRIDFE_H
#define PROGRESSIVEGRIDFE_H

#include <GridFE.h>
#include <vector>
#include <algorithm>

using namespace memorph;

inline bool contains(std::vector<int> &v, int o) {
    if (std::find(v.begin(), v.end(), o) != v.end())
        return true;
    return false;
}

inline void swap(real &a, real &b) {
    real t = a;
    a = b;
    b = t;
}

inline void swap(int &a, int &b) {
    int t = a;
    a = b;
    b = t;
}

inline void swap(bool &a, bool &b) {
    bool t = a;
    a = b;
    b = t;
}

inline void sort3(int *vector) {
    int& a = vector[0];
    int& b = vector[1];
    int& c = vector[2];
    // sort array with 3-elements
    if (a < b) swap(a,b);
    if (b < c) swap(b,c);
    if (a < b) swap(a,b);
}
inline void sort2(int *vector) {
    int& a = vector[0];
    int& b = vector[1];
    // sort array with 2-elements
    if (a < b) swap(a, b);
}

class ProgressiveGridFE : public GridFE {
public:
    // [0] - numer krawędzi, [1]-[2] - numery węzłów krawędzi
    static const size_t edgeMemSize = sizeof (int) * 5;
    static const size_t splitIdxMemSize = sizeof (int) * 6;
    static const size_t splitVecMemSize = sizeof (real) * 3;

protected:
    int _maxNodes;
    int _maxElems;
    int _maxSplits;
    int _noEdges;
    int **_edges;

    int _noSplits;
    int **_splitsIdx;
    real **_splitsVec;

public:
    ProgressiveGridFE();
    void clone(GridFE &grid);
    virtual ~ProgressiveGridFE();

    void adjancentFaces(int* no, int** nrs, int a, int b=-1);
    void adjancentEdges(int* no, int** nrs, int a, int b=-1);
    void adjancentNodes(int* no, int** nrs, int a, int b=-1);

    int addSplit();
    int addNode();
    int addFace();
    void removeNode(int nodeIdx);
    void removeFace(int faceIdx);
    void removeEdge(int edgeIdx);
    void swapNodes(int fromIdx, int toIdx);
    void swapFaces(int fromIdx, int toIdx);
    void swapEdges(int fromIdx, int toIdx);
    void swapSplits(int fromIdx, int toIdx);
    void reconnectNode(int fromNr, int toNr);

public:
    int noEdges() const { return _noEdges; }
    int &edgeNr(int edgeIdx) {
        assertArrIndex(edgeIdx, _noEdges);
        return _edges[edgeIdx][0];
    }
    int &edgeNodeNr(int edgeIdx, int nodeIdx) {
        assertArrIndex(edgeIdx, _noEdges);
        assertArrIndex(nodeIdx, 2);
        return _edges[edgeIdx][nodeIdx + 1];
    }
    int noSplits() const { return _noSplits; }
    int &splitNr(int splitIdx) {
        assertArrIndex(splitIdx, _noSplits);
        return _splitsIdx[splitIdx][0];
    }
    int &splitNodeNr(int splitIdx, int nodeIdx) {
        assertArrIndex(splitIdx, _noSplits);
        assertArrIndex(nodeIdx, 2);
        return _splitsIdx[splitIdx][nodeIdx + 1];
    }
    int &splitCommonNodeNr(int splitIdx, int nodeIdx) {
        assertArrIndex(splitIdx, _noSplits);
        assertArrIndex(nodeIdx, 2);
        return _splitsIdx[splitIdx][nodeIdx + 3];
    }
    int &splitDirectionNodeNr(int splitIdx) {
        assertArrIndex(splitIdx, _noSplits);
        return _splitsIdx[splitIdx][5];
    }
    int &splitFaceConfiguration(int splitIdx, int faceIdx) {
        assertArrIndex(splitIdx, _noSplits);
        assertArrIndex(faceIdx, 2);
        return _splitsIdx[splitIdx][faceIdx + 6];
    }
    real &splitVector(int splitIdx, int coordIdx) {
        assertArrIndex(splitIdx, _noSplits);
        assertArrIndex(coordIdx, 3);
        return _splitsVec[splitIdx][coordIdx];
    }
    int noMaxNodes() { return _maxNodes; }
    int noMaxElems() { return _maxElems; }
    int noMaxSplits() { return _maxSplits; }
public:
    void reinitNodes(int maxNodes);
    void reinitElems(int maxElems);
    void initSplits(int maxSplits=-1);
    void freeSplits();
    bool initEdges();
    void freeEdges();

};

#endif
