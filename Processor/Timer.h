#ifndef TIME_H
#define TIME_H

#include <sys/time.h>
#include <unistd.h>
#include <iostream>

class Timer {
private:
    timeval _startTime;
    timeval _stopTime;
    long _id;
    static long _counter;

public:
    Timer(bool count=false) {
        if (count) {
            _id = _counter++;
        } else {
            _id = -1;
        }
    }

    Timer& start(){
        gettimeofday(&_startTime, NULL);
        return *this;
    }

    Timer& stop(){
        gettimeofday(&_stopTime, NULL);
        return *this;
    }

    void print(const std::string &msg, bool raw=false) {
        long seconds, useconds, duration;
        seconds  = _stopTime.tv_sec  - _startTime.tv_sec;
        useconds = _stopTime.tv_usec - _startTime.tv_usec;

        duration = ((seconds) * 1000 + useconds/1000.0) + 0.5;

        if (raw) {
            std::cerr << _id << ";" << duration << std::endl;
        } else {
            if (_id >= 0) std::cerr << _id << ". ";
            std::cerr << msg << " took " << duration << "ms" << std::endl;
        }
    }

    static void resetCounter() {
        _counter = 0;
    }
};

#endif // TIME_H
