#include <ProgressiveGridFEProcessor.h>
#include <Errors.h>
#include <Timer.h>

ProgressiveGridFEProcessor::ProgressiveGridFEProcessor(QObject *parent):
    QObject(parent) {
    _pgrid = NULL;
}

void ProgressiveGridFEProcessor::process(ProgressiveGridFE *pgrid) {
    _pgrid = pgrid;
}

void ProgressiveGridFEProcessor::split(int nr, int a, int b, int c0, int c1, int d, int cfg0, int cfg1, real dx, real dy, real dz) {
    Timer tSplit(true);
    tSplit.start();

    //print_split(nr, a, b, c0, c1, d, dx, dy, dz);

    Vector position;
    Vector newPosition;
    Vector vector;
    vector.x = dx;
    vector.y = dy;
    vector.z = dz;

    int *facesNrs = NULL;
    int noFaces = 0;

    int n0idx = 0;
    int f0idx = 0;
    int f1idx = 0;

    n0idx = _pgrid->addNode();
    f0idx = _pgrid->addFace();
    f1idx = _pgrid->addFace();

    _pgrid->reconnectNode(b, n0idx+1);
    _pgrid->swapNodes(b-1, n0idx);
    _pgrid->nodeNr(b-1) = b;
    _pgrid->nodeNr(n0idx) = n0idx+1;

    // nodeNr is nr of updated node, we no longer use removed node, because it's invalid
    _pgrid->adjancentFaces(&noFaces, &facesNrs, a, -1);

    int tries = noFaces;
    int mi = c0;
    int ni = d;
    int nj = -1;
    while (tries > 0) {
        for (int i=0; i<noFaces; i++) {
            int index = 0;
            int njCandidate = -1; // nj
            int matchingNodes = 0;
            for (int j=0; j<3; j++) {
                int r = _pgrid->elemNodeNr(facesNrs[i]-1, j);
                if (r == ni) {
                    matchingNodes++;
                } else if (r == a) {
                    index = j;
                    matchingNodes++;
                } else {
                    njCandidate = r;
                }
            }

            if (matchingNodes == 2 && njCandidate == mi) {
                _pgrid->elemNodeNr(facesNrs[i]-1, index) = b;
            } else if (matchingNodes == 2) {
                nj = njCandidate;
            }
        }

        tries--;

        if (ni == c1) {
            tries = 0;
        } else if (nj != -1) {
            mi = ni;
            ni = nj;
        } else {
            tries = 0;
            err_couldnt_reconnect_faces();
        }
    }

    delete[] facesNrs;
    // get a-node position
    _pgrid->getCoords(a-1, position.coords);
    // update a-node position
    add(position, vector, &newPosition);
    _pgrid->setCoords(a-1, newPosition.coords);
    // update b-node position
    sub(position, vector, &newPosition);
    _pgrid->setCoords(b-1, newPosition.coords);

    // adding two new/missing faces
    
    switch (cfg0) {
    	case 1:
	_pgrid->elemNodeNr(f0idx, 0) = a;
	_pgrid->elemNodeNr(f0idx, 1) = b;
	_pgrid->elemNodeNr(f0idx, 2) = c0;
	break;
    	case 2:
	_pgrid->elemNodeNr(f0idx, 0) = b;
	_pgrid->elemNodeNr(f0idx, 1) = c0;
	_pgrid->elemNodeNr(f0idx, 2) = a;
	break;
    	case 3:
	_pgrid->elemNodeNr(f0idx, 0) = c0;
	_pgrid->elemNodeNr(f0idx, 1) = a;
	_pgrid->elemNodeNr(f0idx, 2) = b;
	break;
    	case 4:
	_pgrid->elemNodeNr(f0idx, 0) = a;
	_pgrid->elemNodeNr(f0idx, 1) = c0;
	_pgrid->elemNodeNr(f0idx, 2) = b;
	break;
    	case 5:
	_pgrid->elemNodeNr(f0idx, 0) = c0;
	_pgrid->elemNodeNr(f0idx, 1) = b;
	_pgrid->elemNodeNr(f0idx, 2) = a;
	break;
    	case 6:
	_pgrid->elemNodeNr(f0idx, 0) = b;
	_pgrid->elemNodeNr(f0idx, 1) = a;
	_pgrid->elemNodeNr(f0idx, 2) = c0;
	break;
	default:
	err_invalid_face_configuration();
    }
    
    switch (cfg1) {
    	case 1:
	_pgrid->elemNodeNr(f1idx, 0) = a;
	_pgrid->elemNodeNr(f1idx, 1) = b;
	_pgrid->elemNodeNr(f1idx, 2) = c1;
	break;
    	case 2:
	_pgrid->elemNodeNr(f1idx, 0) = b;
	_pgrid->elemNodeNr(f1idx, 1) = c1;
	_pgrid->elemNodeNr(f1idx, 2) = a;
	break;
    	case 3:
	_pgrid->elemNodeNr(f1idx, 0) = c1;
	_pgrid->elemNodeNr(f1idx, 1) = a;
	_pgrid->elemNodeNr(f1idx, 2) = b;
	break;
    	case 4:
	_pgrid->elemNodeNr(f1idx, 0) = a;
	_pgrid->elemNodeNr(f1idx, 1) = c1;
	_pgrid->elemNodeNr(f1idx, 2) = b;
	break;
    	case 5:
	_pgrid->elemNodeNr(f1idx, 0) = c1;
	_pgrid->elemNodeNr(f1idx, 1) = b;
	_pgrid->elemNodeNr(f1idx, 2) = a;
	break;
    	case 6:
	_pgrid->elemNodeNr(f1idx, 0) = b;
	_pgrid->elemNodeNr(f1idx, 1) = a;
	_pgrid->elemNodeNr(f1idx, 2) = c1;
	break;
	default:
	err_invalid_face_configuration();
    }

    // tSplit.stop().print("NodeSplit", true);
    emit splitted();
}
