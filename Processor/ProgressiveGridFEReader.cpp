#include "ProgressiveGridFEReader.h"

ProgressiveGridFEReader::ProgressiveGridFEReader():
    GridFEReader() {
}

ProgressiveGridFEReader::ProgressiveGridFEReader(const std::string &path):
    GridFEReader(path) {
}

ProgressiveGridFEReader::~ProgressiveGridFEReader() { }

bool ProgressiveGridFEReader::read(ProgressiveGridFE *pgrid) {
    if (!GridFEReader::_readInfo(pgrid)) {
        return false;
    }
    if (!ProgressiveGridFEReader::_readInfo(pgrid)) {
        return false;
    }
    if (!GridFEReader::_readBndInds(pgrid)) {
        return false;
    }
    if (!GridFEReader::_findHash()) {
        return false;
    }
    if (!GridFEReader::_readNodes(pgrid)) {
        return false;
    }
    if (!GridFEReader::_findHash()) {
        return false;
    }
    if (!GridFEReader::_readElems(pgrid)) {
        return false;
    }
    if (!GridFEReader::_findHash()) {
        return false;
    }
    if (!ProgressiveGridFEReader::_readSplits(pgrid)) {
        return false;
    }
    return true;
}

bool ProgressiveGridFEReader::_readInfo(ProgressiveGridFE *pgrid) {
    char line[maxLineLen];
    char *p;
    unsigned char flags = 0x00;

    while (true) {
        _input.getline(line, maxLineLen);
        if (!_input.good()) {
            std::cerr << "Nie udało się odczytać nagłówka";
            return false;
        } else if ((p = strrchr(line, '=')) != NULL && strstr(line, "Number of splits") != NULL) {
            int noSplits = atoi(++p);
            pgrid->initSplits(noSplits);
            pgrid->reinitNodes(noSplits + pgrid->noNodes());
            pgrid->reinitElems(2*noSplits + pgrid->noElems());
            flags |= 0x01;
        }
        if (flags == 0x01) {
            break;
        }
    }
    return true;
}

bool ProgressiveGridFEReader::_readSplits(ProgressiveGridFE *pgrid) {
    int nr, a, b, c0, c1, d, cfg0, cfg1;
    real dx, dy, dz;
    char line[maxLineLen];
    for (int i = 0; i < pgrid->noMaxSplits(); ++i) {
        _input.getline(line, maxLineLen);
        if (!_input.good()) {
            std::cerr << "Nie udało się odczytać listy operacji podziału węzłów - nieoczekiwany koniec pliku lub inny błąd" << std::endl;
            return false;
        }
        if (sscanf(line, " %d NodeSplit %d %d %d %d %d %d %d %g %g %g", &nr, &a, &b, &c0, &c1, &d, &cfg0, &cfg1, &dx, &dy, &dz) == 11) {
            int idx = pgrid->addSplit();
            pgrid->splitNr(idx) = nr;
            pgrid->splitNodeNr(idx, 0) = a;
            pgrid->splitNodeNr(idx, 1) = b;
            pgrid->splitCommonNodeNr(idx, 0) = c0;
            pgrid->splitCommonNodeNr(idx, 1) = c1;
            pgrid->splitDirectionNodeNr(idx) = d;
            pgrid->splitFaceConfiguration(idx, 0) = cfg0;
            pgrid->splitFaceConfiguration(idx, 1) = cfg1;
            pgrid->splitVector(idx, 0) = dx;
            pgrid->splitVector(idx, 1) = dy;
            pgrid->splitVector(idx, 2) = dz;
        } else {
            std::cerr
                    << "Nie udało się odczytać listy operacji podziału węzłów :-(" << "\n"
                    << "Oto feralna linijka:" << "\n"
                    << line << std::endl;
            return false;
        }
    }

    std::cout << "Odczytano " << pgrid->noSplits() << " operacji podziału węzłów." << std::endl;
    return true;
}
