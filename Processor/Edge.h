#ifndef EDGE_H
#define EDGE_H

#include <iostream>

class Edge {
public:
    int idx;
    float volumeChange;
    float minAngle;
    bool flipsFaces;
    bool badCommonNodesNo;
    bool tooGreatVolumeChange;
    bool tooSmallMinAngle;

    Edge(int idx):
        idx(idx),
        volumeChange(0.0),
        minAngle(0.0),
        flipsFaces(false),
        badCommonNodesNo(false),
        tooGreatVolumeChange(false),
        tooSmallMinAngle(false) {}

    bool invalid() const {
        return flipsFaces || badCommonNodesNo || tooGreatVolumeChange || tooSmallMinAngle;
    }

    bool valid() const {
        return !invalid();
    }

    bool operator==(const Edge& other) {
        return other.idx == idx;
    }

    bool operator<(const Edge& other) {
        if (other.valid() && this->invalid())
            return false;
        if (other.invalid() && this->valid())
            return true;

        if(other.volumeChange == this->volumeChange)
            return other.idx > this->idx;
        else
            return other.volumeChange > this->volumeChange; // sort edges ascending
    }
};

#endif // EDGE_H
