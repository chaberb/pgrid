#include <getopt.h>
#include <GridFE.h>
#include <GridFEReader.h>
#include <GridFEProcessor.h>
#include <ProgressiveGridFEWriter.h>
#include <Timer.h>

using namespace memorph;

long Timer::_counter = 1;

int main(int argc, char** argv) {
    GridFE grid;
    GridFEProcessor proc;

    std::string in  = "tetra.grid";
    std::string out = "tetra.pgrid";

    int c;

    while (true) {
        static struct option long_options[] = {
            {"max-total-volume",  required_argument, 0, 'V'},
            {"min-nodes",  required_argument, 0, 'N'},
            {"min-faces",  required_argument, 0, 'F'},
            {"max-volume",  required_argument, 0, 'v'},
            {"min-angle",  required_argument, 0, 'a'},
            {"input",    required_argument, 0, 'f'},
            {"output",    required_argument, 0, 'o'},
            {0, 0, 0, 0}
        };
        int option_index = 0;
        c = getopt_long (argc, argv, "V:N:F:v:a:f:o:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c) {
        case 0: break;
        case 'V': proc.maxTotalVolume(atof(optarg)); break;
        case 'N': proc.minNodes(atoi(optarg)); break;
        case 'F': proc.minFaces(atoi(optarg)); break;
        case 'v': proc.maxVolume(atof(optarg)); break;
        case 'a': proc.minAngle(atof(optarg)); break;
        case 'f': in = std::string(optarg); break;
        case 'o': out = std::string(optarg); break;
        case '?': break;

        default:
            abort ();
        }
    }

    GridFEReader reader(in);
    ProgressiveGridFEWriter writer(out);

    reader.read(&grid);
    proc.process(grid);
    writer.write(proc.pgrid());

    return 0;
}

