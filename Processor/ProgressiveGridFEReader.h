#ifndef PROGRESSIVEGRIDFEREADER_H
#define PROGRESSIVEGRIDFEREADER_H

#include <Reader.h>
#include <GridFEReader.h>
#include <ProgressiveGridFE.h>

using namespace memorph;

class ProgressiveGridFEReader : public GridFEReader {
private:
    bool _readInfo(ProgressiveGridFE *);
    bool _readSplits(ProgressiveGridFE *);

public:
    ProgressiveGridFEReader();
    ProgressiveGridFEReader(const std::string &);
    virtual ~ProgressiveGridFEReader();
    bool read(ProgressiveGridFE *);
};

#endif
