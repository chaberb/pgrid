#ifndef PROGRESSIVEGRIDFEWRITER_H
#define PROGRESSIVEGRIDFEWRITER_H

#include <Writer.h>
#include <GridFE.h>
#include <GridFEWriter.h>
#include <ProgressiveGridFE.h>

using namespace memorph;

class ProgressiveGridFEWriter : public GridFEWriter {
private:
        bool _writeSplits(ProgressiveGridFE &);
        bool _writeInfo(ProgressiveGridFE &);
public:
        ProgressiveGridFEWriter();
        ProgressiveGridFEWriter(const std::string &path);
        virtual ~ProgressiveGridFEWriter();

        bool write(ProgressiveGridFE &);
};

#endif
