include(../Grid/Grid.pri)
!contains(SOURCES, GridFEProcessor.cpp) { 
    message(Project Processor included)
    
    INCLUDEPATH += $$PWD
    DEPENDPATH += $$PWD
    
    HEADERS += Edge.h
    HEADERS += Errors.h
    HEADERS += Timer.h
    HEADERS += GridFEProcessor.h
    HEADERS += ProgressiveGridFEProcessor.h
    HEADERS += ProgressiveGridFE.h
    HEADERS += ProgressiveGridFEWriter.h
    HEADERS += ProgressiveGridFEReader.h

    SOURCES += GridFEProcessor.cpp
    SOURCES += ProgressiveGridFEProcessor.cpp
    SOURCES += ProgressiveGridFE.cpp
    SOURCES += ProgressiveGridFEWriter.cpp
    SOURCES += ProgressiveGridFEReader.cpp
}
