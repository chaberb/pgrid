#ifndef GRIDFEPROCESSOR_H
#define GRIDFEPROCESSOR_H

#include <list>
#include <Edge.h>
#include <GridFE.h>
#include <ProgressiveGridFE.h>

namespace progrid {
    inline bool isnan(real var) {
        volatile real d = var;
        return d != d;
    }
}

class IFaces {
public:
    virtual int &nodeNr(int faceIdx, int nodeIdx) = 0;
    virtual int nr(int faceIdx) = 0;
    virtual int size() = 0;
};

class IEdges {
public:
    virtual int &nodeNr(int edgeIdx, int nodeIdx) = 0;
    virtual int nr(int edgeIdx) = 0;
    virtual int size() = 0;
};

class GridFEProcessor {
private:
    int _minNodesNo;
    int _minFacesNo;
    real _maxVolume;
    real _maxTotalVolume;
    real _minAngle;
    real _totalVolume;

public :
    GridFEProcessor();
    virtual ~GridFEProcessor() {}

    void process(GridFE &grid);
    bool evaluateEdges();
    bool contractNext();
    ProgressiveGridFE &pgrid() { return _pgrid; }
    void minNodes(int nodes) { _minNodesNo = nodes; }
    void minFaces(int faces) { _minFacesNo = faces; }
    void maxTotalVolume(float volume) {_maxTotalVolume = volume; }
    void minAngle(float angle) {_minAngle = angle; }
    void maxVolume(float volume) {_maxVolume = volume; }

protected:
    bool checkGrid(const std::string &msg);

private:
    bool _evaluate(Edge &edge);
    bool _evaluateGeometry(int noFaces, Vector*** facesCoords,
                           const Vector &center,
                           Edge &edge);
    bool _evaluateTopology(int a, int b,
                           int noFaces, int*** faceNodes,
                           int noEdges, int*** edgeNodes,
                           Edge &edge);
    bool _findDirectionNode(int a, int b, int c,
                            IFaces &faces,
                            int &directionNodeNr);
    bool _findInvalidFaces(IFaces &faces,
                           int invalidFacesNrs[2]);
    bool _findInvalidEdges(IEdges &edges,
                           int invalidEdgesNrs[3]);
    int _findFaceConfiguration(int a, int b, int c,
			    IFaces &faces);
    bool _findCommonNodes(int a, int b, IEdges &edges,
                          int commonNodesIdx[2]);
    bool _findVector(int a, int b, Vector &vector);
    bool _findCenter(int *nodeNrs, int noNodes, Vector &center);
    bool _findNewPosition(int a, const Vector &vector, Vector &position);
    bool _replaceEdgeWithNode(int fromNr, int toNr);
    bool _reevaluate(int nodeNr);
    bool _updateEdgeInList(int fromIdx, int toIdx);
    bool _removeEdgeFromList(int removeIdx);

private:
    ProgressiveGridFE _pgrid;
    std::list<Edge> _edges;
};


class TableFaces : public IFaces {
private:
    int** _faces;
    int _size;
public:
    TableFaces(int** faces, int size) : _faces(faces), _size(size) {}
    int &nodeNr(int faceIdx, int nodeIdx) {
        return _faces[faceIdx][nodeIdx];
    }

    int nr(int faceIdx) {
        return faceIdx+1;//IDX2NR
    }

    int size() {
        return _size;
    }
};

class GridFEFaces : public IFaces {
private:
    GridFE& _grid;
    int* _faceNrs;
    int _noFaces;
public:
    GridFEFaces(GridFE &grid, int* faceNrs, int noFaces) :
        _grid(grid), _faceNrs(faceNrs), _noFaces(noFaces) {}
    int &nodeNr(int faceIdx, int nodeIdx) {
        int idx = _faceNrs[faceIdx]-1;
        return _grid.elemNodeNr(idx, nodeIdx);
    }

    int nr(int faceIdx) {
        return _faceNrs[faceIdx];
    }

    int size() {
        return _noFaces;
    }
};

class TableEdges : public IEdges {
private:
    int** _edges;
    int _size;
public:
    TableEdges(int** edges, int size) : _edges(edges), _size(size) {}
    int &nodeNr(int edgeIdx, int nodeIdx) {
        return _edges[edgeIdx][nodeIdx];
    }

    int nr(int edgeIdx) {
        return edgeIdx+1;//IDX2NR
    }

    int size() {
        return _size;
    }
};

class ProgressiveGridFEEdges : public IEdges {
private:
    ProgressiveGridFE& _pgrid;
    int* _edgeNrs;
    int _noEdges;
public:
    ProgressiveGridFEEdges(ProgressiveGridFE& pgrid, int* edgeNrs, int noEdges) :
        _pgrid(pgrid), _edgeNrs(edgeNrs), _noEdges(noEdges) {}

    int &nodeNr(int edgeIdx, int nodeIdx) {
        int idx = _edgeNrs[edgeIdx]-1;
        return _pgrid.edgeNodeNr(idx, nodeIdx);
    }

    int nr(int edgeIdx) {
        return _edgeNrs[edgeIdx];
    }

    int size() {
        return _noEdges;
    }
};

#endif
