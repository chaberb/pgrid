#include <vector>
#include <algorithm>
#include <ProgressiveGridFE.h>
#include <Errors.h>

ProgressiveGridFE::~ProgressiveGridFE() {
    freeSplits();
    freeEdges();
    free();
};

int _maxNodes;
int _maxElems;
int _maxSplits;
int _noEdges;
int **_edges;

int _noSplits;
int **_splitsIdx;
real **_splitsVec;

ProgressiveGridFE::ProgressiveGridFE() :
    GridFE(),
    _maxNodes(0),
    _maxElems(0),
    _maxSplits(0),
    _noEdges(0),
    _edges(NULL),
    _noSplits(0),
    _splitsIdx(NULL),
    _splitsVec(NULL) {
}

void ProgressiveGridFE::clone(GridFE &grid) {
    init(grid.noNodes(), grid.noElems(), grid.noBnds());

    for (int i=0; i<grid.noBnds(); i++) {
        bndNr(i) = grid.bndNr(i);
        bndName(i, grid.bndName(i));
    }

    for (int i=0; i<grid.noNodes(); i++) {
        initNodeBndInds(i, grid.noNodeBndInds(i));
        for (int j=0; j<3; j++) {
            coord(i, j) = grid.coord(i, j);
        }
        for (int j=0; j<grid.noNodeBndInds(i); j++) {
            nodeBndInd(i, j) = grid.nodeBndInd(i, j);
        }
        nodeNr(i) = grid.nodeNr(i);
    }

    for (int i=0; i<grid.noElems(); i++) {
        initElemNodes(i, grid.elemType(i));
        for (int j=0; j<3; j++) {
            elemNodeNr(i, j) = grid.elemNodeNr(i, j);
        }
        elemNr(i) = grid.elemNr(i);
        elemSubdNr(i) = grid.elemSubdNr(i);
    }
}

void ProgressiveGridFE::freeSplits() {
    for (int i = 0; i < _noSplits; i++) {
        delete[] _splitsVec[i];
        delete[] _splitsIdx[i];
    }

    delete[] _splitsVec;
    delete[] _splitsIdx;
}

void ProgressiveGridFE::freeEdges() {
    for (int i = 0; i< _noEdges; i++) {
        delete[] _edges[i];
    }

    delete _edges;

    _edges = NULL;
}

void ProgressiveGridFE::adjancentEdges(int *noEdges, int **adjancentEdgesNrs,
                                       int a, int b) {
    std::vector<int> edges(0);
    std::vector<int> nodes(0);

    for(int edgeIdx=0; edgeIdx<_noEdges; edgeIdx++) {
        int p = edgeNodeNr(edgeIdx, 0);
        int q = edgeNodeNr(edgeIdx, 1);

        if(p == a || p == b ||
           q == a || q == b) {

           edges.push_back(edgeNr(edgeIdx));

           if (p != a && p != b && !contains(nodes, p))
               nodes.push_back(p);
           if (q != a && q != b && !contains(nodes, q))
               nodes.push_back(q);
        }
    }

    for(int edgeIdx=0; edgeIdx<_noEdges; edgeIdx++) {
        int p = edgeNodeNr(edgeIdx, 0);
        int q = edgeNodeNr(edgeIdx, 1);

        if(contains(nodes, p) && contains(nodes, q)) {
           edges.push_back(edgeNr(edgeIdx));
        }
    }

    *adjancentEdgesNrs = new int[edges.size()];

    for(unsigned int i=0; i<edges.size(); i++) {
        (*adjancentEdgesNrs)[i] = edges[i];
    }

    *noEdges = edges.size();
}

void ProgressiveGridFE::adjancentFaces(int *noFaces, int **adjancentFacesNrs,
                                       int a, int b) {
    std::vector<int> faces(0);

    for(int elemIdx=0; elemIdx<_noElems; elemIdx++) {
        int p = elemNodeNr(elemIdx, 0);
        int q = elemNodeNr(elemIdx, 1);
        int r = elemNodeNr(elemIdx, 2);

        int commonNodes = 0;

        if(p == a || p == b) commonNodes++;
        if(q == a || q == b) commonNodes++;
        if(r == a || r == b) commonNodes++;

        if(commonNodes > 0) faces.push_back(elemNr(elemIdx));
    }

    *adjancentFacesNrs = new int[faces.size()];

    for(unsigned int i=0; i<faces.size(); i++) {
        (*adjancentFacesNrs)[i] = faces[i];
    }

    *noFaces = faces.size();
}

void ProgressiveGridFE::adjancentNodes(int *noNodes, int **adjancentNodesNrs,
                                       int a, int b) {
    std::vector<int> nodes(0);

    nodes.push_back(a);
    nodes.push_back(b);

    for(int edgeIdx=0; edgeIdx<_noEdges; edgeIdx++) {
        int p = edgeNodeNr(edgeIdx, 0);
        int q = edgeNodeNr(edgeIdx, 1);
        int nodeNr = -1;

        if(p == a || p == b)
            nodeNr = nodeNr < 0? q : -1;
        if(q == a || q == b)
            nodeNr = nodeNr < 0? p : -1;

        std::vector<int>::iterator it = std::find(nodes.begin(), nodes.end(), nodeNr);
        bool found = (it != nodes.end());

        if(nodeNr >= 0 && !found) {
            nodes.push_back(nodeNr);
        }
    }

    *adjancentNodesNrs = new int[nodes.size()];

    for(unsigned int i=0; i<nodes.size(); i++) {
        (*adjancentNodesNrs)[i] = nodes[i];
    }

    *noNodes = nodes.size();
}

void ProgressiveGridFE::reconnectNode(int fromNr, int toNr) {
    int nodesElemsNo = 0;
    // reconnect nodes in elements
    for (int elemIdx=0; elemIdx<noElems(); elemIdx++) {
        for (int nodeIdx=0; nodeIdx<3; nodeIdx++) {
            int& nr = elemNodeNr(elemIdx, nodeIdx);
            if (nr == fromNr) {
                nr = toNr;
                nodesElemsNo++;
            }
        }
    }
    int nodesEdgesNo = 0;
    // reconnect nodes in edges
    for (int edgeIdx=0; edgeIdx<noEdges(); edgeIdx++) {
        for (int nodeIdx=0; nodeIdx<2; nodeIdx++) {
            int& nr = edgeNodeNr(edgeIdx, nodeIdx);
            if (nr == fromNr) {
                nr = toNr;
                nodesEdgesNo++;
            }
        }
    }
//    std::cout << "Reconnected " << nodesElemsNo;
//    std::cout << " nodes in elemens and " << nodesEdgesNo;
//    std::cout << " nodes in edges." << std::endl;
}

bool ProgressiveGridFE::initEdges() {
    if (_noEdges != 0) freeEdges();

    //_noEdges = _noElems*3; // Number of edges is 3 * number of elements (pessimistic)
    _noEdges = _noElems*1.5; // Number of edges is 3 * number of elements / 2

    _edges = new int *[_noEdges];
    for (int i = 0; i < _noEdges; ++i) {
        _edges[i] = new int[2];
    }

    int lastEdgeIdx = 0;
    for(int elemIdx=0; elemIdx<_noElems; elemIdx++) {
        for(int node=0; node<3; node++) {
            int a = elemNodeNr(elemIdx, (node+0)%3);// 0, 1, 2
            int b = elemNodeNr(elemIdx, (node+1)%3);// 1, 2, 0
            int z = elemNodeNr(elemIdx, (node+2)%3);// 2, 0, 1
            int edgeIdx = -1;
	    if (a == b) std::cout << "Element " << elemIdx << " has " << a << "," << b << "," << z << std::endl;
            for(int e=0; e<_noEdges; e++) {
                int c = edgeNodeNr(e, 0);
                int d = edgeNodeNr(e, 1);
                if ((c == a && d == b ) ||
                    (d == a && c == b )) {
                    edgeIdx = e;
		    std::cout << "Edge: " << a << "-" << b << " matches " << c << "-" << d << std::endl;
                }
            }

            if(edgeIdx < 0) { // edge not found
                edgeIdx = lastEdgeIdx;
                edgeNodeNr(edgeIdx, 0) = a;
                edgeNodeNr(edgeIdx, 1) = b ;
                edgeNr(edgeIdx) = edgeIdx+1;
                lastEdgeIdx++;
                if (lastEdgeIdx > _noEdges) {
                    err_too_many_edges(lastEdgeIdx, _noElems);
                    return false;
                }
            } // else edge has already been added
        }
    }

    if (lastEdgeIdx < _noEdges) {
        err_too_few_edges(lastEdgeIdx, _noElems);
        return false;
    }

    //_noEdges = lastEdgeIdx;
    std::cout << "Znaleziono " << _noEdges << " krawędzi." << std::endl;
    return true;
}

void ProgressiveGridFE::initSplits(int maxSplits) {
    if (_noSplits != 0) {
        freeSplits();
    }

    if (maxSplits < 0) {
        maxSplits = _noNodes;
    }

    _splitsIdx = new int *[maxSplits];
    _splitsVec = new real *[maxSplits];

    _noSplits = 0;
    _maxSplits = maxSplits;
}

void ProgressiveGridFE::reinitNodes(int maxNodes) {
    std::cout << "Reinit " << _noNodes << " -> " << maxNodes << " nodes." << std::endl;
    if (_noNodes > 0) {
        for (int i=0; i<_noNodes; i++) {
            delete[] _coords[i];
            delete[] _nodes[i];
        }
        delete[] _coords;
        delete[] _nodes;
    }

    real **coords;
    int **nodes;
    int **nodesBndInds;

    coords = new real *[maxNodes];
    nodes = new int *[maxNodes];
    nodesBndInds = new int *[maxNodes];

    for (int i = 0; i < maxNodes; ++i) {
        coords[i] = new real[_noSpaceDim];
        nodes[i] = new int[noNodeParams];
        nodesBndInds[i] = new int[1];
    }

    _coords = coords;
    _nodes = nodes;
    _nodesBndInds = nodesBndInds;

    _maxNodes = maxNodes;
}

void ProgressiveGridFE::reinitElems(int maxElems) {
    std::cout << "Reinit " << _noElems << " -> " <<  maxElems << " elems." << std::endl;
    if (_noElems > 0) {
        for (int i=0; i<_noElems; i++) {
            delete[] _elems[i];
            delete[] _elemsNodes[i];
        }
        delete[] _elems;
        delete[] _elemsNodes;
    }

    int **elems;
    int **elemsNodes;
    elems = new int *[maxElems];
    elemsNodes = new int *[maxElems];

    for (int i=0; i<maxElems; ++i) {
        elems[i] = new int[noElemParams];
        elemsNodes[i] = new int[3];
    }

    _elems = elems;
    _elemsNodes = elemsNodes;

    _maxElems = maxElems;
}

int ProgressiveGridFE::addNode() {
    int nodeIdx = _noNodes;

    if (_noNodes >= _maxNodes) {
        err_couldnt_add_node(_maxNodes);
        return -1;
    }

    _nodes[nodeIdx][0] = nodeIdx+1;
    _nodes[nodeIdx][1] = 0;
    _coords[nodeIdx][0] = 0.0;
    _coords[nodeIdx][1] = 0.0;
    _coords[nodeIdx][2] = 0.0;

    _noNodes++;

    return nodeIdx;
}

int ProgressiveGridFE::addFace() {
    int faceIdx = _noElems;

    if (_noElems >= _maxElems) {
        err_couldnt_add_elem(_maxElems);
        return -1;
    }

    _elems[faceIdx][0] = faceIdx+1;
    _elems[faceIdx][1] = 3;
    _elems[faceIdx][2] = 0;
    _elemsNodes[faceIdx][0] = 0;
    _elemsNodes[faceIdx][1] = 0;
    _elemsNodes[faceIdx][2] = 0;

    _noElems++;

    return faceIdx;
}

int ProgressiveGridFE::addSplit() {
    int splitIdx = _noSplits;

    if (_noSplits >= _maxSplits) {
        err_couldnt_add_split(_maxSplits);
        return -1;
    }

    _splitsIdx[splitIdx] = new int[8];
    _splitsVec[splitIdx] = new real[3];

    _noSplits++;

    splitNr(splitIdx) = splitIdx + 1;
    splitNodeNr(splitIdx, 0) = 0;
    splitNodeNr(splitIdx, 1) = 0;
    splitCommonNodeNr(splitIdx, 0) = 0;
    splitCommonNodeNr(splitIdx, 1) = 0;
    splitDirectionNodeNr(splitIdx) = 0;
    splitFaceConfiguration(splitIdx, 0) = 0;
    splitFaceConfiguration(splitIdx, 1) = 0;
    splitVector(splitIdx, 0) = 0.0;
    splitVector(splitIdx, 1) = 0.0;
    splitVector(splitIdx, 2) = 0.0;

    return splitIdx;
}

void ProgressiveGridFE::removeEdge(int edgeIdx) {
    int lastIdx = _noEdges-1;
    swapEdges(lastIdx, edgeIdx);
    _noEdges--;
}

void ProgressiveGridFE::removeFace(int faceIdx) {
    int lastIdx = _noElems-1;
    swapFaces(lastIdx, faceIdx);
    _noElems--;
}

void ProgressiveGridFE::removeNode(int nodeIdx) {
    int lastIdx = _noNodes-1;
    swapNodes(lastIdx, nodeIdx);
    _noNodes--;
}

void ProgressiveGridFE::swapNodes(int fromIdx, int toIdx) {
    swap(_nodes[fromIdx][0], _nodes[toIdx][0]);
    swap(_nodes[fromIdx][1], _nodes[toIdx][1]);
    swap(_coords[fromIdx][0], _coords[toIdx][0]);
    swap(_coords[fromIdx][1], _coords[toIdx][1]);
    swap(_coords[fromIdx][2], _coords[toIdx][2]);
}

void ProgressiveGridFE::swapFaces(int fromIdx, int toIdx) {
    swap(_elems[fromIdx][0], _elems[toIdx][0]);
    swap(_elems[fromIdx][1], _elems[toIdx][1]);
    swap(_elems[fromIdx][2], _elems[toIdx][2]);
    swap(_elemsNodes[fromIdx][0], _elemsNodes[toIdx][0]);
    swap(_elemsNodes[fromIdx][1], _elemsNodes[toIdx][1]);
    swap(_elemsNodes[fromIdx][2], _elemsNodes[toIdx][2]);
}

void ProgressiveGridFE::swapEdges(int fromIdx, int toIdx) {
    swap(_edges[fromIdx][0], _edges[toIdx][0]);
    swap(_edges[fromIdx][1], _edges[toIdx][1]);
    swap(_edges[fromIdx][2], _edges[toIdx][2]);
    swap(_edges[fromIdx][3], _edges[toIdx][3]);
}

void ProgressiveGridFE::swapSplits(int fromIdx, int toIdx) {
    swap(_splitsIdx[fromIdx][0], _splitsIdx[toIdx][0]);
    swap(_splitsIdx[fromIdx][1], _splitsIdx[toIdx][1]);
    swap(_splitsIdx[fromIdx][2], _splitsIdx[toIdx][2]);
    swap(_splitsIdx[fromIdx][3], _splitsIdx[toIdx][3]);
    swap(_splitsIdx[fromIdx][4], _splitsIdx[toIdx][4]);
    swap(_splitsIdx[fromIdx][5], _splitsIdx[toIdx][5]);
    swap(_splitsIdx[fromIdx][6], _splitsIdx[toIdx][6]);
    swap(_splitsIdx[fromIdx][7], _splitsIdx[toIdx][7]);
    swap(_splitsVec[fromIdx][0], _splitsVec[toIdx][0]);
    swap(_splitsVec[fromIdx][1], _splitsVec[toIdx][1]);
    swap(_splitsVec[fromIdx][2], _splitsVec[toIdx][2]);
}
