#include "ProgressiveGridFEWriter.h"

ProgressiveGridFEWriter::ProgressiveGridFEWriter():
    GridFEWriter() {
}

ProgressiveGridFEWriter::ProgressiveGridFEWriter(const std::string &path):
    GridFEWriter(path) {
}

ProgressiveGridFEWriter::~ProgressiveGridFEWriter() { }

bool ProgressiveGridFEWriter::write(ProgressiveGridFE &pgrid) {
    if (!GridFEWriter::_writeInfo(pgrid)) {
        return false;
    }
    if (!ProgressiveGridFEWriter::_writeInfo(pgrid)) {
        return false;
    }
    if (!GridFEWriter::_writeBndInds(pgrid)) {
        return false;
    }
    if (!GridFEWriter::_writeNodes(pgrid)) {
        return false;
    }
    if (!GridFEWriter::_writeElems(pgrid)) {
        return false;
    }
    if (!ProgressiveGridFEWriter::_writeSplits(pgrid)) {
        return false;
    }
    return true;
}

bool ProgressiveGridFEWriter::_writeInfo(ProgressiveGridFE &pgrid) {
    _output << "  Number of splits      =  " << pgrid.noSplits() << std::endl;
    _output << std::endl;
    return _output.good();
}

bool ProgressiveGridFEWriter::_writeSplits(ProgressiveGridFE &pgrid) {
    _output << std::endl;
    _output << "  Split operations " << std::endl;
    _output << "  the columns contain:" << std::endl;
    _output << "   - two nodes forming the contracted edge," << std::endl;
    _output << "   - two nodes shared by the contracted edge," << std::endl;
    _output << "   - node useful in faces reconnection," << std::endl;
    _output << "   - three displacement vector coordinates." << std::endl;
    _output << "#" << std::endl;

    for (int i = pgrid.noSplits()-1; i >= 0; i--) {
            _output.width(7);
            _output << std::right << std::fixed;
            _output << pgrid.splitNr(i);
            _output << "  NodeSplit    ";
            _output << pgrid.splitNodeNr(i, 0) << "   ";
            _output << pgrid.splitNodeNr(i, 1) << "   ";
            _output << pgrid.splitCommonNodeNr(i, 0) << "   ";
            _output << pgrid.splitCommonNodeNr(i, 1) << "   ";
            _output << pgrid.splitDirectionNodeNr(i) << "   ";
            _output << pgrid.splitFaceConfiguration(i, 0) << "   ";
            _output << pgrid.splitFaceConfiguration(i, 1) << "   ";
            _output << pgrid.splitVector(i, 0) << "   ";
            _output << pgrid.splitVector(i, 1) << "   ";
            _output << pgrid.splitVector(i, 2) << "   ";
            _output << std::endl;
    }
    return _output.good();
}
